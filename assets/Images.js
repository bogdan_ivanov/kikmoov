export const Images = {
  // #region Social icons
  facebookIcon: require("./images/facebook/fb.png"),
  linkedinIcon: require("./images/linkedin/in.png"),
  googleIcon: require("./images/google/g.png"),
  // #endregion

  // #region Buttons
  privateOffice: require("./images/private-office/private-office.png"),
  meetingRoom: require("./images/meeting-room/meeting-room.png"),
  desk: require("./images/desk/desk.png"),

  plus: require("./images/plus/plus.png"),
  minus: require("./images/minus/minus.png"),

  openMap: require("./images/open-map/open-map.png"),

  backButtonWhite: require("./images/back-button-white/back-button-white.png"),
  filterButton: require("./images/filter-button/filter-button.png"),
  sortButton: require("./images/sort-button/sort-button.png"),
  backButton: require("./images/back-button/back-button.png"),
  videoButton: require("./images/video-icon/video-icon.png"),
  mapClose: require("./images/map-close/map-close.png"),
  // #endregion

  // #region Logos
  locationMarker: require("./images/location-marker/location-marker.png"),
  visitedLogo: require("./images/visited-logo/visited-logo.png"),
  logoDark: require("./images/logo-dark/kikmoov.png"),
  logoLight: require("./images/mono_logo.png"),
  // #endregion

  // #region Welcome Screen
  welcomeScreen_1: require("./images/welcome_1.png"),
  welcomeScreen_2: require("./images/welcome_2.png"),
  welcomeScreen_3: require("./images/welcome_3.png"),
  // #endregion

  homeBackground: require("./images/home-background.jpg"),

  // #region User icons
  bookingRequestSentIcon: require("./images/booking-request-sent-icon/booking-request-sent-icon.png"),
  scheduleViewingIcon: require("./images/schedule-viewing-icon/schedule-viewing-icon.png"),
  viewingRequestIcon: require("./images/viewing-request-icon/viewing-request-icon.png"),
  requestToBookIcon: require("./images/request-to-book-icon/request-to-book-icon.png"),
  emptySearchIcon: require("./images/empty-search-icon/empty-search-icon.png"),
  emptyLikesIcon: require("./images/empty-likes-icon/empty-likes-icon.png"),
  transportIcon: require("./images/transport-icon/transport-icon.png"),
  heartFilled: require("./images/heart-filled/heart-filled.png"),
  expandIcon: require("./images/expand-icon/expand-icon.png"),
  heartEmpty: require("./images/heart-empty/heart-empty.png"),
  iconCheck: require("./images/icon-check/icon-check.png"),
  shareIcon: require("./images/share-icon/share-icon.png"),
  menuIcon: require("./images/menu-icon/menu-icon.png"),
  calendar: require("./images/calendar/calendar.png"),
  search: require("./images/search/search-icon.png"),
  clear: require("./images/clear/clear-button.png"),
  cross: require("./images/cross/cross.png"),

  emptyBookingIcon: require("./images/empty-booking-icon/empty-booking-icon.png"),
  cancelBooking: require("./images/cancel-booking/cancel-booking.png"),
  addressIcon: require("./images/address-icon/address-icon.png"),
  phoneIcon: require("./images/phone-icon/phone-icon.png"),
  emailIcon: require("./images/email-icon/email-icon.png"),

  iconDelete: require("./images/icon-delete-account/icon-delete-account.png"),
  iconContactUs: require("./images/icon-contact-us/icon-contact-us.png"),
  iconPrivacy: require("./images/privacy-policy/privacy-policy.png"),
  iconWarning: require("./images/icon-warning/icon-warning.png"),
  iconReset: require("./images/icon-reset/icon-reset.png"),
  iconClose: require("./images/icon-close/icon-close.png"),
  iconFaq: require("./images/icon-faq/icon-faq.png"),
  iconMap: require("./images/icon-map/icon-map.png"),

  bookingsIcon: require("./images/bookings-icon/bookings-icon.png"),
  searchIcon: require("./images/search-icon/search-icon.png"),
  chatIcon: require("./images/chat-icon/chat-icon.png"),

  americanExpressIcon: require("./images/american-express-icon/american-express-icon.png"),
  mastercardIcon: require("./images/mastercard-icon/mastercard-icon.png"),
  visaIcon: require("./images/visa-icon/visa-icon.png")
  // #endregion
};
