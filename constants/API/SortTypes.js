// @flow
export const SortTypes = {
	PRICE: {
		HIGHEST: "highest",
		LOWEST: "lowest"
	},
	DATE: {
		NEWEST: "newest",
		OLDEST: "oldest"
	}
};

export const SortLabels = {
	PRICE: {
		HIGHEST: "Highest price",
		LOWEST: "Lowest price"
	},
	DATE: {
		NEWEST: "Newest listed",
		OLDEST: "Oldest listed"
	}
};
