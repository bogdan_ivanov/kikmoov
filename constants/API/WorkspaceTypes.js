// @flow
export const WorkspaceTypes = {
	desk: "desk",
	"meeting-room": "meeting-room",
	"private-office": "private-office"
};

export const WorkspaceTypesLabels = {
	[WorkspaceTypes.desk]: "Desk",
	[WorkspaceTypes["meeting-room"]]: "Meeting space",
	[WorkspaceTypes["private-office"]]: "Private Office"
};

export const WorkspaceTypesDurationLabels = {
	[WorkspaceTypes["meeting-room"]]: "/hr",
	[WorkspaceTypes["private-office"]]: "/m"
};
