// @flow
export const FilterTypes = {
	DURATION: {
		HOURLY: "hourly",
		MONTHLY: "monthly"
	},
	AREA: {
		DESK: "desk",
		PRIVATE: "private-office",
		MEETING: "meeting-room"
	}
};

export const FilterLabels = {
	DURATION: {
		HOURLY: "Hourly",
		MONTHLY: "Monthly"
	},
	AREA: {
		DESK: "Desk",
		PRIVATE: "Private office",
		MEETING: "Meeting space"
	}
};
