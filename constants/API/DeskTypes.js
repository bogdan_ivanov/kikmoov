// @flow
export const DeskTypes = {
	monthly_fixed_desk: "monthly_fixed_desk",
	monthly_hot_desk: "monthly_hot_desk",
	hourly_hot_desk: "hourly_hot_desk"
};

export const DeskTypesDurationLabels = {
	[DeskTypes.monthly_fixed_desk]: "/m",
	[DeskTypes.monthly_hot_desk]: "/m",
	[DeskTypes.hourly_hot_desk]: "/hr"
};
