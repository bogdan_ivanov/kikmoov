import { StyleSheet } from "react-native";

import { Font } from "./Font";

export const borderButton = {
    alignItems: "center",

    borderRadius: 5,
    borderWidth: 1,

    padding: 8
};

export const filledButton = {
    alignItems: "center",

    borderRadius: 5,

    padding: 9
};

export const buttonText = {
    fontFamily: Font.type.medium,
    textAlign: "center",
    fontSize: Font.size.m,
};

export const buttonImageText = {
    fontFamily: Font.type.light,
};

export const buttonStyles = StyleSheet.create({
    borderButton,

    filledButton,

    buttonText,
    buttonImageText
});
