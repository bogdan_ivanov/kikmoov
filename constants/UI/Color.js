export const Color = {
    red: "#f86649",
    redDark: "#d643ee",
    redLight: "#f8664f",
    redError: "#dd021d",
    grayDark: "#2f374b",
    gray: "#9b9b9b",
    extraGray: "#c8c8c8",
    grayLight: "#ececec",
    blue: "#3681b7",
    darkWhite: "#f5f5f5",
    yellow: "#ffcb3f"
};
