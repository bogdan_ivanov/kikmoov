import { Platform, Dimensions } from "react-native";

import { Color } from "./Color";
import { Font } from "./Font";

const { height } = Dimensions.get("window");

const imageWidthPercentage = .247;

export const shadowArea = {
    shadowColor: "rgba(0,0,0,0.05)",
    shadowOffset: {
        width: 0,
        height: 4
    },
    shadowRadius: 3,
    shadowOpacity: 1,

    backgroundColor: "#fff",

    borderWidth: Platform.select({ ios: 1, android: 0 }),
    borderColor: Color.grayLight,

    elevation: 2
};

export const backgroundInput = {
    padding: 7,

    fontFamily: Font.type.medium,
    fontSize: Font.size.l,

    backgroundColor: Color.darkWhite,

    color: "#000",

    borderRadius: 5
};

export const underlineInput = {
    paddingBottom: 7,

    borderBottomWidth: 1,
    borderColor: Color.grayLight,
    fontFamily: Font.type.medium,
    fontSize: Font.size.l,

    color: "#000"
};

export const image = {
    width: "100%",
    height: height * imageWidthPercentage,

    borderRadius: 5
};

export const containerFlex = {
    paddingLeft: "7.5%",
    paddingRight: "7.5%"
};

export const modalOverlay = {
    ...shadowArea,
    ...containerFlex,

    height: Dimensions.get("screen").height,

    backgroundColor: "rgba(0, 0, 0, 0.15)",
    justifyContent: "center"
};

export const containerStatic = {
    paddingLeft: 20,
    paddingRight: 20
};

export const buttonGroupContainer = {
    justifyContent: "space-between",
    alignItems: "stretch",
    flexDirection: "row",

    borderTopWidth: 1,
    borderTopColor: Color.grayLight
};

export const triangle = (params) => {
    let borderParams = {};

    switch (params.type) {
        case "left": {
            borderParams = {
                borderTopWidth: params.size,
                borderBottomWidth: params.size,

                borderRightWidth: params.height,
                borderRightColor: params.color
            };
            break;
        }
        case "right": {
            borderParams = {
                borderTopWidth: params.size,
                borderBottomWidth: params.size,

                borderLeftWidth: params.height,
                borderLeftColor: params.color
            };
            break;
        }
        case "top": {
            borderParams = {
                borderRightWidth: params.size,
                borderLeftWidth: params.size,

                borderBottomWidth: params.height,
                borderBottomColor: params.color
            };
            break;
        }
        case "bottom": {
            borderParams = {
                borderRightWidth: params.size,
                borderLeftWidth: params.size,

                borderTopWidth: params.height,
                borderTopColor: params.color
            };
            break;
        }
        case "bottomLeft": {
            borderParams = {
                borderRightWidth: params.size,
                borderBottomWidth: params.size,

                borderBottomColor: params.color
            };
            break;
        }
        case "bottomRight": {
            borderParams = {
                borderLeftWidth: params.size,
                borderBottomWidth: params.size,

                borderBottomColor: params.color
            };
            break;
        }
        case "topRight": {
            borderParams = {
                borderLeftWidth: params.size,
                borderTopWidth: params.size,

                borderTopColor: params.color
            };
            break;
        }
        case "topLeft": {
            borderParams = {
                borderRightWidth: params.size,
                borderTopWidth: params.size,

                borderTopColor: params.color
            };
            break;
        }
        default: {
            borderParams = {
                borderRightWidth: params.size,
                borderLeftWidth: params.size,

                borderBottomWidth: params.height,
                borderBottomColor: params.color
            };
        }
    }

    return {
        width: 0,
        height: 0,

        borderColor: "transparent",

        ...borderParams
    };
};
