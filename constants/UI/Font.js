import { Dimensions } from "react-native";

const { width } = Dimensions.get("window");

const getNaturalSize = (fakeSize) => {
    const k = 1000;

    return (width / k) * fakeSize;
};

/* eslint-disable no-magic-numbers */
export const Font = {
    size: {
        xxs: getNaturalSize(27),
        xs: getNaturalSize(30),
        s: getNaturalSize(35),
        m: getNaturalSize(40),

        l: getNaturalSize(45),

        xl: getNaturalSize(55),
        xxl: getNaturalSize(60)
    },
    type: {
        light: "montserrat-light",
        regular: "montserrat-regular",

        medium: "montserrat-medium",

        semiBold: "montserrat-semibold",
        bold: "montserrat-bold",

        extraBold: "montserrat-extrabold",
    }
};
/* eslint-enable no-magic-numbers */
