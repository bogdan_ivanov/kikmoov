import Axios from "axios";
import { getActionTypes } from "redux-axios-middleware";

export const onError = ({ action, next, error }, options) => {
	let errorObject;
	if (Axios.isCancel(error)) {
		const nextAction = {
			type: `${action.type}_CANCEL`,
			meta: {
				previousAction: action
			}
		};

		next(nextAction);
		return nextAction;
	}

	if (!error.response) {
		errorObject = {
			data: error.message,
			status: 0
		};
		if (process.env.NODE_ENV !== "production") {
			/* eslint-disable-next-line no-console */
			console.log("HTTP Failure in Axios", error);
		}
	} else {
		errorObject = error;
	}

	const nextAction = {
		type: getActionTypes(action, options)[2],
		error: errorObject,
		meta: {
			previousAction: action
		},
		params: action.params
	};

	next(nextAction);
	return nextAction;
};
