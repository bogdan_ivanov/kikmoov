import { getActionTypes } from "redux-axios-middleware";

export const onSuccess = ({ action, next, response }, options) => {
	const nextAction = {
		type: getActionTypes(action, options)[1],
		payload: response,
		params: action.params,
		meta: {
			previousAction: action
		}
	};
	next(nextAction);
	return nextAction;
};
