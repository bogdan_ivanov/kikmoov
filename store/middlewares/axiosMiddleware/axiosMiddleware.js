import reduxAxiosMiddleware from "redux-axios-middleware";

import * as Client from "../../client";
import { JWT } from "../../interceptors";

import { onError } from "./methods/onError";
import { onSuccess } from "./methods/onSuccess";

export const axiosMiddleware = reduxAxiosMiddleware(Client.instance, {
	interceptors: {
		response: [],
		request: [JWT]
	},
	onSuccess,
	onError
});
