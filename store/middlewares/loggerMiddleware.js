import { Toaster } from "react-native-toastboard";

import { SecurityActionType } from "@store/actions";
import { ActionsManager } from "@store/managers";

const skipToastingActions = [
	ActionsManager.asFail(SecurityActionType.SIGN_IN)
];

export function loggerMiddleware() {
	return (next) => (action) => {
		/* eslint-disable no-console */
		console.log("action", action.type);

		if (action.error && !skipToastingActions.includes(action.type)) {
			Toaster.error(action.error);
		}

		if (action.error && action.error.response) {
			console.log("***ERROR***");
			console.log(action.error.response.data);
			console.log("***ERROR***");
		}

		/* eslint-enable no-console */
		return next(action);
	};
}
