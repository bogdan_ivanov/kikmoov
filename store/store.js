import { createStore, applyMiddleware } from "redux";

import { rootReducer } from "./reducers/rootReducer";
import { axiosMiddleware, loggerMiddleware } from "./middlewares";

export const store = createStore(
    rootReducer,
    applyMiddleware(axiosMiddleware, loggerMiddleware)
);
