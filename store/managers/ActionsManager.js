export class ActionsManager {
    static SUCCESS_POSTFIX = "_SUCCESS";
    static CANCEL_POSTFIX = "_CANCEL";
    static FAIL_POSTFIX = "_FAIL";

    static asSuccess(actionName) {
        return `${actionName}${ActionsManager.SUCCESS_POSTFIX}`;
    }

    static asFail(actionName) {
        return `${actionName}${ActionsManager.FAIL_POSTFIX}`;
    }

    static asCancel(actionName) {
        return `${actionName}${ActionsManager.CANCEL_POSTFIX}`;
    }

    static isSuccess(actionName) {
        return actionName.includes(ActionsManager.SUCCESS_POSTFIX);
    }

    static isFail(actionName) {
        return actionName.includes(ActionsManager.FAIL_POSTFIX);
    }

    static isCancel(actionName) {
        return actionName.includes(ActionsManager.CANCEL_POSTFIX);
    }

    static isPure(actionName) {
        return !ActionsManager.isFail(actionName)
            && !ActionsManager.isCancel(actionName)
            && !ActionsManager.isSuccess(actionName);
    }

    static makePure(actionName) {
        return actionName
            .replace(ActionsManager.FAIL_POSTFIX, "")
            .replace(ActionsManager.CANCEL_POSTFIX, "")
            .replace(ActionsManager.SUCCESS_POSTFIX, "");
    }
}
