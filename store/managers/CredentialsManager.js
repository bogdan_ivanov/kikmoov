import { API_DATA_STORAGE_KEY } from "react-native-dotenv";
import { AsyncStorage } from "react-native";

export class CredentialsManager {
	static async getCredentials() {
		let apiData;
		try {
			apiData = await AsyncStorage.getItem(API_DATA_STORAGE_KEY);
		} catch (error) {
			return false;
		}

		if (!apiData) {
			return false;
		}

		try {
			apiData = JSON.parse(apiData);
		} catch (error) {
			AsyncStorage.removeItem(API_DATA_STORAGE_KEY);
			return false;
		}

		return apiData;
	}

	static async setCredentials(data) {
		const generate_in = Date.now();

		try {
			await AsyncStorage.setItem(API_DATA_STORAGE_KEY, JSON.stringify({ ...data, generate_in }));
		} catch (error) {
			return false;
		}

		return true;
	}

	static async removeCredentials() {
		try {
			await AsyncStorage.removeItem(API_DATA_STORAGE_KEY);
		} catch (error) {
			return false;
		}

		return true;
	}
}
