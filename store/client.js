// @flow
import Axios from "axios";
import queryString from "query-string";
import { API_URL } from "react-native-dotenv";

export const instance = Axios.create({
    baseURL: API_URL,
    headers: {
        mobileApp: true
    },
    paramsSerializer: (params) => queryString.stringify(params, { arrayFormat: "index" })
});

instance.interceptors.request.use((request) => {
    if (!request.isPrivate) {
        delete request.headers.Authorization;
    }

    return request;
});

export function setToken(token: string) {
    Object.assign(instance.defaults.headers, {
        Authorization: `Bearer ${token}`
    });
}

export function removeToken() {
    delete instance.defaults.headers.Authorization;
}
