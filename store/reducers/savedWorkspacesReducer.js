// @flow
import { SavedWorkspacesActionType } from "../actions";
import { ActionsManager } from "../managers";

import { SavedWorkspacesReducerInterface, ActionInteface } from "../../interfaces";

const initialState: SavedWorkspacesReducerInterface = {
    workspaces: undefined,
    ids: new Set()
};

export function savedWorkspacesReducer(state: SavedWorkspacesReducerInterface = initialState, action: ActionInteface) {
    switch (action.type) {
        case ActionsManager.asSuccess(SavedWorkspacesActionType.FETCH): {
            state.ids.clear();
            action.payload.data.workspaces.forEach(({ workspace }) => {
                state.ids.add(workspace.id);
            });

            return {
                workspaces: action.payload.data.workspaces,
                ids: state.ids
            };
        }
        case ActionsManager.asFail(SavedWorkspacesActionType.SWITCH_LIKE):
        case SavedWorkspacesActionType.SWITCH_LIKE: {
            if (state.ids.has(action.params.id)) {
                state.ids.delete(action.params.id);
            } else {
                state.ids.add(action.params.id);
            }

            return {
                ...state,
                ids: state.ids
            };
        }
        case SavedWorkspacesActionType.RESET: {
            return {
                workspaces: undefined,
                ids: state.ids
            };
        }
        default: {
            return state;
        }
    }
}
