// @flow
import { SecurityActionType } from "../actions/security";
import { ActionsManager } from "../managers";

import { SecurityReducerInterface, ActionInteface } from "../../interfaces";

const initialState: SecurityReducerInterface = {
    authorized: false
};

export function securityReducer(state: SecurityReducerInterface = initialState, action: ActionInteface) {
    switch (action.type) {
        case ActionsManager.asSuccess(SecurityActionType.FACEBOOK_SIGN_IN):
        case ActionsManager.asSuccess(SecurityActionType.LINKEDIN_SIGN_IN):
        case ActionsManager.asSuccess(SecurityActionType.GOOGLE_SIGN_IN):
        case ActionsManager.asSuccess(SecurityActionType.SIGN_IN): {
            // TODO: Store some stuff
            return state;
        }
        case ActionsManager.asSuccess(SecurityActionType.REFRESH_TOKEN):
        case ActionsManager.asSuccess(SecurityActionType.GET_TOKEN): {
            return {
                ...state,
                authorized: true
            };
        }
        case SecurityActionType.SIGN_OUT:
        case ActionsManager.asFail(SecurityActionType.REFRESH_TOKEN): {
            return {
                authorized: false
            };
        }
        case ActionsManager.asFail(SecurityActionType.FACEBOOK_SIGN_IN):
        case ActionsManager.asFail(SecurityActionType.LINKEDIN_SIGN_IN):
        case ActionsManager.asFail(SecurityActionType.GOOGLE_SIGN_IN):
        case ActionsManager.asFail(SecurityActionType.GET_TOKEN):
        case ActionsManager.asFail(SecurityActionType.SIGN_IN): {
            // TODO: Error handling
            return state;
        }
        default: {
            return state;
        }
    }
}
