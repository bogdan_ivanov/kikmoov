import { BookingListActionType } from "../actions/bookingList";
import { ActionsManager } from "../managers";

export const initialState = {
	list: []
};

export function bookingListReducer(state = initialState, action) {
	switch (action.type) {
		case ActionsManager.asSuccess(BookingListActionType.FETCH): {
			return {
				list: action.payload.data
			};
		}
		case BookingListActionType.RESET: {
			return initialState;
		}
		case ActionsManager.asSuccess(BookingListActionType.CANCEL_BOOKING): {
			return {
				list: state.list.map((item) => {
					if (item.booking && item.booking.id === action.params.id) {
						return {
							...item,
							booking: {
								...item.booking,
								future: false
							}
						};
					}

					return item;
				})
			};
		}
		case ActionsManager.asSuccess(BookingListActionType.CANCEL_VIEWING): {
			return {
				list: state.list.map((item) => {
					if (item.viewing && item.viewing.id === action.params.id) {
						return {
							...item,
							viewing: {
								...item.viewing,
								future: false
							}
						};
					}

					return item;
				})
			};
		}
		default: {
			return state;
		}
	}
}
