// @flow
import { ActionsManager } from "../managers";
import { UserActionType } from "../actions";

import { UserReducerInterface, ActionInteface } from "../../interfaces";

export const initialState: UserReducerInterface = {
	email: "",
	lastname: "",
	firstname: "",
	pushStatus: false
};

export function userReducer(state: UserReducerInterface = initialState, action: ActionInteface) {
	switch (action.type) {
		case UserActionType.UPDATE_PUSH_STATUS: {
			return {
				...state,
				pushStatus: action.params.status
			};
		}
		case ActionsManager.asFail(UserActionType.UPDATE_PUSH_STATUS): {
			return {
				...state,
				pushStatus: !state.pushStatus
			};
		}
		case ActionsManager.asSuccess(UserActionType.UPDATE_DETAILS): {
			return {
				...state,
				...action.payload.data.details
			};
		}
		case ActionsManager.asSuccess(UserActionType.GET_DETAILS): {
			return {
				...state,
				...action.payload.data
			};
		}
		default: {
			return state;
		}
	}
}
