// @flow
import { combineReducers } from "redux";

import { savedWorkspacesReducer } from "./savedWorkspacesReducer";
import { bookingListReducer } from "./bookingListReducer";
import { securityReducer } from "./securityReducer";
import { userReducer } from "./userReducer";

export const rootReducer = combineReducers({
    savedWorkspacesReducer,
    bookingListReducer,
    securityReducer,
    userReducer
});
