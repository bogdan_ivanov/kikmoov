export const withReducerPrefix = (prefix) => (actionList) => {
	const wrapped = {};

	Object.keys(actionList).forEach((key) => {
		wrapped[key] = `${prefix.toUpperCase()}_${key}`;
	});

	return wrapped;
};
