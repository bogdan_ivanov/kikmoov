import Axios from "axios";

const generateToken = (plainActions, key) => {
	const source = Axios.CancelToken.source();

	// Fetch saved(or not if it first generation) action
	const origin = plainActions[key].origin || plainActions[key];

	plainActions[key] = (...args) => {
		const params = origin(...args);

		if (!params.payload) {
			return params;
		}

		// assing cancelToken to params
		Object.assign(params.payload.request, { cancelToken: source.token });

		return params;
	};

	// Save origin action to prevent deep execution
	plainActions[key].origin = origin;

	plainActions[key].cancel = (...args) => {
		source.cancel(...args);
		plainActions[key].canceled = true;

		// Generate new token after cancel
		generateToken(plainActions, key);
	};
};

export function bindCancelToken(plainActions) {
	Object.keys(plainActions).forEach((key) => generateToken(plainActions, key));

	return function (dispatch) {
		const linked = {};
		Object.keys(plainActions).forEach((key) => {
			// Create new linked action based on origin action
			linked[key] = bindCancelToken.link((...args) => dispatch(plainActions[key](...args)), plainActions[key]);
		});

		return linked;
	};
}

// Link to method. For example, you can link some context handler with origin action
bindCancelToken.link = function (target, source) {
	const linked = target;

	const origin = source.cancel;
	source.cancel = (...args) => {
		linked.canceled = true;
		return origin(...args);
	};

	linked.cancel = source.cancel;

	return linked;
};
