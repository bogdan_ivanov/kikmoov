// @flow
import * as axiosMiddlewareMethods from "../middlewares/axiosMiddleware/methods";
import { withReducerPrefix } from "../utils/withReducerPrefix";
import { signOut } from "./security";

import type { UpdateDetailsRequestPayloadData } from "../../interfaces";

export const UserActionType = withReducerPrefix("USER")({
	DELETE: "DELETE",
	GET_DETAILS: "GET_DETAILS",
	UPDATE_DETAILS: "UPDATE_DETAILS",
	UPDATE_PUSH_STATUS: "UPDATE_PUSH_STATUS",
});

export function getUserDetails() {
	return {
		type: UserActionType.GET_DETAILS,
		payload: {
			request: {
				method: "GET",
				url: "/api/v1/user",
				isPrivate: true
			}
		}
	};
}

export function updatePushStatus(status: boolean) {
	return {
		type: UserActionType.UPDATE_PUSH_STATUS,
		payload: {
			request: {
				method: "PATCH",
				url: "/api/v1/user/push",
				isPrivate: true
			},
		},
		params: { status }
	};
}

export function updateUserDetails(data: UpdateDetailsRequestPayloadData) {
	return {
		type: UserActionType.UPDATE_DETAILS,
		payload: {
			request: {
				data,
				method: "PUT",
				url: "/api/v1/user",
				isPrivate: true
			}
		}
	};
}

export function deleteUser() {
	return {
		type: UserActionType.DELETE,
		payload: {
			request: {
				method: "DELETE",
				url: "/api/v1/user",
				isPrivate: true
			},
			options: {
				onSuccess: async (params: any, options: any) => {
					await params.dispatch(signOut());

					axiosMiddlewareMethods.onSuccess(params, options);
				}
			}
		}
	};
}
