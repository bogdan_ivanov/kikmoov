// @flow
import { withReducerPrefix } from "../utils/withReducerPrefix";

export const BookingListActionType = withReducerPrefix("BOOKING_LIST")({
	FETCH: "FETCH",
	INVITE: "INVITE",
	CANCEL_VIEWING: "CANCEL_VIEWING",
	CANCEL_BOOKING: "CANCEL_BOOKING",

	RESET: "RESET"
});

export function fetchBookingList() {
	return {
		type: BookingListActionType.FETCH,
		payload: {
			request: {
				method: "GET",
				url: "/api/v1/user/requests",
				isPrivate: true
			}
		}
	};
}

export function inviteAttendee(id: number, emails: Array<string>) {
	return {
		type: BookingListActionType.INVITE,
		payload: {
			request: {
				method: "POST",
				url: `/api/v1/user/booking/${id}/invite-attendee`,
				isPrivate: true,
				data: { emails }
			}
		}
	};
}

export function cancelBooking(id: number) {
	return {
		type: BookingListActionType.CANCEL_BOOKING,
		payload: {
			request: {
				method: "PATCH",
				url: `/api/v1/user/booking/${id}/cancel`,
				isPrivate: true
			},
		},
		params: { id }
	};
}

export function cancelViewing(id: number) {
	return {
		type: BookingListActionType.CANCEL_VIEWING,
		payload: {
			request: {
				method: "PATCH",
				url: `/api/v1/user/viewing/${id}/cancel`,
				isPrivate: true
			},
			id
		},
		params: { id }
	};
}

export function resetBookingListStore() {
	return {
		type: BookingListActionType.RESET
	};
}
