// @flow
import { Toaster } from "react-native-toastboard";

import * as axiosMiddlewareMethods from "../../middlewares/axiosMiddleware/methods";
import { withReducerPrefix } from "../../utils/withReducerPrefix";

import * as Client from "../../client";
import { CredentialsManager } from "../../managers";

import type {
        SignInRequestPayloadData,
        GetTokenRequestPayloadData,
        RefreshTokenRequestPayloadData
} from "../../../interfaces";

export const SecurityActionType = withReducerPrefix("SECURITY")({
    SIGN_IN: "SIGN_IN",
    SIGN_OUT: "SIGN_OUT",
    GET_TOKEN: "GET_TOKEN",
    REFRESH_TOKEN: "REFRESH_TOKEN",
    GOOGLE_SIGN_IN: "GOOGLE_SIGN_IN",
    FACEBOOK_SIGN_IN: "FACEBOOK_SIGN_IN",
    LINKEDIN_SIGN_IN: "LINKEDIN_SIGN_IN"
});

export function signIn(data: SignInRequestPayloadData) {
    return {
        type: SecurityActionType.SIGN_IN,
        payload: {
            request: {
                data,
                method: "POST",
                url: "/api/login"
            },
            options: {
                onSuccess: async (params: any, options: any) => {
                    await params.dispatch(getToken({
                        username: data.email,
                        password: data.password,

                        client_secret: params.response.data.client_secret,
                        client_id: params.response.data.client_id,
                    }));

                    axiosMiddlewareMethods.onSuccess(params, options);
                }
            }
        }
    };
}

export function signOut() {
    CredentialsManager.removeCredentials();
    Client.removeToken();

    return {
        type: SecurityActionType.SIGN_OUT
    };
}

export function getToken(data: GetTokenRequestPayloadData) {
    return {
        type: SecurityActionType.GET_TOKEN,
        payload: {
            request: {
                params: {
                    ...data,
                    grant_type: "password",
                },
                method: "GET",
                url: "/oauth/v2/token"
            },
            options: {
                onSuccess: async (params: any, options: any) => {
                    const setled = await CredentialsManager.setCredentials({
                        refresh_token: params.response.data.refresh_token,
                        access_token: params.response.data.access_token,

                        expires_in: params.response.data.expires_in,

                        client_secret: data.client_secret,
                        client_id: data.client_id
                    });

                    if (!setled) {
                        return Toaster.error({});
                    }

                    Client.setToken(params.response.data.access_token);

                    axiosMiddlewareMethods.onSuccess(params, options);
                }
            }
        }
    };
}

export function refreshToken(data: RefreshTokenRequestPayloadData) {
    return {
        type: SecurityActionType.REFRESH_TOKEN,
        payload: {
            request: {
                params: {
                    ...data,
                    grant_type: "refresh_token"
                },
                method: "GET",
                url: "/oauth/v2/token"
            },
            options: {
                onSuccess: async (params: any, options: any) => {
                    const setled = await CredentialsManager.setCredentials({
                        refresh_token: params.response.data.refresh_token,
                        access_token: params.response.data.access_token,

                        expires_in: params.response.data.expires_in,

                        client_secret: data.client_secret,
                        client_id: data.client_id
                    });

                    if (!setled) {
                        return Toaster.error({});
                    }

                    Client.setToken(params.response.data.access_token);

                    axiosMiddlewareMethods.onSuccess(params, options);
                },
                onError: async (params: any, options: any) => {
                    params.dispatch(signOut());

                    axiosMiddlewareMethods.onError(params, options);
                }
            }
        }
    };
}
