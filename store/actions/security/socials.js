// @flow
import { SecurityActionType, getToken } from "./security";
import * as axiosMiddlewareMethods from "../../middlewares/axiosMiddleware/methods";

import type {
		GoogleInRequestPayloadData,
		FacebookSignInRequestPayloadData,
		LinkedinSignInRequestPayloadData
} from "../../../interfaces";

export function facebookSignIn(data: FacebookSignInRequestPayloadData) {
	return {
		type: SecurityActionType.FACEBOOK_SIGN_IN,
		payload: {
			request: {
				data,
				method: "POST",
				url: "/api/fb/check"
			},
			options: {
				onSuccess: async (params: any, options: any) => {
					const { client_secret, client_id, socialToken, email } = params.response.data;

					await params.dispatch(getToken({
						username: email,
						password: socialToken,

						client_secret: client_secret,
						client_id: client_id
					}));

					axiosMiddlewareMethods.onSuccess(params, options);
				}
			}
		}
	};
}

export function googleSignIn(data: GoogleInRequestPayloadData) {
	return {
		type: SecurityActionType.GOOGLE_SIGN_IN,
		payload: {
			request: {
				data,
				method: "POST",
				url: "/api/google/check"
			},
			options: {
				onSuccess: async (params: any, options: any) => {
					const { client_secret, client_id, socialToken, email } = params.response.data;

					await params.dispatch(getToken({
						username: email,
						password: socialToken,

						client_secret: client_secret,
						client_id: client_id
					}));

					axiosMiddlewareMethods.onSuccess(params, options);
				}
			}
		}
	};
}

export function linkedinSignIn(data: LinkedinSignInRequestPayloadData) {
	return {
		type: SecurityActionType.LINKEDIN_SIGN_IN,
		payload: {
			request: {
				data,
				method: "POST",
				url: "/api/linkedin/check"
			},
			options: {
				onSuccess: async (params: any, options: any) => {
					const { client_secret, client_id, socialToken, email } = params.response.data;

					await params.dispatch(getToken({
						username: email,
						password: socialToken,

						client_secret: client_secret,
						client_id: client_id
					}));

					axiosMiddlewareMethods.onSuccess(params, options);
				}
			}
		}
	};
}
