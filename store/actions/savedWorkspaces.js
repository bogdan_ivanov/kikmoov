// @flow
import { withReducerPrefix } from "../utils/withReducerPrefix";

export const SavedWorkspacesActionType = withReducerPrefix("SAVED_WORKSPACES")({
	FETCH: "FETCH",
	SWITCH_LIKE: "SWITCH_LIKE",

	RESET: "RESET"
});

export function fetchSavedWorkspacesList() {
	return {
		type: SavedWorkspacesActionType.FETCH,
		payload: {
			request: {
				method: "GET",
				url: "/api/v1/user/liked",
				isPrivate: true
			}
		}
	};
}

export function switchLike(id: number) {
	return {
		type: SavedWorkspacesActionType.SWITCH_LIKE,
		payload: {
			request: {
				method: "POST",
				url: `/api/v1/user/like/${id}`,
				isPrivate: true
			}
		},
		params: { id }
	};
}

export function resetSavedWorkspacesList() {
	return {
		type: SavedWorkspacesActionType.RESET
	};
}
