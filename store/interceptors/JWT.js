import { CredentialsManager } from "../managers";
import { refreshToken } from "../actions/security";

export async function JWT(params, request) {
	if (!request.isPrivate || !refreshToken) {
		return request;
	}

	const data = await CredentialsManager.getCredentials();

	if (!data) {
		return request;
	}

	const currentTime = Date.now();
	const expireTime = data.generate_in + (data.expires_in * 1000); // convert sec to msec
	const timeShift = 120000; // 2 minutes leeway for request

	if (currentTime >= (expireTime - timeShift)) {
		const { client_secret, client_id, refresh_token } = data;

		await params.dispatch(refreshToken({ client_secret, client_id, refresh_token }));
		const newData = await CredentialsManager.getCredentials();

		request.headers.Authorization = `Bearer ${newData.access_token}`;
	}

	return request;
}
