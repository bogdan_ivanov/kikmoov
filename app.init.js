/* eslint-disable */
const fs = require("fs");
const env = require("dotenv").config();

let template = JSON.stringify(require("./app.template"));

Object.keys(env).forEach((key) => {
	template = template.replace(key, env[key]);
});

fs.writeFile("app.json", template, function (err) {
	if (err) throw err;
	console.log("'app.json' initialized");
});
