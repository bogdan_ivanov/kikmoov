import { StatusBar } from "react-native";

export class StatusBarManager {
    static currentState = false;

    static history = [false];

    static reset = function () {
        StatusBarManager.history = [false];
        StatusBarManager.currentState = false;

        StatusBar.setHidden(StatusBarManager.currentState);
    }

    static setState = function (state, animation = "slide") {
        const isHidden = state === "hidden";

        StatusBarManager.history.push(isHidden);
        StatusBarManager.currentState = isHidden;

        StatusBar.setHidden(isHidden, animation);
    }

    static restoreState = function (animation = "slide") {
        if (StatusBarManager.history.length === 1) {
            return StatusBarManager.reset();
        }

        StatusBarManager.history.splice(-1);

        StatusBarManager.currentState = StatusBarManager.history[StatusBarManager.history.length - 1];

        StatusBar.setHidden(!!StatusBarManager.currentState, animation);
    }

}
