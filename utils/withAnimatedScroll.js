import { Animated } from "react-native";

export const withAnimatedScroll = (useNativeDriver = true) => (WrappedComponent) => {
    WrappedComponent.prototype.value = new Animated.Value(0);

    WrappedComponent.prototype.handleScroll = Animated.event(
        [
            { nativeEvent: { contentOffset: { y: WrappedComponent.prototype.value } } }
        ],
        { useNativeDriver }
    );

    return WrappedComponent;
};
