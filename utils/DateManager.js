import { LocaleConfig } from "react-native-calendars";

LocaleConfig.locales["en"] = {
	...LocaleConfig.locales[""],
	dayNamesShort: ["Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"]
};
LocaleConfig.defaultLocale = "en";

export class DateManager {
	static fromUNIX = function (value) {
		return value * 1000;
	}

	static toUNIX = function (value) {
		return value / 1000;
	}

	static dateStringFromTimestamp = function (value) {
		const date = new Date(value);

		return String(date.getFullYear()).padStart(2, "0") + "-"
			+ String(date.getMonth() + 1).padStart(2, "0") + "-"
			+ String(date.getUTCDate()).padStart(2, "0");
	}

	static AMPMTime = function (value) {
		const date = new Date(value);
		let hours = date.getHours();

		const minutes = String(date.getMinutes()).padStart(2, "0");
		/* eslint-disable no-magic-numbers */
		const ampm = hours >= 12 ? "pm" : "am";

		hours = hours % 12;
		hours = String(hours ? hours : 12).padStart(2, "0");
		/* eslint-enable no-magic-numbers */

		return `${hours}:${minutes}${ampm}`;
	}

	static parsedDateFromTimestamp = function (value, params = {}) {
		const date = new Date(value);

		const monthes = params.isFull
			? LocaleConfig.locales["en"].monthNames
			: LocaleConfig.locales["en"].monthNamesShort;

		return `${date.getUTCDate()}, ${monthes[date.getMonth()]} ${date.getFullYear()}`;
	}

	static parsedDateTimeFromTimestamp = function (startValue, endValue, params = {}) {
		const timestamp = (new Date(startValue)).getTime();

		const date = DateManager.parsedDateFromTimestamp(timestamp, { isFull: true });
		const startTime = DateManager.AMPMTime(timestamp);

		const endTime = endValue
			? DateManager.AMPMTime((new Date(endValue)).getTime())
			: "";

		if (params.onlyTime) {
			return `${startTime}${endTime ? ` - ${endTime}` : ""}`;
		}

		return `${startTime}${endTime ? ` - ${endTime}` : ""}: ${date}`;
	}

}
