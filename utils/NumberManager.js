const postfixDimension = [
	{ limit: 3, postfix: "K" },
	{ limit: 6, postfix: "M" },
	{ limit: 9, postfix: "G" },
	{ limit: 12, postfix: "T" }
];

export class NumberManager {
	static abbreviate = function (value) {
		const parsed = Math.floor(value);
		const dimension = postfixDimension.find(({ limit }) => {
			const divider = Math.pow(10, limit);
			const dividend = parsed / divider;
			return Math.floor(dividend) > 0 && dividend < 1000;
		});

		if (!dimension) {
			return parsed;
		}

		const leading = parsed.toString().slice(0, dimension.limit * -1);
		// Transform value only if it more than 9 000
		if (leading.length < 2 && dimension.postfix === "K") {
			return parsed;
		}

		let following = parsed.toString().slice(dimension.limit * -1);
		following = Math.floor(following / Math.pow(10, following.length - 1));

		return `${leading}${following ? `.${following}`: ""}${dimension.postfix}`;
	}
}
