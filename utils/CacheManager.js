import shortHash from "shorthash";
import { FileSystem } from "expo";

function generatePath(uri, tempDir) {
	const name = shortHash.unique(uri);
	let mimeType = uri.split(".");
	mimeType = mimeType[mimeType.length - 1];

	return `${FileSystem.cacheDirectory}${tempDir}/${name}.${mimeType}`;
}

export class CacheManager {
	static defaultTempDir = "tempDir";

	static dropTempDir = function (tempDir) {
		return FileSystem.deleteAsync(`${FileSystem.cacheDirectory}${tempDir}`, { idempotent: true });
	}

	static createTempDir = async function (tempDir) {
		const dir = await FileSystem.getInfoAsync(`${FileSystem.cacheDirectory}${tempDir}`);

		if (dir.exists) {
			return;
		}

		return FileSystem.makeDirectoryAsync(`${FileSystem.cacheDirectory}${tempDir}`, { intermediates: true });
	}

	static writeOrRead = async function (uri, tempDir = CacheManager.defaultTempDir) {
		const image = await CacheManager.read(uri, tempDir);

		return image || CacheManager.write(uri, tempDir);
	}

	static read = async function (uri, tempDir = CacheManager.defaultTempDir) {
		const image = await FileSystem.getInfoAsync(generatePath(uri, tempDir));

		return image.exists ? image.uri : false;
	}

	static write = async function (uri, tempDir = CacheManager.defaultTempDir) {
		const dir = await FileSystem.getInfoAsync(`${FileSystem.cacheDirectory}${tempDir}`);

		if (!dir.exists) {
			await CacheManager.createTempDir(tempDir);
		}

		const image = await FileSystem.downloadAsync(uri, generatePath(uri, tempDir));

		return image.uri;
	}
}
