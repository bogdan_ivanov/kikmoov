// @flow
import * as Client from "../store/client";

export function checkBookingDates(
	id: number,
	data: { startTime: number; endTime: number },
	cancelToken?: any): Promise<{ available: boolean }> {
	return Client.instance.post(`/api/workspace/${id}/booking-available`, data, { cancelToken });
}
