// @flow
import * as Client from "../store/client";

import type { TopWorkspacesResponsePayloadData } from "../interfaces";

export function getTopWorkspaces(cancelToken?: any): Promise<TopWorkspacesResponsePayloadData> {
	return Client.instance.get("/api/top-workspaces", { cancelToken });
}
