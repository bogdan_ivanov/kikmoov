// @flow
import * as Client from "../store/client";

import type { UpdatePasswordRequestPayloadData } from "../interfaces";

export function updatePassword(data: UpdatePasswordRequestPayloadData, cancelToken?: any) {
	return Client.instance.patch("/api/v1/user/password", data, { cancelToken, isPrivate: true });
}
