// @flow
import * as Client from "../store/client";

import { NotificationItemInterface } from "../interfaces";

export function getNotifications(cancelToken?: any): Promise<Array<NotificationItemInterface>> {
	return Client.instance.get("/api/v1/internal-notifications", { cancelToken, isPrivate: true });
}
