// @flow
import * as Client from "../store/client";

import type { RequestToBookRequestPayloadData } from "../interfaces";

export function requestToBook(id: number, data: RequestToBookRequestPayloadData, cancelToken?: any): Promise<void> {
	return Client.instance.post(`/api/v1/workspace/${id}/booking-request`, data, { cancelToken, isPrivate: true });
}
