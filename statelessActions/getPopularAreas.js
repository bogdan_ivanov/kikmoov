/* eslint-disable */
// @flow
import * as Client from "../store/client";

export function getPopularAreas(cancelToken?: any): Promise<any> {
	return new Promise((resolve) => resolve({
		data: {
			results: [
				{
					workspace: {
						id: 322,
						coverImageUrl: "https://kikmoov.com/upload/media/default/0001/04/4db98e53a5471e73d03d08127ef34afab476caef.png",
					},
					location: {
						name: "King Cross"
					}
				},
				{
					workspace: {
						id: 322,
						coverImageUrl: "https://kikmoov.com/upload/media/default/0001/04/3ec6942e6ec24e6c61900ee71ca34ec4cc9cf422.png",
					},
					location: {
						name: "Shoreditch"
					}
				},
				{
					workspace: {
						id: 322,
						coverImageUrl: "https://kikmoov.com/upload/media/default/0001/04/b9285f308b1ecefc955b0bd2cc9378b63dd83864.png",
					},
					location: {
						name: "London Bridge"
					}
				},
				{
					workspace: {
						id: 322,
						coverImageUrl: "https://kikmoov.com/upload/media/default/0001/04/8ffb403ebec2e73596a5bde025a160513ba764ce.png",
					},
					location: {
						name: "The city"
					}
				}
			]
		}
	}));
}
