// @flow
import * as Client from "../store/client";

import type { ContactUsRequestPayloadData } from "../interfaces";

export function contactUs(data: ContactUsRequestPayloadData, cancelToken?: any): Promise<void> {
	return Client.instance.post("/api/v1/contact-us", data, { cancelToken, isPrivate: true });
}
