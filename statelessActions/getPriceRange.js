// @flow
import * as Client from "../store/client";

import type { PriceRangeResponsePayloadData } from "../interfaces";

export function getPriceRange(cancelToken?: any): Promise<PriceRangeResponsePayloadData> {
	return Client.instance.get("/api/price-range", { cancelToken });
}
