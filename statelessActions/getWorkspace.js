// @flow
import * as Client from "../store/client";

import type { WorkspaceInfoResponsePaylodData } from "../interfaces";

export function getWorkspace(id: number, cancelToken?: any): Promise<WorkspaceInfoResponsePaylodData> {
	return Client.instance.get(`/api/workspace/${id}`, { cancelToken, isPrivate: true });
}
