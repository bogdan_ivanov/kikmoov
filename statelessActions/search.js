// @flow
import * as Client from "../store/client";

import { SearchRequestPayloadData } from "../interfaces";

export function search(params: SearchRequestPayloadData, cancelToken?: any): Promise<any> {
	return Client.instance.get("/api/search", { params, cancelToken, isPrivate: true });
}
