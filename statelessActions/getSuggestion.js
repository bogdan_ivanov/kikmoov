// @flow
import * as Client from "../store/client";

export function getSuggestion(params: { [key: string]: string }, cancelToken?: any): Promise<Array<string>> {
	return Client.instance.get("/api/suggestion", { params, cancelToken });
}
