// @flow
import * as Client from "../store/client";

import type { AddViewingToScheduleRequestPaylodData } from "../interfaces";

export function addViewingToSchedule(
	id: number,
	data: AddViewingToScheduleRequestPaylodData,
	cancelToken?: any
): Promise<void> {
	return Client.instance.post(`/api/v1/workspace/${id}/viewing`, data, { cancelToken, isPrivate: true });
}
