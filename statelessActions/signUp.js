// @flow
import * as Client from "../store/client";

import type { SignUpRequestPayloadData } from "../interfaces";

export function signUp(data: SignUpRequestPayloadData, cancelToken?: any) {
    return Client.instance.post("/api/registration/buyer", data, { cancelToken });
}
