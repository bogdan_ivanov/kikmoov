// @flow
import * as Client from "../store/client";

import type { FacilitiesListResponsePayloadData } from "../interfaces";

export function fetchFacilitiesList(cancelToken?: any): Promise<FacilitiesListResponsePayloadData> {
	return Client.instance.get("/api/facilities", { cancelToken });
}
