// @flow
import * as Client from "../store/client";

import type { BookRequestPayloadData } from "../interfaces";

export function book(id: number, data: BookRequestPayloadData, cancelToken?: any): Promise<void> {
	return Client.instance.post(`/api/v1/workspace/${id}/booking`, data, { cancelToken, isPrivate: true });
}
