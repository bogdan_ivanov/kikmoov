import Axios from "axios";
import shortId from "shortid";
import * as React from "react";
import { Toaster } from "react-native-toastboard";
import { SafeAreaView, View, Text, ScrollView, ActivityIndicator, Image } from "react-native";

import { NotificationItem } from "@components/NotificationComponents/NotificationItem";

import { BookingListContainer } from "@containers/BookingListContainer";
import { getNotifications } from "@statelessActions";
import { Images } from "@assets/Images";
import { Color } from "@constants/UI";

import { styles } from "./styles";
import { NavigationPropTypes } from "../../navigation/NavigationPropTypes";

export class NotificationsScreen extends React.Component {
	static propTypes = NavigationPropTypes;

	constructor(props) {
		super(props);

		this.cancelToken = Axios.CancelToken.source();
		this.state = {
			notifications: undefined
		};

		this.didBlurListener = props.navigation.addListener("didBlur", () => {
			this.setState({ notifications: undefined });
		});

		this.willFocusListener = props.navigation.addListener("willFocus", this.fetchNotifications);
	}

	componentWillUnmount() {
		this.cancelToken.cancel();
		this.didBlurListener.remove();
		this.willFocusListener.remove();
	}

	render() {
		if (!Array.isArray(this.state.notifications)) {
			return (
				<View style={{ flex: 1, justifyContent: "center" }}>
					<ActivityIndicator size="large" color={Color.gray} />
				</View>
			);
		}

		if (!this.state.notifications.length) {
			return (
				<SafeAreaView style={styles.root}>
					<View style={styles.header}>
						<Text style={styles.headerText}>Notifications</Text>
					</View>
					<View style={styles.noResultsContainer}>
						<Image
							resizeMode="contain"
							style={styles.imageNoResults}
							source={Images.emptyBookingIcon}
						/>
						<Text style={styles.textNoResults}>
							No notifications yet, your past notifications
							will be stored here
						</Text>
					</View>
				</SafeAreaView>
			);
		}

		return (
			<SafeAreaView style={styles.root}>
				<BookingListContainer>
					<View style={styles.header}>
						<Text style={styles.headerText}>Notifications</Text>
					</View>
					<ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
						{this.renderNotificatons()}
					</ScrollView>
				</BookingListContainer>
			</SafeAreaView>
		);
	}

	renderNotificatons = () => {
		return this.state.notifications
			.map((item) => <NotificationItem key={shortId.generate()} {...item} />);
	}

	fetchNotifications = async () => {
		let response;
		try {
			response = await getNotifications(this.cancelToken.token);
		} catch (error) {
			if (Axios.isCancel(error)) {
				return;
			}

			return Toaster.error(error);
		}

		this.setState({ notifications: response.data.notifications });
	}
}
