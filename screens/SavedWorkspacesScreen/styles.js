import { StyleSheet, Dimensions } from "react-native";

import { Font, Color, containerFlex } from "@constants/UI";

export const styles = StyleSheet.create({
    root: {
        flex: 1
    },
    header: {
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 5,
        paddingBottom: 7,

        borderColor: "transparent",
        borderWidth: 1,

        borderBottomColor: Color.grayLight
    },
    headerText: {
        fontSize: Font.size.l,
        fontFamily: Font.type.medium
    },
    contentContainer: {
        flex: 1
    },
	imageNoResults: {
		width: Dimensions.get("window").width / 2,
		maxHeight: Dimensions.get("window").width / 2,

		marginBottom: 15
	},
	noResultsContainer: {
		...containerFlex,

		flex: 1,
		justifyContent: "center",
		alignItems: "center"
	},
	textNoResults: {
		fontFamily: Font.type.regular,
		fontSize: Font.size.m,

		color: Color.grayDark
	}
});
