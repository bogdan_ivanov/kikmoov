import * as React from "react";
import { Ionicons } from "@expo/vector-icons";
import { SafeAreaView, View, Text, Image, ScrollView } from "react-native";

import { WorkspacePreviewItem } from "@components/WorkspaceComponents";
import { BottomTabs } from "@containers/BottomTabs";

import { Color, containerFlex, Font } from "@constants/UI";
import { Images } from "@assets/Images";

import { SavedWorkspacesContainer } from "@containers/SavedWorkspacesContainer";

import { styles } from "./styles";

export class SavedWorkspacesScreen extends React.Component {
    render() {
        return (
            <BottomTabs>
                <SafeAreaView style={styles.root}>
                    <View style={styles.header}>
                        <Text style={styles.headerText}>Saved workspaces</Text>
                    </View>
                    <SavedWorkspacesContainer>
                        {this.renderWorkspacesList}
                    </SavedWorkspacesContainer>
                </SafeAreaView>
            </BottomTabs>
        );
    }

    renderWorkspacesList = (workspaces) => {
        if (!workspaces.length) {
            return (
                <View style={styles.noResultsContainer}>
                    <Image
                        resizeMode="contain"
                        style={styles.imageNoResults}
                        source={Images.emptyLikesIcon}
                    />
                    <Text style={styles.textNoResults}>
                        No likes yet, Collect your favorite
                        workspaces here by clicking the&nbsp;
                        <Ionicons name="md-heart" size={Font.size.m} color={Color.grayDark} />.
                    </Text>
                </View>
            );
        }

        return (
            <View style={styles.contentContainer}>
                <ScrollView style={containerFlex}>
                    {workspaces.map((result) => (
                        <WorkspacePreviewItem key={result.workspace.id} {...result} />
                    ))}
                </ScrollView>
            </View>
        );
    }

}
