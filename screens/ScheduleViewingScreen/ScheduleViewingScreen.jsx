import * as React from "react";
import { View } from "react-native";

import { WorkspaceInfoItem, ScheduleViewingForm } from "@components/WorkspaceComponents";
import { AvoidingScrollView } from "@components/partials/AvoidingScrollView";

import { UserContainer } from "@containers/UserContainer";

import { NavigationPropTypes } from "../../navigation/NavigationPropTypes";
import { styles } from "./styles";

export class ScheduleViewingScreen extends React.Component {
	static propTypes = NavigationPropTypes;

	componentWillUnmount() {
		if (this.props.navigation.state.params) {
			delete this.props.navigation.state.params.workspaceInfo;
		}
	}

	render() {
		return (
			<UserContainer>
				{this.renderLayout}
			</UserContainer>
		);
	}

	get workspaceInfo() {
		return JSON.parse(this.props.navigation.getParam("workspaceInfo") || "{}");
	}

	renderLayout = (userData) => (
		<AvoidingScrollView contentContainerStyle={styles.root}>
			<View style={styles.previewContainer}>
				<WorkspaceInfoItem
					type={this.workspaceInfo.type}
					size={this.workspaceInfo.size}
					capacity={this.workspaceInfo.capacity}
					locationName={this.workspaceInfo.locationName}
					coverImageUrl={this.workspaceInfo.coverImageUrl}
					locationAddress={this.workspaceInfo.locationAddress}
				/>
			</View>
			<ScheduleViewingForm
				id={this.workspaceInfo.id}
				onPressDone={this.handleGoBack}
				prefilledPhone={userData.phone}
			/>
		</AvoidingScrollView>
	)

	handleGoBack = () => {
		this.props.navigation.pop();
	}
}
