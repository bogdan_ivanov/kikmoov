import * as React from "react";
import { View } from "react-native";

import { BookFormController, WorkspaceInfoItem } from "@components/WorkspaceComponents";
import { AvoidingScrollView } from "@components/partials/AvoidingScrollView";

import { UserContainer } from "@containers/UserContainer";

import { NavigationPropTypes } from "../../navigation/NavigationPropTypes";

import { styles } from "./styles";

export class BookCreationScreen extends React.Component {
    static propTypes = NavigationPropTypes;

    componentWillUnmount() {
        if (this.props.navigation.state.params) {
            delete this.props.navigation.state.params.workspaceInfo;
        }
    }

    render() {
        return (
            <UserContainer>
                {this.renderLayout}
            </UserContainer>
        );
    }

    get workspaceInfo() {
        return JSON.parse(this.props.navigation.getParam("workspaceInfo") || "{}");
    }

    renderLayout = (userData) => (
        <AvoidingScrollView contentContainerStyle={styles.root}>
            <View style={styles.previewContainer}>
                <WorkspaceInfoItem
                    type={this.workspaceInfo.type}
                    size={this.workspaceInfo.size}
                    capacity={this.workspaceInfo.capacity}
                    locationName={this.workspaceInfo.locationName}
                    coverImageUrl={this.workspaceInfo.coverImageUrl}
                    locationAddress={this.workspaceInfo.locationAddress}
                />
            </View>
            <BookFormController
                id={this.workspaceInfo.id}
                onDone={this.handleGoBack}
                total={this.workspaceInfo.price}

                prefilledPhone={userData.phone}
                prefilledEmail={userData.email}
            />
        </AvoidingScrollView>
    )

    handleGoBack = () => {
        this.props.navigation.pop();
    }

}
