import { StyleSheet, Dimensions } from "react-native";
import { Header } from "react-navigation";

export const styles = StyleSheet.create({
    root: {
        marginLeft: 14,
        marginRight: 14,
        marginTop: 10,

        minHeight: Dimensions.get("window").height - Header.HEIGHT
    },
    previewContainer: {
        paddingLeft: 14,
        paddingRight: 14
    }
});
