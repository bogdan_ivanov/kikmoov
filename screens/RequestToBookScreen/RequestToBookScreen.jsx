import * as React from "react";
import { View } from "react-native";

import { AvoidingScrollView } from "@components/partials/AvoidingScrollView";
import { WorkspaceInfoItem, RequestToBookForm } from "@components/WorkspaceComponents";

import { UserContainer } from "@containers/UserContainer";

import { NavigationPropTypes } from "../../navigation/NavigationPropTypes";

import { styles } from "./styles";

export class RequestToBookScreen extends React.Component {
	static propTypes = NavigationPropTypes;

	componentWillUnmount() {
		if (this.props.navigation.state.params) {
			delete this.props.navigation.state.params.workspaceInfo;
		}
	}

	render() {
		return (
			<UserContainer>
				{this.renderLayout}
			</UserContainer>
		);
	}

	get workspaceInfo() {
		return JSON.parse(this.props.navigation.getParam("workspaceInfo") || "{}");
	}

	renderLayout = (userData) => (
		<AvoidingScrollView contentContainerStyle={styles.root}>
			<View style={styles.previewContainer}>
				<WorkspaceInfoItem
					type={this.workspaceInfo.type}
					size={this.workspaceInfo.size}
					capacity={this.workspaceInfo.capacity}
					locationName={this.workspaceInfo.locationName}
					coverImageUrl={this.workspaceInfo.coverImageUrl}
					locationAddress={this.workspaceInfo.locationAddress}
				/>
			</View>
			<RequestToBookForm
				id={this.workspaceInfo.id}
				onPressDone={this.handleGoBack}

				prefilledName={`${userData.firstname} ${userData.lastname}`}
				prefilledPhone={userData.phone}
				prefilledEmail={userData.email}
			/>
		</AvoidingScrollView>
	)

	handleGoBack = () => {
		this.props.navigation.pop();
	}
}
