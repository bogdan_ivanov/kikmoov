import * as React from "react";
import { Constants } from "expo";
import PropTypes from "prop-types";
import {
	Text,
	View,
	Keyboard,
	Animated,
	Platform,
	StyleSheet,
	Dimensions,
	TouchableWithoutFeedback
} from "react-native";

import { Images } from "@assets/Images";
import { Color, Font } from "@constants/UI";

const IMAGE_RATIO = 0.6851851852;
/* eslint-disable no-magic-numbers */
export const COLLAPSED_HEIGHT = Dimensions.get("screen").height * .1125;
export const BOUNCE_RATE = COLLAPSED_HEIGHT * .2666;
export const IMAGE_HEIGHT_PERCENTAGE = .4;
export const ADDITIONAL_SPACE = 13;
export const isNotSafeScreen = Constants.statusBarHeight > 25;
let backgroundHeight = Dimensions.get("window").height * IMAGE_HEIGHT_PERCENTAGE;
if (isNotSafeScreen) {
	backgroundHeight = Platform.select({
		android: backgroundHeight + Constants.statusBarHeight * 1.5 - ADDITIONAL_SPACE,
		ios: backgroundHeight - Constants.statusBarHeight + ADDITIONAL_SPACE
	});
}
export const BACKGROUND_HEIGHT = backgroundHeight;
/* eslint-enable no-magic-numbers */

export class AnimatedBackground extends React.Component {
	static propTypes = {
		animationValue: PropTypes.object.isRequired,
	};

	shouldComponentUpdate() {
		return false;
	}

	render() {
		return (
			<TouchableWithoutFeedback accessible={false} onPress={Keyboard.dismiss}>
				<Animated.View style={[styles.root, { transform: [{ translateY: this.scrollTrasnlateY }] }]}>
					<View style={styles.backgroundWrapper}>
						<View style={styles.view}>
							<Animated.Image
								resizeMode="stretch"
								source={Images.homeBackground}
								style={[styles.image, { opacity: this.opacity }]}
							/>
							<Animated.View
								style={[
									styles.headerTitle,
									{
										opacity: this.opacity,
										transform: [{ scaleX: .65 }, { translateY: this.titleTrasnlateY }]
									}
								]}
							>
								<Text style={styles.header}>Find your perfect workspace.</Text>
								<Text style={styles.header}>Innovate. Create.</Text>
							</Animated.View>
						</View>
						<View style={styles.bottomChildrenContainer}>
							{this.props.children}
						</View>
					</View>
				</Animated.View>
			</TouchableWithoutFeedback>
		);
	}

	get opacity() {
		return this.props.animationValue.interpolate({
			inputRange: [0, COLLAPSED_HEIGHT / 2 + BOUNCE_RATE, COLLAPSED_HEIGHT],
			outputRange: [1, 0, 0]
		});
	}

	get titleTrasnlateY() {
		return this.props.animationValue.interpolate({
			inputRange: [-COLLAPSED_HEIGHT * 2, 0, COLLAPSED_HEIGHT, COLLAPSED_HEIGHT * 2],
			outputRange: [BOUNCE_RATE, 0, -COLLAPSED_HEIGHT / 2, -COLLAPSED_HEIGHT + BOUNCE_RATE]
		});
	}

	get scrollTrasnlateY() {
		return this.props.animationValue.interpolate({
			inputRange: [-COLLAPSED_HEIGHT * 2, 0, COLLAPSED_HEIGHT, COLLAPSED_HEIGHT * 2],
			outputRange: [0, -BOUNCE_RATE, -COLLAPSED_HEIGHT, -(COLLAPSED_HEIGHT + BOUNCE_RATE)],
			extrapolate: "clamp"
		});
	}
}

const styles = StyleSheet.create({
	root: {
		position: "absolute",

		top: 0,
		left: 0
	},
	bottomChildrenContainer: {
		width: 180,
		height: 40,

		position: "absolute"
	},
	header: {
		fontFamily: Font.type.semiBold,
		fontSize: Font.size.l,

		color: "#fff",

		textShadowColor: "rgba(0, 0, 0, 0.5)",
		textShadowOffset: {
			width: 0,
			height: 2,
		},
		textShadowRadius: 3,

		lineHeight: 25
	},
	headerTitle: {
		justifyContent: "center",
		alignItems: "center",

		top: "27%"
	},
	backgroundWrapper: {
		height: BACKGROUND_HEIGHT,

		justifyContent: "flex-end",

		position: "relative",

		width: "100%",

		alignItems: "center",

		paddingBottom: 15
	},
	view: {
		height: Dimensions.get("screen").width * IMAGE_RATIO,
		width: Dimensions.get("screen").width,

		backgroundColor: Color.red,

		borderBottomRightRadius: Dimensions.get("screen").width / 2,
		borderBottomLeftRadius: Dimensions.get("screen").width / 2,

		transform: [{ scaleX: 1.5 }],

		overflow: "hidden",

		position: "relative"
	},
	image: {
		position: "absolute",

		top: 0,
		left: 0,

		height: "101%",
		width: "100%",

		transform: [{ scaleX: .73 }]
	}
});
