import { StyleSheet, Dimensions, Platform } from "react-native";
import { Constants } from "expo";

import { shadowArea, containerStatic, containerFlex, Font, Color, buttonText, filledButton } from "@constants/UI";

import { COLLAPSED_HEIGHT, BACKGROUND_HEIGHT, isNotSafeScreen, ADDITIONAL_SPACE } from "./AnimatedBackground";

export const styles = StyleSheet.create({
    container: {
        flex: 1,

        position: "relative"
    },
    inputContainer: {
        ...containerStatic,

        position: "absolute",

        width: "100%",

        /* eslint-disable-next-line no-magic-numbers */
        top: BACKGROUND_HEIGHT * .5
            + (isNotSafeScreen
                ? Platform.select({ ios: ADDITIONAL_SPACE * 2, android: ADDITIONAL_SPACE })
                : 0)
    },
    headerSection: {
        ...shadowArea,
        ...containerStatic,

        paddingBottom: 15,

        borderTopColor: "transparent"
    },
    navHeader: {
        position: "absolute",
        top: 0,
        left: 0,
        right: 0,

        paddingBottom: 10,
        marginTop: 5,
        /* eslint-disable-next-line no-magic-numbers */
        paddingTop: Constants.statusBarHeight || 20
    },
    searchButton: {
        marginTop: -3,
        marginRight: -3,
        marginBottom: 5,
        marginLeft: 4,

        backgroundColor: Color.red,

        justifyContent: "center",
        alignItems: "center",

        borderRadius: 20
    },
    searchText: {
        fontFamily: Font.type.medium,
        fontSize: Font.size.s,

        paddingLeft: 6,
        paddingRight: 6,

        paddingBottom: Platform.select({ ios: 0, android: 1 }),

        color: "#fff"
    },
    input: {
        width: "100%"
    },
    contentContainerStyle: {
        paddingTop: BACKGROUND_HEIGHT,

        minHeight: Dimensions.get("window").height + COLLAPSED_HEIGHT
    },
    areasContainer: {
        ...containerFlex,

        marginTop: 40
    },
    mapButtonText: {
        ...buttonText,

        flex: 2,

        fontFamily: Font.type.semiBold,

        textAlign: "center",

        paddingRight: 20,

        color: "#fff"
    },
    mapButton: {
        ...filledButton,

        flexDirection: "row",

        borderRadius: 40,

        marginTop: 15,
        marginBottom: 15,

        backgroundColor: Color.red
    },
    iconMap: {
        flex: 0,

        marginLeft: 20,

        width: 35,
        height: 35
    }
});
