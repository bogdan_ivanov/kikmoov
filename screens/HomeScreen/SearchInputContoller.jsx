import * as React from "react";
import PropTypes from "prop-types";
import { withNavigation } from "react-navigation";

import { NavigationPropTypes } from "../../navigation/NavigationPropTypes";

@withNavigation
export class SearchInputController extends React.Component {
	static propTypes = {
		...NavigationPropTypes,

		onSubmit: PropTypes.func.isRequired
	};

	constructor(props) {
		super(props);

		this.state = {
			searchValue: "",
			searchValueType: "address"
		};

		this.didBlurListener = props.navigation.addListener("didBlur", () => {
			this.setState({ searchValue: "" });
		});
	}

	componentWillUnmount() {
		this.didBlurListener.remove();
	}

	render() {
		return this.props.children(this.state, this.handleSearchChange, this.handleSubmit);
	}

	handleSearchChange = (searchValue, searchValueType) => {
        this.setState({ searchValue, searchValueType });
    }

	handleSubmit = () => {
		this.props.onSubmit(this.state);
	}
}
