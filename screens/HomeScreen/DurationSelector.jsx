import * as React from "react";
import PropTypes from "prop-types";
import GestureRecognizer from "react-native-swipe-gestures";
import { Text, TouchableOpacity, StyleSheet, Animated, Platform } from "react-native";

import { FilterTypes, FilterLabels } from "@constants/API";

import { Font, Color, shadowArea } from "@constants/UI";

const Offset = {
	MONTHLY: 0,
	HOURLY: 91
};

export class DurationSelector extends React.Component {
	static propTypes = {
		onChange: PropTypes.func
	};

	slide = new Animated.Value(Offset.MONTHLY);
	opacity = new Animated.Value(1);

	state = {
		selectedValue: FilterTypes.DURATION.MONTHLY,
	};

	render() {
		return (
			<GestureRecognizer
				onSwipeLeft={this.animate(FilterTypes.DURATION.MONTHLY)}
				onSwipeRight={this.animate(FilterTypes.DURATION.HOURLY)}
				style={styles.container}
			>
				<Animated.View
					style={[
						styles.slideHandle,
						{ transform: [{ translateX: this.slide }], opacity: this.opacity }
					]}
				/>
				<TouchableOpacity
					hitSlop={{ top: 10, left: 10, right: 10, bottom: 10 }}
					activeOpacity={this.isActive("MONTHLY") ? 1 : .2}
					style={styles.button}
					onPress={!this.isActive("MONTHLY")
						? this.animate(FilterTypes.DURATION.MONTHLY)
						: undefined
					}
				>
					<Text style={[styles.text, this.isActive("MONTHLY") && styles.textActive]}>
						{FilterLabels.DURATION.MONTHLY}
					</Text>
				</TouchableOpacity>
				<TouchableOpacity
					hitSlop={{ top: 10, left: 10, right: 10, bottom: 10 }}
					activeOpacity={this.isActive("HOURLY") ? 1 : .2}
					style={styles.button}
					onPress={!this.isActive("HOURLY")
						? this.animate(FilterTypes.DURATION.HOURLY)
						: undefined
					}
				>
					<Text style={[styles.text, this.isActive("HOURLY") && styles.textActive]}>
						{FilterLabels.DURATION.HOURLY}
					</Text>
				</TouchableOpacity>
			</GestureRecognizer>
		);
	}

	isActive = (key) => {
		return this.state.selectedValue === FilterTypes.DURATION[key];
	}

	animate = (selectedValue) => () => {
		Animated.parallel([
			Animated.timing(
				this.opacity,
				{
					toValue: 1,
					useNativeDriver: true,
					duration: 100
				}
			),
			Animated.spring(
				this.slide,
				{
					toValue: Offset[selectedValue.toUpperCase()],
					useNativeDriver: true,
					speed: 50,
					bounciness: 4
				}
			)
		]).start();
		this.setState({ selectedValue }, () => {
			this.props.onChange && this.props.onChange(this.state.selectedValue);
		});
	}
}

const styles = StyleSheet.create({
	container: {
		...shadowArea,

		justifyContent: "space-between",
		alignItems: "stretch",
		flexDirection: "row",
		flexWrap: "wrap",
		flex: 1,

		backgroundColor: "#fff",

		borderRadius: 25,

		padding: 5,

		position: "relative"
	},
	button: {
		width: "45%",

		justifyContent: "center"
	},
	text: {
		fontFamily: Font.type.medium,
		fontSize: Font.size.s,

		color: Color.gray,

		marginBottom: Platform.select({ ios: 0, android: 1 }),

		textAlign: "center"
	},
	textActive: {
		color: "#fff"
	},
	slideHandle: {
		position: "absolute",

		top: 4.5,
		left: 6,

		width: "45%",
		height: "100%",

		borderRadius: 25,

		backgroundColor: Color.red
	}
});
