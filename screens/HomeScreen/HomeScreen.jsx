import Axios from "axios";
import * as React from "react";
import { Toaster } from "react-native-toastboard";
import {
    TouchableWithoutFeedback,
    InteractionManager,
    TouchableOpacity,
    StatusBar,
    Keyboard,
    Animated,
    Image,
    View,
    Text
} from "react-native";

import { withAnimatedScroll } from "@utils/withAnimatedScroll";
import { getPopularAreas } from "@statelessActions";

import { AvoidingScrollView } from "@components/partials/AvoidingScrollView";
import { SearchInput } from "@components/partials/SearchInput";

import { BottomTabs } from "@containers/BottomTabs";

import { MenuButton } from "@components/NavigationComponents";
import { PopularAreas } from "@components/PopularAreas";
import { FilterTypes } from "@constants/API";
import { Images } from "@assets/Images";

import { NavigationPropTypes } from "../../navigation/NavigationPropTypes";

import { AnimatedBackground, COLLAPSED_HEIGHT, BOUNCE_RATE, isNotSafeScreen } from "./AnimatedBackground";
import { SearchInputController } from "./SearchInputContoller";
import { DurationSelector } from "./DurationSelector";
import { styles } from "./styles";

const defaultAreasCount = 5;
@withAnimatedScroll()
export class HomeScreen extends React.Component {
    static propTypes = NavigationPropTypes;

    static navigationOptions = ({ navigation }) => ({
        headerTransparent: true,
        header: (
            <View style={styles.navHeader}>
                <MenuButton white onPress={navigation.openDrawer} />
            </View>
        )
    });

    constructor(props) {
        super(props);

        this.cancelToken = Axios.CancelToken.source();
        this.state = {
            loading: true,
            areas: (new Array(defaultAreasCount)).fill({}),

            duration: FilterTypes.DURATION.MONTHLY
        };

        this.willBlurListener = props.navigation.addListener("willBlur", () => {
            StatusBar.setBarStyle("dark-content", true);
        });

        this.didBlurListener = props.navigation.addListener("didBlur", () => {
            this.scroller && this.scroller.getNode().scrollTo({ y: 0, animated: false });
        });

        this.willFocusListener = props.navigation.addListener("willFocus", () => {
            StatusBar.setBarStyle("light-content", true);

            if (this.props.navigation.state.params && this.props.navigation.state.params.focus) {
                this.input.focus();

                delete this.props.navigation.state.params.focus;
            }
        });
    }

    componentDidMount() {
        this.cancellableLoading = InteractionManager.runAfterInteractions(async () => {
            let response;
            try {
                response = await getPopularAreas(this.cancelToken.token);
            } catch (error) {
                if (Axios.isCancel(error)) {
                    return;
                }

                return Toaster.error(error);
            }

            this.setState({
                loading: false,
                areas: response.data.results.map(({ workspace, location }) => ({
                    id: workspace.id,
                    title: location.name,
                    imgSource: workspace.coverImageUrl || (workspace.images[0] && workspace.images[0].url)
                }))
            });
        });
    }

    componentWillUnmount() {
        this.cancellableLoading && this.cancellableLoading.cancel();
        this.didBlurListener.remove();
        this.willBlurListener.remove();
        this.willFocusListener.remove();
        this.cancelToken.cancel();
    }

    render() {
        return (
            <BottomTabs>
                <View style={styles.container}>
                    <AvoidingScrollView
                        keyboardShouldPersistTaps="handled"
                        showsVerticalScrollIndicator={false}

                        scrollRef={this.getScroller}
                        onScroll={this.handleScroll}
                        scrollEnabled={!this.state.loading}

                        bounces={!isNotSafeScreen}

                        style={styles.container}
                        contentContainerStyle={styles.contentContainerStyle}
                    >
                        <TouchableWithoutFeedback accessible={false} onPress={Keyboard.dismiss}>
                            <View style={styles.areasContainer}>
                                <PopularAreas areas={this.state.areas} />
                                <TouchableOpacity style={styles.mapButton} onPress={this.navigateMap}>
                                    <Image resizeMode="contain" style={styles.iconMap} source={Images.iconMap} />
                                    <Text style={styles.mapButtonText}>Open workspace map</Text>
                                </TouchableOpacity>
                            </View>
                        </TouchableWithoutFeedback>
                    </AvoidingScrollView>
                    <AnimatedBackground animationValue={this.value}>
                        <DurationSelector onChange={this.handleDurationChange} />
                    </AnimatedBackground>
                    <Animated.View
                        style={[styles.inputContainer, { transform: [{ translateY: this.scrollTrasnlateY }] }]}
                    >
                        <SearchInputController onSubmit={this.handleSubmit}>
                            {(state, onChange, onSubmit) => (
                                <SearchInput
                                    valueType={state.searchValueType}
                                    onSubmitEditing={onSubmit}
                                    value={state.searchValue}
                                    createRef={this.getInput}
                                    onChangeText={onChange}
                                    returnKeyType="search"
                                    style={styles.input}
                                >
                                    <TouchableOpacity onPress={onSubmit} style={styles.searchButton}>
                                        <Text style={styles.searchText}>Search</Text>
                                    </TouchableOpacity>
                                </SearchInput>
                            )}
                        </SearchInputController>
                    </Animated.View>
                </View>
            </BottomTabs>
        );
    }

    get scrollTrasnlateY() {
        return this.value.interpolate({
            inputRange: [-COLLAPSED_HEIGHT * 2, 0, COLLAPSED_HEIGHT, COLLAPSED_HEIGHT * 2],
            outputRange: [0, -BOUNCE_RATE, -COLLAPSED_HEIGHT, -(COLLAPSED_HEIGHT + BOUNCE_RATE)],
            extrapolate: "clamp"
        });
    }

    getInput = (ref) => this.input = ref;

    getScroller = (ref) => this.scroller = ref

    handleSubmit = (state) => {
        this.props.navigation.navigate("Search", {
            value: state.searchValue,
            type: state.searchValueType,
            duration: this.state.duration
        });
    }

    navigateMap = () => {
        this.props.navigation.navigate("Map", {
            renderWith: "tooltips"
        });
    }

    handleDurationChange = (duration) => this.setState({ duration })
}
