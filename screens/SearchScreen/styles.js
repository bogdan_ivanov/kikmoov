import { StyleSheet } from "react-native";

import { containerFlex, shadowArea } from "@constants/UI";

export const styles = StyleSheet.create({
    root: {
        flex: 1,

        position: "relative"
    },
    openMapButton: {
        position: "absolute",

        bottom: 20,
        right: 20
    },
    openMapIcon: {
        width: 65,
        height: 65
    },
    itemWrap: {
        ...containerFlex,
        ...shadowArea,

        backgroundColor: "#fff",

        marginBottom: 15
    }
});
