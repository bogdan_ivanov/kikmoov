import * as React from "react";
import { SafeAreaView, View, TouchableOpacity, Image, StatusBar } from "react-native";

import { Images } from "@assets/Images";

import { MenuButton } from "@components/NavigationComponents";
import { WorkspacePreviewItem } from "@components/WorkspaceComponents";
import {
    SortView,
    FiltersView,
    SearchResults,
    SearchProvider,
    SearchControls,
    withSearchControlsProvider
} from "@components/SearchComponents";

import { Color } from "@constants/UI";
import { BottomTabs } from "@containers/BottomTabs";

import { NavigationPropTypes } from "../../navigation/NavigationPropTypes";

import { styles } from "./styles";
import { composeMapData } from "./utils/composeMapData";

@withSearchControlsProvider
export class SearchScreen extends React.PureComponent {
    static propTypes = NavigationPropTypes;

    static navigationOptions = ({ navigation }) => ({
        headerStyle: {
            backgroundColor: Color.grayDark,
        },
        headerRight: <SearchControls />,
        headerLeft: <MenuButton white onPress={navigation.openDrawer} />
    });

    constructor(props) {
        super(props);

        this.didBlurListener = props.navigation.addListener("didBlur", () => {
            StatusBar.setBarStyle("dark-content", true);
        });

        this.willFocusListener = props.navigation.addListener("willFocus", () => {
            StatusBar.setBarStyle("light-content", true);
        });
    }

    componentWillUnmount() {
        this.didBlurListener.remove();
        this.willFocusListener.remove();
    }

    render() {
        return (
            <BottomTabs>
                <SearchProvider
                    initialValue={this.props.navigation.getParam("value")}
                    initialValueType={this.props.navigation.getParam("type")}
                    initialDuration={this.props.navigation.getParam("duration")}
                    topLayoutChildren={<SortView />}
                >
                    <FiltersView />
                    <SafeAreaView style={styles.root}>
                        <SearchResults
                            style={{ flex: 1, backgroundColor: Color.darkWhite }}
                            outerContainerChildren={this.renderOpenMapButton}
                            contentContainerStyle={{ paddingBottom: 85 }}
                        >
                            {this.renderResults}
                        </SearchResults>
                    </SafeAreaView>
                </SearchProvider>
            </BottomTabs>
        );
    }

    renderOpenMapButton = (results) => {
        return (
            <TouchableOpacity style={styles.openMapButton} onPress={this.handleOpenMap(results)}>
                <Image style={styles.openMapIcon} source={Images.openMap} />
            </TouchableOpacity>
        );
    }

    renderResults = ({ item }) => (
        <View key={item.workspace.id} style={styles.itemWrap}>
            <WorkspacePreviewItem
                style={{ marginBottom: 10 }}
                workspace={item.workspace}
                location={item.location}
                related={item.related}
            />
        </View>
    )

    handleOpenMap = (results) => () => {
        this.props.navigation.navigate("Map", {
            regions: composeMapData(results),
            renderWith: "tooltips"
        });
    }
}
