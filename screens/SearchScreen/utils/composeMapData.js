export function composeMapData(results) {
	return results
		.reduce((unique, item) => unique.find(({ location }) => (
			item.location.latitude === location.latitude
			&& item.location.longitude === location.longitude
		)) ? unique : [...unique, item], [])
		.map(({ location, workspace, related }) => ({
			latitude: location.latitude,
			longitude: location.longitude,
			workspaceData: {
				workspace,
				related,
				location: {
					name: location.name,
					address: location.address,
				}
			}
		}));
}
