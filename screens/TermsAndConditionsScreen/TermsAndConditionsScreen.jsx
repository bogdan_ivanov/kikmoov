import shortId from "shortid";
import * as React from "react";
import { SafeAreaView, View, Text, ScrollView } from "react-native";

import { NavigationPropTypes } from "../../navigation/NavigationPropTypes";

import { styles } from "./styles";

const texts = require("./texts.json");

export class TermsAndConditionsScreen extends React.PureComponent {
	static propTypes = NavigationPropTypes

	componentWillUnmount() {
		if (this.props.navigation.state.params) {
			delete this.props.navigation.state.params.textType;
		}
	}

	render() {
		return (
			<SafeAreaView style={styles.root}>
				<View style={styles.header}>
					<Text style={styles.headerText}>
						{this.headerLabel}
					</Text>
				</View>
				<ScrollView contentContainerStyle={styles.contentContainer}>
					{this.renderParagraph()}
				</ScrollView>
			</SafeAreaView>
		);
	}

	get headerLabel() {
		return this.type === "terms" ? "Terms & conditions" : "Privacy policy";
	}

	get type() {
		return this.props.navigation.getParam("textType") || "terms";
	}

	renderParagraph = () => {
		return texts[this.type].map((paragraph) => (
			<Text style={styles.paragraph} key={shortId.generate()}>{paragraph}</Text>
		));
	}
}
