import { StyleSheet } from "react-native";

import { Color, Font } from "@constants/UI";

export const styles = StyleSheet.create({
	root: {
		flex: 1
	},
	header: {
		paddingLeft: 15,
		paddingRight: 15,
		paddingTop: 5,
		paddingBottom: 7,

		borderColor: "transparent",
		borderWidth: 1,

		borderBottomColor: Color.grayLight
	},
	headerText: {
		fontSize: Font.size.l,
		fontFamily: Font.type.medium
	},
	contentContainer: {
		padding: 15
	},
	paragraph: {
		fontSize: Font.size.m,
		fontFamily: Font.type.medium,

		marginBottom: Font.size.l
	}
});
