import * as React from "react";
import { View, Text, Image } from "react-native";
import Touchable from "react-native-platform-touchable";

import { Images } from "@assets/Images";

import { AvoidingScrollView } from "@components/partials/AvoidingScrollView";
import {
    ProfileDetailsPropvider,

    PersonalDetailsForm,
    DeleteAccount,
    Notifications,
    PrivacyPolicy,
    FAQ
} from "@components/ProfileComponents";

import { styles } from "./styles";
import { NavigationPropTypes } from "../../navigation/NavigationPropTypes";

export class ProfileDetailsScreen extends React.Component {
    static propTypes = NavigationPropTypes;

    render() {
        return (
            <ProfileDetailsPropvider>
                <AvoidingScrollView>
                    <View style={styles.root}>
                        <View style={styles.personalDetailsBlock}>
                            <Text style={styles.headerTitle}>Personal details</Text>
                            <PersonalDetailsForm />
                        </View>
                        <Notifications />
                        <Touchable style={styles.button} onPress={this.handleNavigate("ResetPassword")}>
                            <React.Fragment>
                                <Image resizeMode="stretch" style={styles.resetIcon} source={Images.iconReset} />
                                <Text style={styles.buttonText}>
                                    Reset password
                                </Text>
                            </React.Fragment>
                        </Touchable>
                        <FAQ />
                        <PrivacyPolicy />
                        <Touchable style={styles.button} onPress={this.handleNavigate("ContactUs")}>
                            <React.Fragment>
                                <Image
                                    resizeMode="stretch"
                                    style={styles.iconContactUs}
                                    source={Images.iconContactUs}
                                />
                                <Text style={styles.buttonText}>
                                    Contact us
                                </Text>
                            </React.Fragment>
                        </Touchable>
                        <DeleteAccount />
                    </View>
                </AvoidingScrollView>
            </ProfileDetailsPropvider>
        );
    }

    handleNavigate = (route) => () => {
        this.props.navigation.navigate(route);
    }
}
