import { StyleSheet } from "react-native";

import { shadowArea, containerFlex, Font, filledButton, Color, containerStatic, buttonText } from "@constants/UI";

export const styles = StyleSheet.create({
    root: {
        ...containerFlex,

        paddingTop: 20,
        paddingBottom: 20,
    },
    personalDetailsBlock: {
        ...shadowArea,
        padding: 20,

        borderRadius: 5,

        marginBottom: 10
    },
    headerTitle: {
        fontFamily: Font.type.medium,
        fontSize: Font.size.l,
    },
    button: {
        ...shadowArea,
        ...filledButton,
        ...containerStatic,

        alignItems: "center",
        flexDirection: "row",
        justifyContent: "flex-start",

        marginBottom: 10,

        paddingTop: 15,
        paddingBottom: 15
    },
    buttonText: {
        ...buttonText,
        fontSize: Font.size.l,

        color: Color.red
    },
    iconContactUs: {
        width: 20,
        height: 20,

        marginRight: 10
    },
    resetIcon: {
        width: 20.4,
        height: 19,

        marginRight: 10
    }
});
