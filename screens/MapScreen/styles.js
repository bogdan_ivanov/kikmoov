import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
	root: {
		flex: 1,
	},
	map: {
		flex: 1
	},
	closeButton: {
		width: 39.25,
		height: 39.25
	},
	marker: {
		width: 45.2898,
		height: 52.5361
	},
	header: {
		position: "absolute",
		top: 0,
		left: 0,
		right: 0,

		padding: 10
	}
});
