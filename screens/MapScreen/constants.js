import { delta as deltaUtil } from "@components/MapComponents/utils/delta";

export const initialZoom = .999;

export const AreaLimits = {
	northEast: {
		latitude: 51.700665,
		longitude: 0.302216
	},
	southWest: {
		latitude: 51.263284,
		longitude: -0.549065
	}
};

export const delta = deltaUtil(initialZoom);

export const mapBoundaries = [
	{
		latitude: AreaLimits.northEast.latitude - (delta / 2),
		longitude: AreaLimits.northEast.longitude - (delta / 2)
	},
	{
		latitude: AreaLimits.southWest.latitude + (delta / 2),
		longitude: AreaLimits.southWest.longitude + (delta / 2)
	}
];
