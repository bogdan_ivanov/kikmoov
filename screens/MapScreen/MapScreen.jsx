import Axios from "axios";
import * as React from "react";
import shortId from "shortid";
import { MapView, ScreenOrientation } from "expo";
import { Toaster } from "react-native-toastboard";
import { View, Image, ActivityIndicator } from "react-native";

import { Color } from "@constants/UI";
import { Images } from "@assets/Images";
import { search } from "@statelessActions";
import { SortTypes, FilterTypes } from "@constants/API";
import { StatusBarManager } from "@utils/StatusBarManager";

import { delta } from "@components/MapComponents/utils/delta";
import { MapTooltip } from "@components/MapComponents";

import { styles } from "./styles";
import { AreaLimits, initialZoom } from "./constants";
import { composeMapData } from "../SearchScreen/utils/composeMapData";

import { navigationOptions } from "./navigationOptions";
import { NavigationPropTypes } from "../../navigation/NavigationPropTypes";

export class MapScreen extends React.PureComponent {
    static propTypes = NavigationPropTypes;
    static navigationOptions = navigationOptions;

    constructor(props) {
        super(props);

        this.map = undefined;
        this.delta = delta(initialZoom);
        this.cancelToken = Axios.CancelToken.source();
        this.state = { loaded: false, updateImage: true };

        this.willFocusListener = props.navigation.addListener("willFocus", async () => {
            ScreenOrientation.allowAsync(ScreenOrientation.Orientation.ALL);
            StatusBarManager.setState("hidden");

            if (!this.props.navigation.getParam("regions")) {
                let response;
                try {
                    response = await search(
                        {
                            duration: FilterTypes.DURATION.MONTHLY,
                            date: SortTypes.DATE.NEWEST,
                            availableFrom: Date.now(),
                            facilities: [],
                            minPrice: 0,
                            address: ""
                        },
                        this.cancelToken.token
                    );
                } catch (error) {
                    if (Axios.isCancel(error)) {
                        StatusBarManager.restoreState();
                        return;
                    }

                    return Toaster.error(error);
                }
                /* eslint-disable-next-line no-magic-numbers */
                this.setState({ loaded: true, regions: composeMapData(response.data.results.slice(0, 30)) });
            } else {
                this.setState({ loaded: true, regions: this.props.navigation.getParam("regions") });
            }
        });

        this.willBlurListener = props.navigation.addListener("willBlur", () => {
            this.setState({ loaded: false });
            StatusBarManager.restoreState();
            ScreenOrientation.allowAsync(ScreenOrientation.Orientation.PORTRAIT_UP);
        });
    }

    componentWillUnmount() {
        this.cancelToken.cancel();

        ScreenOrientation.allowAsync(ScreenOrientation.Orientation.PORTRAIT_UP);

        this.willBlurListener.remove();
        this.willFocusListener.remove();

        if (this.props.navigation.state.params) {
            delete this.props.navigation.state.params.regions;
            delete this.props.navigation.state.params.renderWith;
        }
    }

    render() {
        if (!this.state.loaded) {
            return (
                <View style={[styles.root, { justifyContent: "center" }]}>
                    <ActivityIndicator color={Color.gray} size="large" />
                </View>
            );
        }

        return (
            <View style={styles.root}>
                <MapView
                    showsCompass={false}
                    showsMyLocationButton={false}

                    ref={(map) => this.map = map}
                    onMapReady={this.bound}
                    onRegionChange={this.handleRegionChange}

                    minZoomLevel={9}
                    provider={MapView.PROVIDER_GOOGLE}
                    initialRegion={this.getRegion(this.state.regions[0])}

                    style={styles.map}
                >
                    {this.props.navigation.getParam("renderWith") === "tooltips"
                        ? this.renderTooltips
                        : this.renderMarkers
                    }
                </MapView>
            </View>
        );
    }

    get renderTooltips() {
        if (this.state.regions.some(({ workspaceData }) => !workspaceData)) {
            return null;
        }

        return this.state.regions.map(({ latitude, longitude, workspaceData }) => (
            <MapTooltip
                {...workspaceData}
                key={shortId.generate()}
                coordinate={this.getRegion({ latitude, longitude })}
            />
        ));
    }

    get renderMarkers() {
        return this.state.regions.map(({ latitude, longitude }) => (
            <MapView.Marker
                key={shortId.generate()}
                tracksViewChanges={false}
                coordinate={this.getRegion({ latitude, longitude })}
            >
                <Image onLoad={this.handleMarkerLoad} style={styles.marker} source={Images.locationMarker} />
            </MapView.Marker>
        ));
    }

    /* Dirty hack. On Android marker image does not visible after first render (cache issue?)
       https://github.com/react-native-community/react-native-maps/issues/100
    */
    handleMarkerLoad = () => {
        if (!this.state.updateImage) {
            return;
        }

        this.setState({ updateImage: false });
    }

    getRegion = (region) => ({
        latitude: Number(region.latitude),
        longitude: Number(region.longitude),
        latitudeDelta: delta(initialZoom),
        longitudeDelta: delta(initialZoom)
    })

    bound = () => {
        if (this.state.bounded) {
            return;
        }

        this.map.fitToElements();
        this.map.setMapBoundaries(
            {
                latitude: AreaLimits.northEast.latitude - (this.delta / 2),
                longitude: AreaLimits.northEast.longitude - (this.delta / 2)
            },
            {
                latitude: AreaLimits.southWest.latitude + (this.delta / 2),
                longitude: AreaLimits.southWest.longitude + (this.delta / 2)
            }
        );

        this.setState({ bounded: true });
    }

    handleRegionChange = ({ longitudeDelta }) => {
        if (this.delta === longitudeDelta) {
            return;
        }

        this.delta = longitudeDelta;
        this.bound();
    }
}
