import { DeskTypes, WorkspaceTypes } from "@constants/API";
import { DateManager } from "@utils/DateManager";

export class BookingScreenManager {
	static canCancel = function (bookingItem) {
		if (!bookingItem || !bookingItem.booking) {
			return false;
		}

		return bookingItem.booking.status !== "payment-received"
			&& bookingItem.booking.future;
	}

	static canInviteAttendee = function (bookingItem) {
		if (!bookingItem) {
			return false;
		}

		return bookingItem.itemType !== "viewing"
			&& !this.isRequestToBook;
	}

	static isRequestToBook = function (workspace) {
		if (!workspace) {
			return false;
		}

		return workspace.type === WorkspaceTypes["private-office"]
			|| workspace.deskType === DeskTypes.monthly_fixed_desk
			|| workspace.deskType === DeskTypes.monthly_hot_desk;
	}

	static getItemData = function (bookingItem, list) {
		return list.find((item) => item[bookingItem.itemType] && item[bookingItem.itemType].id === bookingItem.itemId);
	}

	static getDates = function (startTimeValue, endTimeValue) {
		return DateManager.parsedDateTimeFromTimestamp(
			DateManager.fromUNIX(startTimeValue),
			endTimeValue ? DateManager.fromUNIX(endTimeValue) : undefined
		);
	}
}
