import { StyleSheet } from "react-native";

import { shadowArea, Color, containerStatic, borderButton, buttonText, image, Font } from "@constants/UI";

export const styles = StyleSheet.create({
    root: {
        flex: 1,
    },
    scroll: {
        ...containerStatic,
    },
    shadowContainer: {
        padding: 1,
        paddingBottom: 4,

        marginBottom: 10
    },
    shadow: {
        ...shadowArea,

        borderRadius: 5,
    },
    paddingContainer: {
        padding: 12
    },
    filledContainer: {
        backgroundColor: Color.darkWhite
    },
    typeText: {
        color: Color.red,

        fontSize: Font.size.s,
        fontFamily: Font.type.medium
    },
    locationNameText: {
        fontSize: Font.size.l,
        fontFamily: Font.type.medium
    },
    locationAddressText: {
        fontSize: Font.size.s,
        fontFamily: Font.type.medium,

        color: Color.gray
    },
    image: {
        ...image,

        marginBottom: 7
    },
    horizontalSeparator: {
        backgroundColor: Color.grayLight,

        height: 1,
        width: "100%"
    },
    providerIcon: {
        width: 25,
        height: 25,

        marginRight: 12
    },
    providerText: {
        fontFamily: Font.type.regular,
        fontSize: Font.size.m,

        maxWidth: "90%",

        flex: 1
    },
    row: {
        flexDirection: "row",
        alignItems: "center",

        paddingTop: 6,
        paddingBottom: 6
    },
    spaceBetween: {
        justifyContent: "space-between",

        paddingTop: 0,
        paddingBottom: 0
    },
    circle: {
        width: 20,
        height: 20,

        borderRadius: 10,

        backgroundColor: Color.red,

        justifyContent: "center",
        alignItems: "center",

        marginLeft: 3,
        marginRight: 7
    },
    circleText: {
        color: "#fff",

        fontFamily: Font.type.medium,
        fontSize: Font.size.s,

        textAlign: "center"
    },
    emailItemText: {
        color: Color.grayDark,

        fontFamily: Font.type.regular,
        fontSize: Font.size.m,
    },
    buttonText: {
        ...buttonText,

        color: Color.red
    },
    cancelButton: {
        ...borderButton,

        paddingTop: 3,
        paddingBottom: 3,

        marginRight: 15,

        borderColor: Color.redError
    },
    cancelButtonText: {
        ...buttonText,

        color: Color.redError
    },
    infoFooterTextLeft: {
        fontFamily: Font.type.regular,
        fontSize: Font.size.m,

        color: Color.gray
    },
    infoFooterTextRight: {
        fontFamily: Font.type.medium,
        fontSize: Font.size.m
    },
    termsContainer: {
        marginTop: 50,
        marginBottom: 20,

        justifyContent: "center",
        flexDirection: "row",
        flexWrap: "wrap"
    },
    termsText: {
        textAlign: "center",

        color: Color.grayDark,

        fontFamily: Font.type.regular,
        fontSize: Font.size.s
    },
    termsLink: {
        textDecorationLine: "underline",
        textDecorationColor: Color.gray
    }
});
