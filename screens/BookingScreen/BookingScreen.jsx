/* eslint-disable max-lines */
import * as React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Toaster } from "react-native-toastboard";
import Touchable from "react-native-platform-touchable";
import { SafeAreaView, ScrollView, View, Text, Image, TouchableOpacity, Linking } from "react-native";

import {
    CancelBookingModal,
    InviteAttendeeModal,
    CancelBookingModalOpenButton,
    InviteAttendeeModalOpenButton,
    withBookingViewControlsProvider
} from "@components/BookingComponents";

import { WorkspaceTypesLabels } from "@constants/API";
import { Images } from "@assets/Images";

import { ViewingInfo } from "./partials/ViewingInfo";
import { PaymentInfo } from "./partials/PaymentInfo";
import { BookingInfo } from "./partials/BookingInfo";
import { TermsInfo } from "./partials/TermsInfo";

import { NavigationPropTypes } from "../../navigation/NavigationPropTypes";
import { BookingScreenManager } from "./helpers/BookingScreenManager";
import { styles } from "./styles";

const mapStateToProps = (state) => ({
    list: state.bookingListReducer.list
});

@connect(mapStateToProps)
@withBookingViewControlsProvider
export class BookingScreen extends React.Component {
    static propTypes = {
        ...NavigationPropTypes,
        list: PropTypes.array.isRequired
    };
    static navigationOptions = ({ navigation: { state: { params } } }) => ({
        headerRight: (
            <CancelBookingModalOpenButton canCancel={!!params.canCancel} style={styles.cancelButton}>
                <Text style={styles.cancelButtonText}>cancel</Text>
            </CancelBookingModalOpenButton>
        )
    });

    constructor(props) {
        super(props);

        props.navigation.setParams({
            canCancel: BookingScreenManager.canCancel(BookingScreenManager.getItemData(
                props.navigation.state.params.bookingItem,
                props.list
            ))
        });
    }

    shouldComponentUpdate() {
        return false;
    }

    componentWillUnmount() {
        if (this.props.navigation.state.params) {
            delete this.props.navigation.state.params.canCancel;
        }
    }

    render() {
        const { bookingItem } = this.props.navigation.state.params;

        const itemData = BookingScreenManager.getItemData(bookingItem, this.props.list);

        if (!itemData) {
            return null;
        }

        const { workspace, location, viewing, booking } = itemData;

        return (
            <SafeAreaView style={styles.root}>
                <ScrollView contentContainerStyle={styles.scroll}>
                    <View style={styles.shadowContainer}>
                        <View style={styles.shadow}>
                            <View style={styles.paddingContainer}>
                                <TouchableOpacity onPress={this.handleImagePress(workspace.id)}>
                                    <Image
                                        resizeMode="cover"
                                        style={styles.image}
                                        source={{ uri: workspace.coverImageUrl }}
                                    />
                                </TouchableOpacity>
                                <Text style={styles.typeText}>{WorkspaceTypesLabels[workspace.type]}</Text>
                                <Text style={styles.locationNameText}>{location.name}</Text>
                                <Text style={styles.locationAddressText}>{location.address}</Text>
                            </View>
                            {bookingItem.itemType === "viewing" || BookingScreenManager.isRequestToBook(workspace)
                                ? <View style={styles.horizontalSeparator} />
                                : (
                                    <PaymentInfo
                                        type={workspace.type}
                                        price={workspace.price}
                                        deskType={workspace.deskType}
                                    />
                                )
                            }
                            <View style={styles.paddingContainer}>
                                {bookingItem.itemType === "viewing"
                                    ? <ViewingInfo startTime={viewing.startTime} />
                                    : (
                                        <BookingInfo
                                            startTime={booking.startTime}
                                            endTime={booking.endTime}
                                            name={booking.name}
                                            workspace={workspace}
                                        />
                                    )
                                }
                            </View>
                        </View>
                    </View>
                    <View style={styles.shadowContainer}>
                        <View style={styles.shadow}>
                            <Touchable
                                onPress={this.handleAddressPress(location)}
                                style={[styles.paddingContainer, styles.row]}
                            >
                                <React.Fragment>
                                    <Image style={styles.providerIcon} source={Images.addressIcon} />
                                    <View>
                                        <Text numberOfLines={1} style={styles.providerText}>{location.name}</Text>
                                        <Text numberOfLines={1} style={styles.providerText}>{location.address}</Text>
                                        <Text numberOfLines={1} style={styles.providerText}>
                                            {location.town}&nbsp;{location.postcode}
                                        </Text>
                                    </View>
                                </React.Fragment>
                            </Touchable>
                            <View style={styles.horizontalSeparator} />
                            <Touchable
                                onPress={this.handlePhonePress(location.phone)}
                                style={[styles.paddingContainer, styles.row]}
                            >
                                <React.Fragment>
                                    <Image style={styles.providerIcon} source={Images.phoneIcon} />
                                    <Text numberOfLines={1} style={styles.providerText}>{location.phone}</Text>
                                </React.Fragment>
                            </Touchable>
                            <View style={styles.horizontalSeparator} />
                            <Touchable
                                onPress={this.handleEmailPress(location.email)}
                                style={[styles.paddingContainer, styles.row]}
                            >
                                <React.Fragment>
                                    <Image style={styles.providerIcon} source={Images.emailIcon} />
                                    <Text numberOfLines={1} style={styles.providerText}>{location.email}</Text>
                                </React.Fragment>
                            </Touchable>
                        </View>
                    </View>
                    {/* <View style={styles.paddingContainer}>
                        <View style={styles.row}>
                            <View style={styles.circle}>
                                <Text style={styles.circleText}>1</Text>
                            </View>
                            <Text style={styles.emailItemText}>guest@email.com</Text>
                        </View>
                    </View> */}
                    {BookingScreenManager.canInviteAttendee(bookingItem) && (
                        <InviteAttendeeModalOpenButton>
                            <Text style={styles.buttonText}>+ invite attendee</Text>
                        </InviteAttendeeModalOpenButton>
                    )}
                    <TermsInfo
                        onPrivacyPress={this.handlePrivacyPress}
                        onTermsPress={this.handleTermsPress}
                        type={bookingItem.itemType}
                        workspace={workspace}
                    />
                </ScrollView>
                {BookingScreenManager.canInviteAttendee(bookingItem) && <InviteAttendeeModal />}
                <CancelBookingModal id={bookingItem.itemId} type={bookingItem.itemType} />
            </SafeAreaView>
        );
    }

    handleEmailPress = (addres) => async () => {
        try {
            await Linking.openURL(`mailto: ${addres}`);
        } catch (error) {
            return Toaster.error(`Couldn't open link '${addres}'`);
        }
    }

    handlePhonePress = (phone) => async () => {
        try {
            await Linking.openURL(`tel: ${phone}`);
        } catch (error) {
            return Toaster.error(`Couldn't open link '${phone}'`);
        }
    }

    handleAddressPress = (location) => () => {
        this.props.navigation.navigate("Map", {
            regions: [{ latitude: location.latitude, longitude: location.longitude }],
            renderWith: "markers"
        });
    }

    handleTermsPress = () => {
        this.props.navigation.navigate("TermsAndConditions", { textType: "terms" });
    }

    handlePrivacyPress = () => {
        this.props.navigation.navigate("TermsAndConditions", { textType: "privacy" });
    }

    handleImagePress = (workspaceId) => () => {
        this.props.navigation.navigate("Workspace", { workspaceId });
    }
}
/* eslint-enable max-lines */
