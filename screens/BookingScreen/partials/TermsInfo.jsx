import * as React from "react";
import PropTypes from "prop-types";
import { View, Text } from "react-native";

import { styles } from "../styles";
import { BookingScreenManager } from "../helpers/BookingScreenManager";

const getText = (type, workspace) => {
	if (type === "viewing") {
		return "By viewing this workspace";
	}

	if (BookingScreenManager.isRequestToBook(workspace)) {
		return "By requesting a booking";
	}

	return "By confirming your reservation";
};

export const TermsInfo = (props) => (
	<View style={styles.termsContainer}>
		<Text style={styles.termsText}>
			{getText(props.type, props.workspace) + ", you are agreeing to our"}
			&nbsp;<Text onPress={props.onPrivacyPress} style={styles.termsLink}>privacy policy</Text>&nbsp;
			and
			&nbsp;<Text onPress={props.onTermsPress} style={styles.termsLink}>terms & conditions</Text>
		</Text>
	</View>
);

TermsInfo.propTypes = {
	type: PropTypes.string.isRequired,
	workspace: PropTypes.object.isRequired,

	onPrivacyPress: PropTypes.func,
	onTermsPress: PropTypes.func
};
