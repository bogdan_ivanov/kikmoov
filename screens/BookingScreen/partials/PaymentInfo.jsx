import * as React from "react";
import PropTypes from "prop-types";
import { View, Text } from "react-native";

import { DeskTypesDurationLabels, WorkspaceTypesDurationLabels, WorkspaceTypesLabels } from "@constants/API";
import { NumberManager } from "@utils/NumberManager";

import { styles } from "../styles";

export const PaymentInfo = (props) => (
	<View style={[styles.paddingContainer, styles.filledContainer]}>
		<Text style={styles.infoFooterTextRight}>
			{WorkspaceTypesLabels[props.type] || props.type}
			- £{NumberManager.abbreviate(props.price)}&nbsp;
			{DeskTypesDurationLabels[props.deskType] || WorkspaceTypesDurationLabels[props.type] || "/m"}
		</Text>
	</View>
);

PaymentInfo.propTypes = {
	price: PropTypes.string.isRequired,
	type: PropTypes.string.isRequired,
	deskType: PropTypes.string
};
