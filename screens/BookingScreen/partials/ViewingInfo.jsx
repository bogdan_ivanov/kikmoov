import * as React from "react";
import PropTypes from "prop-types";
import { Text, View } from "react-native";

import { BookingScreenManager } from "../helpers/BookingScreenManager";

import { styles } from "../styles";

export const ViewingInfo = (props) => (
	<View style={[styles.row, styles.spaceBetween]}>
		<Text style={styles.infoFooterTextLeft}>Viewing at</Text>
		<Text style={styles.infoFooterTextRight}>{BookingScreenManager.getDates(props.startTime)}</Text>
	</View>
);

ViewingInfo.propTypes = {
	startTime: PropTypes.number.isRequired
};
