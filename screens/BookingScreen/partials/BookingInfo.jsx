import * as React from "react";
import PropTypes from "prop-types";
import { Text, View } from "react-native";

import { DateManager } from "@utils/DateManager";
import { WorkspaceTypes } from "@constants/API";

import { BookingScreenManager } from "../helpers/BookingScreenManager";
import { styles } from "../styles";

export const BookingInfo = (props) => {
	if (BookingScreenManager.isRequestToBook(props.workspace)) {
		return (
			<View style={[styles.row, styles.spaceBetween]}>
				<Text style={styles.infoFooterTextLeft}>Start date</Text>
				<Text style={styles.infoFooterTextRight}>
					{DateManager.parsedDateFromTimestamp(
						DateManager.fromUNIX(props.startTime),
						{ isFull: true }
					)}
				</Text>
			</View>
		);
	}

	if (props.workspace.type === WorkspaceTypes.desk) {
		return (
			<View style={styles.row}>
				<Text style={styles.infoFooterTextRight}>
					{BookingScreenManager.getDates(props.startTime, props.endTime)}
				</Text>
			</View>
		);
	}

	return (
		<React.Fragment>
			<Text style={[styles.infoFooterTextRight, { marginBottom: 7 }]}>{props.name}</Text>
			<Text style={styles.infoFooterTextRight}>
				{BookingScreenManager.getDates(props.startTime, props.endTime)}
			</Text>
		</React.Fragment>
	);
};

BookingInfo.propTypes = {
	startTime: PropTypes.number.isRequired,
	workspace: PropTypes.shape({
		type: PropTypes.string.isRequired,
	}).isRequired,

	endTime: PropTypes.number,
	name: PropTypes.string
};
