import { StyleSheet, Dimensions } from "react-native";

import { Font, containerFlex, filledButton, Color, buttonText } from "@constants/UI";

const { width, height } = Dimensions.get("window");

export const styles = StyleSheet.create({
    button: {
      ...filledButton,

      paddingLeft: 50,
      paddingRight: 50,

      backgroundColor: Color.red
    },
    buttonText: {
        ...buttonText,

        color: "#fff"
    },
    scrollContainer: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        height,
    },
    logo: {
        position: "absolute",
        width: 111,
        height: 122,
        top: 40
    },
    bottomContainer: {
        flex: 1,
        position: "absolute",
        bottom: 30,
        alignItems: "center"
    },
    dotsContainer: {
        flexDirection: "row",

        marginBottom: 30,
        marginTop: 47
    },
    imageTextContainer: {
        ...containerFlex,

        top: "60%",
        width: "100%"
    },
    imageTextTitle: {
        color: "#fff",

        fontSize: Font.size.xl,
        fontFamily: Font.type.medium,

        textAlign: "center",

        textShadowColor: "rgba(0, 0, 0, 0.5)",
		textShadowOffset: {
			width: 0,
			height: 2,
		},
		textShadowRadius: 3
    },
    imageTextSubTitle: {
        color: "#fff",

        fontSize: Font.size.l,
        fontFamily: Font.type.medium,

        marginTop:  Font.size.l,

        textAlign: "center",

        textShadowColor: "rgba(0, 0, 0, 0.5)",
		textShadowOffset: {
			width: 0,
			height: 2,
		},
		textShadowRadius: 3
    },
    image: {
        width,
        height,

        alignItems: "center",
    }
});
