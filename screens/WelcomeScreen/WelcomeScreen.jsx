import * as React from "react";
import PropTypes from "prop-types";
import {
    View,
    Text,
    Image,
    Platform,
    AsyncStorage,
    ImageBackground,
    TouchableOpacity
} from "react-native";
import { APP_DATA_STORAGE_KEY } from "react-native-dotenv";

import { Images } from "@assets/Images";
import { Carousel } from "@components/partials/Carousel";

import { StatusBarManager } from "@utils/StatusBarManager";

import { styles } from "./styles";

export class WelcomeScreen extends React.Component {
    static propTypes = {
        onPress: PropTypes.func.isRequired
    };
    static appDataStoragePrefix = ":welcomeScreenShown";

    static imagesText = [
        {
            title: "The most diverse real-time network of workspaces",
            subTitle: "Find your perfect workspace. Innovate. Create."
        },
        {
            title: "Hourly | daily | monthly | yerly",
            subTitle: "Find. Book. Get to work."
        },
        {
            title: "Real-time availability",
            subTitle: "Find, instantly book and pay for your workspace on the go."
        }
    ];

    componentDidMount() {
        StatusBarManager.setState("hidden");
        AsyncStorage.setItem(APP_DATA_STORAGE_KEY + WelcomeScreen.appDataStoragePrefix, "true");
    }

    render() {
        return (
            <View style={styles.scrollContainer}>
                <Carousel
                    itemsList={this.images}
                    renderItem={this.renderImage}
                    style={Platform.select({ android: { height: "100%", width: "100%" } })}
                >
                    {this.renderChildren}
                </Carousel>
            </View>
        );
    }

    get images() {
        return [Images.welcomeScreen_1, Images.welcomeScreen_2, Images.welcomeScreen_3];
    }

    renderChildren = (Dots) => {
        return (
            <React.Fragment>
                <Image source={Images.logoLight} style={styles.logo} />
                <View style={styles.bottomContainer}>
                    <View style={styles.dotsContainer}>
                        <Dots />
                    </View>
                    <TouchableOpacity style={styles.button} onPress={this.props.onPress}>
                        <Text style={styles.buttonText}>
                            Find a Workspace
                        </Text>
                    </TouchableOpacity>
                </View>
            </React.Fragment>
        );
    }

    renderImage = (source, i) => {
        return (
            <ImageBackground key={source} style={styles.image} source={source}>
                <View style={styles.imageTextContainer}>
                    <Text numberOfLines={2} style={styles.imageTextTitle}>
                        {WelcomeScreen.imagesText[i].title}
                    </Text>
                    <Text style={styles.imageTextSubTitle}>
                        {WelcomeScreen.imagesText[i].subTitle}
                    </Text>
                </View>
            </ImageBackground>
        );
    }
}
