import { StyleSheet, Dimensions } from "react-native";

import { Color, Font, containerFlex } from "@constants/UI";

export const styles = StyleSheet.create({
    root: {
        flex: 1,

        position: "relative",
    },
    trackActive: {
        width: 1.5,
        height: "150%",

        backgroundColor: Color.blue,

        position: "absolute",
        bottom: 0,
        left: 17
    },
    trackDisabled: {
        width: 1.5,
        height: "100%",

        borderWidth: 1.5,
        borderColor: Color.gray,
        borderStyle: "dashed",

        borderRadius: 1,

        position: "absolute",
        top: 0,
        left: 17
    },
    trackMask: {
        width: 2,
        height: "100%",

        position: "absolute",

        backgroundColor: "#fff",
        top: 0,
        left: 18.5
    },
    futureTitle: {
        fontSize: Font.size.l,
        fontFamily: Font.type.medium,

        color: Color.blue,

        marginLeft: 40,
        marginBottom: 5
    },
    pastTitle: {
        fontSize: Font.size.l,
        fontFamily: Font.type.medium,

        color: Color.gray,

        marginLeft: 40,
        marginBottom: 5
    },
    imageNoResults: {
        width: Dimensions.get("window").width / 2,
        maxHeight: Dimensions.get("window").width / 2,

        marginBottom: 15
    },
    noResultsContainer: {
        ...containerFlex,

        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    textNoResults: {
        fontFamily: Font.type.regular,
        fontSize: Font.size.m,

        color: Color.grayDark,

        padding: 15,
        textAlign: "center"
    }
});
