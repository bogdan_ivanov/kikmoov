import * as React from "react";
import shortId from "shortid";
import { SafeAreaView, View, ScrollView, Text, Image } from "react-native";

import { BookingItem } from "@components/BookingComponents";
import { Images } from "@assets/Images";

import { BottomTabs } from "@containers/BottomTabs";
import { BookingListContainer } from "@containers/BookingListContainer";

import { styles } from "./styles";
import { NavigationPropTypes } from "../../navigation/NavigationPropTypes";

export class BookingsScreen extends React.Component {
    static propTypes = NavigationPropTypes;

    componentWillUnmount() {
        if (this.props.navigation.state.params) {
            delete this.props.navigation.state.params.bookingItem;
        }
    }

    render() {
        return (
            <BottomTabs>
                <BookingListContainer>
                    {this.renderBookingList}
                </BookingListContainer>
            </BottomTabs>
        );
    }

    renderBookingList = (list) => {
        if (!list.length) {
            return (
                <SafeAreaView style={styles.root}>
                    <View style={styles.noResultsContainer}>
                        <Image
                            resizeMode="contain"
                            style={styles.imageNoResults}
                            source={Images.emptyBookingIcon}
                        />
                        <Text style={styles.textNoResults}>
                            No bookings yet, your past and
                            previous workspace bookings will
                            be stored here
                        </Text>
                    </View>
                </SafeAreaView>
            );
        }

        const pastBookings = [];
        const futureBookings = [];

        list.forEach((item) => {
            const itemType = item.viewing ? "viewing" : "booking";

            if (item[itemType].future
                && !BookingListContainer.asPastStatuses.includes(item[itemType].status)
            ) {
                futureBookings.push({
                    ...item,
                    [itemType]: {
                        ...item[itemType],
                        future: true
                    }
                });
            } else {
                pastBookings.push({
                    ...item,
                    [itemType]: {
                        ...item[itemType],
                        future: false
                    }
                });
            }
        });

        return (
            <SafeAreaView style={styles.root}>
                <ScrollView removeClippedSubviews bounces={false} contentContainerStyle={{ minHeight: "100%" }}>
                    {!!futureBookings.length && (
                        <View style={{ flex: 1 }}>
                            <Text style={styles.futureTitle}>Bookings</Text>
                            <View style={styles.trackActive} />
                            {this.renderBookings(futureBookings)}
                        </View>
                    )}
                    {!!pastBookings.length && (
                        <View style={{ flex: 1 }}>
                            <Text style={styles.pastTitle}>Past bookings</Text>
                            <View style={styles.trackDisabled} />
                            <View style={styles.trackMask} />
                            {this.renderBookings(pastBookings)}
                        </View>
                    )}
                </ScrollView>
            </SafeAreaView>
        );
    }

    renderBookings = (items) => {
        return items.map((item) => {
            const itemType = item.viewing ? "viewing" : "booking";

            return (
                <BookingItem
                    key={shortId.generate()}

                    itemType={itemType}
                    itemId={item[itemType].id}
                    isPast={!item[itemType].future}

                    startTime={item[itemType].startTime}
                    endTime={item[itemType].endTime}
                    status={item[itemType].status}

                    coverImageUrl={item.workspace.coverImageUrl
                        || item.workspace.images[0] ? item.workspace.images[0].url : undefined}
                    capacity={item.workspace.capacity}
                    quantity={item.workspace.quantity}
                    type={item.workspace.type}
                    size={item.workspace.size}

                    locationAddress={item.location.address}
                    locationName={item.location.name}

                    workspaceId={item.workspace.id}
                />
            );
        });
    }
}
