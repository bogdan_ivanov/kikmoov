import { StyleSheet } from "react-native";

import { containerFlex } from "@constants/UI";

export const styles = StyleSheet.create({
	root: {
		...containerFlex,

		paddingTop: 20,
		paddingBottom: 20,
	}
});
