import * as React from "react";

import { ContactUs } from "@components/ProfileComponents";
import { AvoidingScrollView } from "@components/partials/AvoidingScrollView";

import { styles } from "./styles";
import { NavigationPropTypes } from "../../navigation/NavigationPropTypes";

export const ContactUsScreen = () => (
	<AvoidingScrollView contentContainerStyle={styles.root}>
		<ContactUs />
	</AvoidingScrollView>
);

ContactUsScreen.propTypes = NavigationPropTypes;
