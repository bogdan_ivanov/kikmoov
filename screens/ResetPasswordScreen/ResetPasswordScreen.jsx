import * as React from "react";

import { ResetPassword } from "@components/ProfileComponents";
import { AvoidingScrollView } from "@components/partials/AvoidingScrollView";

import { styles } from "./styles";
import { NavigationPropTypes } from "../../navigation/NavigationPropTypes";

export const ResetPasswordScreen = () => (
	<AvoidingScrollView contentContainerStyle={styles.root}>
		<ResetPassword />
	</AvoidingScrollView>
);

ResetPasswordScreen.propTypes = NavigationPropTypes;
