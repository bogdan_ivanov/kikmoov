import * as React from "react";

import { withAnimatedScroll } from "@utils/withAnimatedScroll";

import { LoginForm } from "@components/AuthorizeComponents";
import { AvoidingScrollView } from "@components/partials/AvoidingScrollView";

import { SecurityContainer } from "@containers/SecurityContainer";

import { AnimatedLogo } from "./AnimatedLogo";

@withAnimatedScroll()
export class SignInScreen extends React.Component {
    render() {
        return (
            <AvoidingScrollView onScroll={this.handleScroll}>
                <AnimatedLogo animationValue={this.value} />
                <SecurityContainer>
                    {({ signIn }) => <LoginForm signIn={signIn} />}
                </SecurityContainer>
            </AvoidingScrollView>
        );
    }
}
