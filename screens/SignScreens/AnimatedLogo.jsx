import * as React from "react";
import PropTypes from "prop-types";
import { Animated, StyleSheet } from "react-native";

import { Images } from "@assets/Images";

const imageHeight = 127;
export class AnimatedLogo extends React.Component {
    static propTypes = {
        animationValue: PropTypes.object.isRequired
    };

    render() {
        return (
            <Animated.View style={this.ViewStyle}>
                <Animated.Image
                    style={this.ImageStyle}
                    source={Images.logoDark}
                    resizeMode="contain"
                />
            </Animated.View>
        );
    }

    get ImageStyle() {
        return { opacity: this.opacity, transform: [{ scale: this.scale }, { translateY: this.translateY }] };
    }

    get ViewStyle() {
        return [
            styles.view,
            { transform: [{ translateY: this.props.animationValue }] }
        ];
    }

    get translateY() {
        return Animated.multiply(Animated.add(-1, this.scale), this.props.animationValue)
            .interpolate({
                inputRange: [
                    0,
                    1
                ],
                /* eslint-disable-next-line no-magic-numbers */
                outputRange: [0, 1.8]
            });
    }

    get opacity() {
        return this.props.animationValue.interpolate({
            inputRange: [
                0,
                imageHeight / 2,
                /* eslint-disable-next-line no-magic-numbers */
                imageHeight / 1.2,
            ],
            outputRange: [1, 1, 0]
        });
    }

    get scale() {
        return this.props.animationValue.interpolate({
            inputRange: [
                0,
                imageHeight,
            ],
            outputRange: [
                1,
                0
            ],
            extrapolateLeft: "clamp"
        });
    }
}

const styles = StyleSheet.create({
    view: {
        flexDirection: "row",
        justifyContent: "center",
    }
});
