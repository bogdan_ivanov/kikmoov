// @flow
import { FilterTypes, SortTypes } from "../constants/API";

export interface SignUpRequestPayloadData {
    email: string;
    password: string;
    lastname: string;
    password: string;
    firstname: string;
}

export interface ContactUsRequestPayloadData {
    message: string;
}

export interface FacilityItem {
    appIcon: string;
    webIcon: string;
    name: string;
    slug: string;
}

export type FacilitiesListResponsePayloadData = Array<FacilityItem>;

export interface PriceRangeResponsePayloadData {
    min: number;
    max: number;
}

export interface SearchRequestPayloadData {
    area?: ?string;
    address?: ?string;
    station?: ?string;
    borough?: ?string;

    minPrice?: ?number;
    maxPrice?: ?number;
    quantity?: ?number;
    size?: ?string | ?number;
    facilities: Array<string>;
    date: $Keys<typeof SortTypes.DATE>;
    price: $Keys<typeof SortTypes.PRICE>;
    type: $Keys<typeof FilterTypes.AREA>;
    duration: $Keys<typeof FilterTypes.DURATION>;
}

export interface WorkspaceInterface {
    id: number;
    type: string;
    price: string;
    status: string;
    size?: ?number;
    deskType: string;
    quantity: number;
    isLiked: boolean;
    visited: boolean;
    createdAt: number;
    capacity?: ?number;
    description: string;
    coverImageUrl?: ?string;
    minContractLength?: ?string;
    maxContractLength?: ?string;
    images: Array<{
        id: number;
        url: string;
    }>;
    facilities: Array<FacilityItem>;
}

export interface ShortLocationInterface {
    id: number;
    name: string;
    address: string;
    lattitude: string;
    longitude: string;
}

export interface FullLocationInterface extends ShortLocationInterface {
    town: string;
    status: string;
    postcode: string;
    description: string;
    addressOptional: string;
    nearby: Array<{
        type: string;
        name: string;
        distance: string;
        duration: string;
    }>;
}

export interface NotificationItemInterface {
    message: string;
    createdAt: number;
    action: {
        actionType: string;
        id: number;
    };
}

export interface SearchResponsePayloadData {
    results: Array<{
        related: Array<WorkspaceInterface>;
        location: ShortLocationInterface;
        workspace: WorkspaceInterface;
    }>;
}

export interface WorkspaceInfoResponsePaylodData {
    workspace: WorkspaceInterface;
    location: FullLocationInterface;
    related: Array<WorkspaceInterface>;
    user: {
        id: number;
        email: string;
    };
    bookedDates: Array<{
        startTime: number;
        endTime: number;
    }>;
}

export interface AddViewingToScheduleRequestPaylodData {
    phone: string;
    startTime: number;
    endTime?: ?number;
}

export interface RequestToBookRequestPayloadData {
    information: string;
    tenantType: string;
    startTime: number;
    duration: string;
    phone: string;
    email: string;
    name: string;
}

export interface BookRequestPayloadData {
    paymentToken: string;
    startTime: number;
    endTime: number;
    phone: number;
    email: string;
    name: string;
}

export interface TopWorkspacesResponsePayloadData {
    results: Array<{
        related: Array<WorkspaceInterface>;
        location: ShortLocationInterface;
        workspace: WorkspaceInterface;
    }>;
}
