// @flow

export interface ActionInteface {
    payload: {
        config: {
            data: any;
        };
        data: any;
    };
    params: any;
    type: string;
    error: ?any;
    outerPayload: any;
}
