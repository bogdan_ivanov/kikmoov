// @flow

export interface SignInRequestPayloadData {
    email: string;
    password: string;
}

export interface GetTokenRequestPayloadData {
    client_secret: string;
    client_id: string;

    username: string;
    password: string;
}

export interface RefreshTokenRequestPayloadData {
    client_secret: string;
    client_id: string;

    refresh_token: string;
}

export interface FacebookSignInRequestPayloadData {
    firstName: string;
    lastName: ?string;
    email: string;

    authToken: string;
    id: string;
}

export interface GoogleInRequestPayloadData {
    id: string;
    idToken: string;

    email: string;
    fistName: string;
    lastName: ?string;
}

export interface LinkedinSignInRequestPayloadData {
    code: string;
}
