// @flow
export interface UpdateDetailsRequestPayloadData {
	companyName?: ?string;
	firstname?: ?string;
	lastname?: ?string;
	phone?: ?string;
	email?: ?string;
}

export interface UpdatePasswordRequestPayloadData {
	oldPassword: string;
	newPassword: string;
}
