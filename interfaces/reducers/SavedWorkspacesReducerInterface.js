// @flow
import { ShortLocationInterface, WorkspaceInterface } from "../StatelessActions";

export interface SavedWorkspacesReducerInterface {
	workspaces?: ?Array<{
		location: ShortLocationInterface;
		workspace: WorkspaceInterface;
	}>;
	ids: Set<number>;
}
