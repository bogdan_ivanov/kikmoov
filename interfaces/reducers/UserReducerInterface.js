// @flow

export interface UserReducerInterface {
	companyName?: ?string;
	pushStatus: boolean;
	firstname: string;
	lastname: string;
	phone?: ?string;
	email: string;
}
