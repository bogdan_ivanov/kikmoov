// @flow
import { FullLocationInterface, WorkspaceInterface } from "../StatelessActions";

interface ItemInteface {
	id: number;
	name?: ?string;
	amount: number;
	status: string;
	future: boolean;
	endTime?: ?number;
	startTime: number;
	createdAt: number;
}

export interface BookingListReducerInterface {
	list: Array<{
		location: FullLocationInterface;
		workspace: WorkspaceInterface;
		booking: ?ItemInteface;
		viewing: ?ItemInteface;
	}>;
}
