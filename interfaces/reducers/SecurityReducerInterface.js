// @flow

export interface SecurityReducerInterface {
    authorized: boolean;
}
