import PropTypes from "prop-types";

export const NavigationPropTypes = {
    navigation: PropTypes.shape({
        navigate: PropTypes.func.isRequired,
        goBack: PropTypes.func,
        setState: PropTypes.func
    }).isRequired,
    dispatch: PropTypes.func,
    toggleDrawer: PropTypes.func,
    state: PropTypes.shape({
        routes: PropTypes.array.isRequired
    })
};
