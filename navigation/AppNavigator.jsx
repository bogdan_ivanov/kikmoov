import * as React from "react";
import { Dimensions } from "react-native";
import { createStackNavigator, createDrawerNavigator } from "react-navigation";

import { BackButton } from "@components/NavigationComponents";
import { SideMenu } from "@containers/SideMenu";

import { MapScreen } from "../screens/MapScreen";
import { HomeScreen } from "../screens/HomeScreen";
import { SearchScreen } from "../screens/SearchScreen";
import { BookingScreen } from "../screens/BookingScreen";
import { BookingsScreen } from "../screens/BookingsScreen";
import { WorkspaceScreen } from "../screens/WorkspaceScreen";
import { ContactUsScreen } from "../screens/ContactUsScreen";
import { BookCreationScreen } from "../screens/BookCreationScreen";
import { SignInScreen, SignUpScreen } from "../screens/SignScreens";
import { ResetPasswordScreen } from "../screens/ResetPasswordScreen";
import { RequestToBookScreen } from "../screens/RequestToBookScreen";
import { NotificationsScreen } from "../screens/NotificationsScreen";
import { ProfileDetailsScreen } from "../screens/ProfileDetailsScreen";
import { SavedWorkspacesScreen } from "../screens/SavedWorkspacesScreen";
import { ScheduleViewingScreen } from "../screens/ScheduleViewingScreen";
import { TermsAndConditionsScreen } from "../screens/TermsAndConditionsScreen";

const commonNavigationOptions = ({ navigation }) => ({
    headerLeft: <BackButton onPress={() => navigation.pop()} />
});

const MainStack = createStackNavigator(
    {
        Main: {
            screen: HomeScreen
        },
        SignIn: {
            screen: SignInScreen,
            navigationOptions: (options) => ({
                ...commonNavigationOptions(options)
            })
        },
        SignUp: {
            screen: SignUpScreen,
            navigationOptions: (options) => ({
                ...commonNavigationOptions(options)
            })
        },
        Search: {
            screen: SearchScreen
        },
        ProfileDetails: {
            screen: ProfileDetailsScreen,
            navigationOptions: (options) => ({
                ...commonNavigationOptions(options)
            })
        },
        SavedWorkspaces: {
            screen: SavedWorkspacesScreen,
            navigationOptions: (options) => ({
                ...commonNavigationOptions(options)
            })
        },
        Bookings: {
            screen: BookingsScreen,
            navigationOptions: (options) => ({
                ...commonNavigationOptions(options)
            })
        },
        Booking: {
            screen: BookingScreen,
            navigationOptions: (options) => ({
                ...commonNavigationOptions(options)
            })
        },
        Workspace: {
            screen: WorkspaceScreen,
            navigationOptions: (options) => ({
                ...commonNavigationOptions(options)
            })
        },
        ScheduleViewing: {
            screen: ScheduleViewingScreen,
            navigationOptions: (options) => ({
                ...commonNavigationOptions(options)
            })
        },
        RequestToBook: {
            screen: RequestToBookScreen,
            navigationOptions: (options) => ({
                ...commonNavigationOptions(options)
            })
        },
        ContactUs: {
            screen: ContactUsScreen,
            navigationOptions: (options) => ({
                ...commonNavigationOptions(options)
            })
        },
        ResetPassword: {
            screen: ResetPasswordScreen,
            navigationOptions: (options) => ({
                ...commonNavigationOptions(options)
            })
        },
        BookCreation: {
            screen: BookCreationScreen,
            navigationOptions: (options) => ({
                ...commonNavigationOptions(options)
            })
        },
        TermsAndConditions: {
            screen: TermsAndConditionsScreen,
            navigationOptions: (options) => ({
                ...commonNavigationOptions(options)
            })
        },
        Map: {
            screen: MapScreen
        },
        Notifications: {
            screen: NotificationsScreen,
            navigationOptions: (options) => ({
                ...commonNavigationOptions(options)
            })
        }
    },
    {
        navigationOptions: {
            headerStyle: {
                backgroundColor: "#fff",
                borderBottomColor: "#fff",
                elevation: 0,
                height: 44
            },
            headerTintColor: "#fff",
            headerTitleStyle: {
                fontWeight: "bold",
            },
            gesturesEnabled: false
        },
        headerMode: "screen",
        cardStyle: { backgroundColor: "#fff" }
    }
);

const drawerWidthPercentage = .57;

export const AppNavigator = createDrawerNavigator(
    { MainStack },
    {
        contentComponent: SideMenu,
        drawerWidth: Dimensions.get("window").width * drawerWidthPercentage
    }
);
