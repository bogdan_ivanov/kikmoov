import * as React from "react";
import { connect } from "react-redux";
import { ActivityIndicator, View, InteractionManager } from "react-native";

import { fetchBookingList, resetBookingListStore } from "@store/actions";
import { bindCancelToken } from "@store/utils/bindCancelToken";
import { Color } from "@constants/UI";

import { BookingListContainerPropTypes } from "./BookingListContainerPropTypes";

const mapDispatchToProps = bindCancelToken({
    fetchBookingList,
    resetBookingListStore
});

const mapStateToProps = (state) => ({
    list: state.bookingListReducer.list
});

@connect(mapStateToProps, mapDispatchToProps)
export class BookingListContainer extends React.Component {
    static propTypes = BookingListContainerPropTypes;

    static asPastStatuses = ["declined", "canceled"];

    state = {
        loading: true
    };

    async componentDidMount() {
        this.cancellableLoading = InteractionManager.runAfterInteractions(async () => {
            await this.props.fetchBookingList();

            if (this.props.fetchBookingList.canceled) {
                return;
            }

            this.setState({ loading: false });
        });
    }

    componentWillUnmount() {
        this.cancellableLoading && this.cancellableLoading.cancel();
        this.props.fetchBookingList.cancel();
        this.props.resetBookingListStore();
    }

    render() {
        if (this.state.loading) {
            return (
                <View style={{ flex: 1, justifyContent: "center" }}>
                    <ActivityIndicator size="large" color={Color.gray} />
                </View>
            );
        }

        return typeof this.props.children === "function"
            ? this.props.children(this.props.list)
            : this.props.children;
    }
}
