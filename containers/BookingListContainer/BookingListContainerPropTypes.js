import PropTypes from "prop-types";

export const BookingListContainerPropTypes = {
	fetchBookingList: PropTypes.func.isRequired,
	resetBookingListStore: PropTypes.func.isRequired,

	children: PropTypes.oneOfType([PropTypes.func, PropTypes.node]).isRequired,

	list: PropTypes.array.isRequired
};
