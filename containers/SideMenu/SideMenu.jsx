import * as React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { NavigationActions } from "react-navigation";
import { SafeAreaView, TouchableOpacity, View, Image, Platform } from "react-native";

import { SignOut } from "@components/AuthorizeComponents";

import { StatusBarManager } from "@utils/StatusBarManager";

import { Images } from "@assets/Images";
import { Color } from "@constants/UI";

import { list } from "./list";
import { styles } from "./styles";
import { MenuItem } from "./MenuItem";
import { NavigationPropTypes } from "../../navigation/NavigationPropTypes";

const mapStateToProps = (state) => ({
    authorized: state.securityReducer.authorized
});

const SideMenuPropTypes = {
    ...NavigationPropTypes,
    authorized: PropTypes.bool.isRequired
};

@connect(mapStateToProps)
export class SideMenu extends React.PureComponent {
    static propTypes = SideMenuPropTypes;

    componentDidUpdate(prevProps) {
        if (prevProps.navigation.state.isDrawerOpen === this.props.navigation.state.isDrawerOpen) {
            return;
        }

        if (this.props.navigation.state.isDrawerOpen) {
            StatusBarManager.setState("hidden");
        } else {
            StatusBarManager.restoreState();
        }
    }

    render() {
        const currentRouteName = this.currentRouteName;

        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: "#fff"}}>
                <View style={styles.sideBarContainer}>
                    <View style={{ marginBottom: 60, marginTop: Platform.select({ ios: 0, android: 25 }) }}>
                        <TouchableOpacity
                            onPress={this.handleClose}
                            hitSlop={{ left: 10, right: 10, top: 10, bottom: 10 }}
                        >
                            <Image
                                fadeDuration={0}
                                source={Images.cross}
                                style={{ width: 18, height: 18, tintColor: Color.grayDark }}
                            />
                        </TouchableOpacity>
                    </View>
                    {this.renderMappedList(currentRouteName)}
                    <View style={styles.footerContainer}>
                        {this.renderFooterButtons(currentRouteName)}
                    </View>
                </View>
            </SafeAreaView>
        );
    }

    get currentRouteName() {
        const found = this.props.navigation.state.routes.findIndex(({ routeName }) => routeName === "MainStack");

        if (!~found) {
            return "";
        }

        const { index } = this.props.navigation.state.routes[found];
        return this.props.navigation.state.routes[found].routes[index].routeName;
    }

    renderMappedList = (currentRouteName) => {
        return list
            .filter(({ authorizedOnly }) => !this.props.authorized ? !authorizedOnly : true)
            .filter(({ disabled }) => !disabled)
            .map(({ route, label }) => (
                <MenuItem key={label} isActive={currentRouteName === route} onPress={this.navigateToScreen(route)}>
                    {label}
                </MenuItem>
            ));
    }

    renderFooterButtons = (currentRouteName) => {
        return this.props.authorized
            ? (
                <React.Fragment>
                    <SignOut navigation={this.props.navigation}>
                        {(openHandler) => (
                            <MenuItem isActive={false} onPress={openHandler}>
                                Sign out
                            </MenuItem>
                        )}
                    </SignOut>
                    <MenuItem
                        isActive={currentRouteName === "TermsAndConditions"}
                        onPress={this.navigateToScreen("TermsAndConditions")}
                    >
                        Terms & conditions
                    </MenuItem>
                </React.Fragment>
            )
            : (
                <MenuItem
                    isActive={currentRouteName === "SignIn"}
                    onPress={this.navigateToScreen("SignIn")}
                >
                    Sign in
                </MenuItem>
            );
    }

    handleClose = () => {
        this.props.navigation.closeDrawer();
    }

    navigateToScreen = (activeRoute) => () => {
        const navigateAction = NavigationActions.navigate({
            routeName: activeRoute
        });

        this.props.navigation.dispatch(navigateAction);
    };
}
