import * as React from "react";
import PropTypes from "prop-types";
import { Text, TouchableOpacity } from "react-native";

import { styles } from "./styles";

export const MenuItem = (props) => (
    <TouchableOpacity onPress={props.onPress}>
        <Text style={[styles.navItem, props.isActive && styles.navItemActive]}>
            {props.children}
        </Text>
    </TouchableOpacity>
);

MenuItem.propTypes = {
    onPress: PropTypes.func.isRequired,
    isActive: PropTypes.bool.isRequired
};
