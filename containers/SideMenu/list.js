export const list = [
	{
		route: "Main",
		label: "Search & Discover"
	},
	{
		route: "SavedWorkspaces",
		label: "Saved workspaces",
		authorizedOnly: true
	},
	{
		route: "Bookings",
		label: "Bookings",
		authorizedOnly: true
	},
	{
		route: "Notifications",
		label: "Notifications",
		authorizedOnly: true
	},
	{
		route: "ProfileDetails",
		label: "Profile",
		authorizedOnly: true
	}
];
