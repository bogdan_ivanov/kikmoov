import { StyleSheet } from "react-native";

import { Font, Color } from "@constants/UI";

export const styles = StyleSheet.create({
  sideBarContainer: {
    backgroundColor: "#fff",
    paddingTop: 20,
    padding: 15,
    flex: 1,
  },
  container: {
    paddingTop: 20,
    flex: 1
  },
  navItem: {
    fontFamily: Font.type.regular,
    fontSize: Font.size.l,
    color: Color.grayDark,
    marginLeft: 10,
    marginBottom: 25
  },
  navItemActive: {
    fontFamily: Font.type.semiBold,
  },
  footerContainer: {
    width: "100%",
    position: "absolute",
    bottom: 10,
    margin: 15
  }
});
