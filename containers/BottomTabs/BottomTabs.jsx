import * as React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { withNavigation } from "react-navigation";
import { View, TouchableOpacity, Image, StyleSheet, Keyboard, Platform, Dimensions } from "react-native";

import { shadowArea, Color } from "@constants/UI";
import { Images } from "@assets/Images";

import { NavigationPropTypes } from "../../navigation/NavigationPropTypes";

const mapStateToProps = (state) => ({
	authorized: state.securityReducer.authorized
});

@withNavigation
@connect(mapStateToProps)
export class BottomTabs extends React.Component {
	static propTypes = {
		authorized: PropTypes.bool.isRequired,

		...NavigationPropTypes
	};

	constructor(props) {
		super(props);

		this.state = {
			isHidden: false
		};

		this.showListener = Keyboard.addListener(
			Platform.select({ ios: "keyboardWillShow", android: "keyboardDidShow" }),
			this.keyboardShow
		);
		this.hideListener = Keyboard.addListener(
			Platform.select({ ios: "keyboardWillHide", android: "keyboardDidHide" }),
			this.keyboardHide
		);
	}

	componentWillUnmount() {
		this.showListener.remove();
		this.hideListener.remove();
	}

	render() {
		if (this.state.isHidden) {
			return this.props.children;
		}

		return (
			<React.Fragment>
				{this.props.children}
				<View style={styles.root}>
					<TouchableOpacity
						hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
						onPress={this.handleNavigateMain}
					>
						<Image
							resizeMode="contain"

							style={[styles.icon, this.isActive(["Main", "Search"]) && styles.iconActive]}
							source={Images.searchIcon}
						/>
					</TouchableOpacity>
					<TouchableOpacity
						hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
						onPress={this.handleNavigateSaved}
					>
						<Image
							resizeMode="contain"

							style={[styles.icon, this.isActive(["SavedWorkspaces"]) && styles.iconActive]}
							source={Images.heartFilled}
						/>
					</TouchableOpacity>
					<TouchableOpacity
						hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
						onPress={this.handleNavigateBookings}
					>
						<Image
							resizeMode="contain"

							style={[styles.icon, this.isActive(["Bookings"]) && styles.iconActive]}
							source={Images.bookingsIcon}
						/>
					</TouchableOpacity>
				</View>
			</React.Fragment>
		);
	}

	isActive = (routes) => {
		return routes.includes(this.props.navigation.state.routeName);
	}

	keyboardShow = () => {
		this.setState({ isHidden: true });
	}

	keyboardHide = () => {
		this.setState({ isHidden: false });
	}

	handleNavigateMain = () => {
		if (["Main", "Search"].includes(this.props.navigation.state.routeName)) {
			return;
		}

		this.props.navigation.navigate("Main", { focus: true });
	}

	handleNavigateSaved = () => {
		if (!this.props.authorized) {
			return this.props.navigation.navigate("SignIn", {
				redirectRouteName: "SavedWorkspaces",
				action: "see Saved workspaces"
			});
		}

		this.props.navigation.navigate("SavedWorkspaces");
	}

	handleNavigateBookings = () => {
		if (!this.props.authorized) {
			return this.props.navigation.navigate("SignIn", {
				redirectRouteName: "Bookings",
				action: "see Bookings"
			});
		}

		this.props.navigation.navigate("Bookings");
	}
}

const styles = StyleSheet.create({
	root: {
		...shadowArea,
		shadowOffset: {
			width: 0,
			height: -3
		},
		shadowRadius: 1,
		shadowOpacity: .5,

		justifyContent: "space-between",
		alignItems: "stretch",
		flexDirection: "row",
		flex: 0,

		paddingTop: 12,
		paddingBottom: 12,
		paddingLeft: 55,
		paddingRight: 55
	},
	icon: {
		/* eslint-disable no-magic-numbers */
		width: Dimensions.get("window").width / 12.5,
		height: Dimensions.get("window").width / 12.5
		/* eslint-disable no-magic-numbers */,

		tintColor: Color.extraGray
	},
	iconActive: {
		tintColor: Color.red
	}
});
