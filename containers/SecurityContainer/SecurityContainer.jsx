import * as React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { withNavigation } from "react-navigation";
import { Toaster } from "react-native-toastboard";
import { View, Text, ActivityIndicator } from "react-native";

import { bindCancelToken } from "@store/utils/bindCancelToken";
import { signIn } from "@store/actions";
import { Color } from "@constants/UI";

import { SignInWithGoogle, SignInWithFacebook, SignInWithLinkedin } from "@components/AuthorizeComponents";

import { styles } from "./styles";
import { NavigationPropTypes } from "../../navigation/NavigationPropTypes";

const SecurityContainerPropTypes = {
    children: PropTypes.func.isRequired,
    authorized: PropTypes.bool.isRequired,

    ...NavigationPropTypes
};

const mapDispatchToProps = bindCancelToken({
    signIn
});

const mapStateToProps = (state) => ({
    authorized: state.securityReducer.authorized
});

@withNavigation
@connect(mapStateToProps, mapDispatchToProps)
export class SecurityContainer extends React.Component {
    static propTypes = SecurityContainerPropTypes;

    state = {
        socialsRequestLoading: false
    };

    componentDidMount() {
        if (!this.props.navigation.state.params || !this.props.navigation.state.params.redirectRouteName) {
            return;
        }

        Toaster.info(`You should be authorized to ${this.props.navigation.state.params.action}`);
    }

    componentWillUnmount() {
        if (this.props.navigation.state.params) {
            delete this.props.navigation.state.params.redirectRouteName;
        }

        this.props.signIn.cancel();
    }

    render() {
        if (this.state.socialsRequestLoading) {
            return (
                <ActivityIndicator style={{ marginTop: 50 }} color={Color.gray} size="large" />
            );
        }

        return (
            <View style={styles.innerWindow}>
                {this.props.children(this.props)}
                <View style={styles.separatorContainer}>
                    <View style={styles.line} />
                    <Text style={styles.separatorText}>or</Text>
                    <View style={styles.line} />
                </View>
                <View style={styles.socialsContainer}>
                    <SignInWithGoogle
                        onRequestStart={this.handleSocialsLoadingStart}
                        onRequestEnd={this.handleSocialsLoadingEnd}
                    />
                    <SignInWithFacebook
                        onRequestStart={this.handleSocialsLoadingStart}
                        onRequestEnd={this.handleSocialsLoadingEnd}
                    />
                    <SignInWithLinkedin
                        onRequestStart={this.handleSocialsLoadingStart}
                        onRequestEnd={this.handleSocialsLoadingEnd}
                    />
                </View>
            </View>
        );
    }

    handleSocialsLoadingStart = () => {
        this.setState({ socialsRequestLoading: true });
    }

    handleSocialsLoadingEnd = () => {
        this.setState({ socialsRequestLoading: false });

        if (!this.props.authorized) {
            return;
        }

        if (!this.props.navigation.state.params) {
            return this.props.navigation.replace("Main");
        }

        this.props.navigation.replace(
            this.props.navigation.state.params.redirectRouteName,
            this.props.navigation.state.params.actionParams
        );
    }
}
