import { StyleSheet } from "react-native";

import { shadowArea, Color, Font } from "@constants/UI";

export const styles = StyleSheet.create({
    innerWindow: {
        ...shadowArea,

        marginLeft: "11%",
        marginRight: "11%",
        marginTop: 35,
        marginBottom: 35,

        flex: 1,
        borderRadius: 10,
        padding: "6.92%",
        alignItems: "center"
    },
    separatorContainer: {
        marginTop: 22,
        flexDirection: "row",
        width: "100%"
    },
    line: {
        backgroundColor: Color.grayLight,
        height: 1,
        marginTop: 9,
        flexGrow: 1
    },
    separatorText: {
        width: 30,
        textAlign: "center",
        color: Color.grayLight,
        fontFamily: Font.type.medium
    },
    socialsContainer: {
        width: "100%",
        paddingLeft: 17,
        paddingRight: 17,
        flexDirection: "row",
        justifyContent: "space-between",
        marginTop: 25
    }
});
