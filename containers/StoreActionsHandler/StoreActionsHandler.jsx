/*
	Component for global handling some store changings.
*/
import * as React from "react";
import { connect } from "react-redux";
import { NavigationActions } from "react-navigation";
import { withLifeCycles } from "react-redux-lifecycles";

import { StoreActionsHandlerPropTypes } from "./StoreActionsHandlerPropTypes";
import { AppNavigator } from "../../navigation/AppNavigator";

@withLifeCycles(["securityReducer.authorized"])
@connect((state) => state, undefined, undefined, { withRef: true })
export class StoreActionsHandler extends React.Component {
	static propTypes = StoreActionsHandlerPropTypes;

	storeDidUpdateState(currentStoreState, prevStoreState) {
		if (this.wasSignOut(currentStoreState, prevStoreState)) {
			AppNavigator.dispatch(NavigationActions.navigate({ routeName: "Main" }));
		}
	}

	render() {
		return this.props.children;
	}

	wasSignOut = (currentStoreState, prevStoreState) => {
		return !currentStoreState.securityReducer.authorized && prevStoreState.securityReducer.authorized;
	}
}
