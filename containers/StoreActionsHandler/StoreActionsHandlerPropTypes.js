import PropTypes from "prop-types";

import { UserReducerStatePropTypes } from "@components/ProfileComponents";

export const StoreActionsHandlerPropTypes = {
	securityReducer: PropTypes.shape({
		authorized: PropTypes.bool.isRequired
	}).isRequired,
	userReducer: PropTypes.shape(UserReducerStatePropTypes).isRequired
};
