import PropTypes from "prop-types";

import { NavigationPropTypes } from "../../navigation/NavigationPropTypes";

export const SavedWorkspacesContainerPropTypes = {
	fetchSavedWorkspacesList: PropTypes.func.isRequired,
	resetSavedWorkspacesList: PropTypes.func.isRequired,

	children: PropTypes.func.isRequired,

	workspaces: PropTypes.array,
	ids: PropTypes.instanceOf(Set).isRequired,

	...NavigationPropTypes
};
