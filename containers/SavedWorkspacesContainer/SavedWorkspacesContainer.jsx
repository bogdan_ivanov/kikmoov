import * as React from "react";
import deepEqual from "deep-equal";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withNavigation } from "react-navigation";
import { View, ActivityIndicator, InteractionManager } from "react-native";

import { fetchSavedWorkspacesList, resetSavedWorkspacesList } from "@store/actions";

import { Color } from "@constants/UI";

import { SavedWorkspacesContainerPropTypes } from "./SavedWorkspacesContainerPropTypes";

const mapDispatchToProps = (dispatch) => bindActionCreators({
	fetchSavedWorkspacesList,
	resetSavedWorkspacesList
}, dispatch);

const mapStateToProps = (state) => ({
	workspaces: state.savedWorkspacesReducer.workspaces,
	ids: state.savedWorkspacesReducer.ids
});

@withNavigation
@connect(mapStateToProps, mapDispatchToProps)
export class SavedWorkspacesContainer extends React.PureComponent {
	static propTypes = SavedWorkspacesContainerPropTypes;

	constructor(props) {
		super(props);

		this.willFocusListener = props.navigation.addListener("willFocus", () => {
			this.forceUpdate();
			if (!this.shouldFetch) {
				return;
			}
			this.cancellableLoading && this.cancellableLoading.cancel();

			this.cancellableLoading = InteractionManager.runAfterInteractions(this.props.fetchSavedWorkspacesList);
		});
	}

	componentWillUnmount() {
		this.cancellableLoading && this.cancellableLoading.cancel();
		this.willFocusListener.remove();
		this.props.resetSavedWorkspacesList();
	}

	render() {
		if (this.shouldFetch) {
			return (
				<View style={{ flex: 1, justifyContent: "center" }}>
					<ActivityIndicator size="large" color={Color.gray} />
				</View>
			);
		}

		return this.props.children(this.props.workspaces);
	}

	get shouldFetch() {
		return !Array.isArray(this.props.workspaces) || !this.isSynchronized;
	}

	get isSynchronized() {
		return deepEqual(
			this.props.workspaces.map(({ workspace }) => workspace.id),
			Array.from(this.props.ids.values())
		);
	}
}
