import * as React from "react";
import { connect } from "react-redux";
import { ActivityIndicator, View, InteractionManager } from "react-native";

import { bindCancelToken } from "@store/utils/bindCancelToken";
import { getUserDetails } from "@store/actions";
import { Color } from "@constants/UI";

import { UserContainerPropTypes } from "./UserContainerPropTypes";

const mapDispatchToProps = bindCancelToken({
	getUserDetails
});

const mapStateToProps = (state) => ({
	userData: state.userReducer
});

@connect(mapStateToProps, mapDispatchToProps)
export class UserContainer extends React.Component {
	static propTypes = UserContainerPropTypes;

	state = {
		loading: true
	};

	async componentDidMount() {
		this.cancellableLoading = InteractionManager.runAfterInteractions(async () => {
			await this.props.getUserDetails();

			if (this.props.getUserDetails.canceled) {
				return;
			}

			this.props.onFetchSuccess && this.props.onFetchSuccess(this.props.userData);

			this.setState({ loading: false });
		});
	}

	componentWillUnmount() {
		this.cancellableLoading && this.cancellableLoading.cancel();
		this.props.getUserDetails.cancel();
	}

	render() {
		if (this.state.loading) {
			return (
				<View style={{ flex: 1, justifyContent: "center" }}>
					<ActivityIndicator size="large" color={Color.gray} />
				</View>
			);
		}

		return this.props.children(this.props.userData);
	}
}
