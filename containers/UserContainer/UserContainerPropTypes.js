import PropTypes from "prop-types";

export const UserContainerPropTypes = {
	children: PropTypes.func.isRequired,

	getUserDetails: PropTypes.func.isRequired,
	onFetchSuccess: PropTypes.func,

	userData: PropTypes.shape({
		firstname: PropTypes.string.isRequired,
		lastname: PropTypes.string.isRequired,
		pushStatus: PropTypes.bool.isRequired,
		email: PropTypes.string.isRequired,
		companyName: PropTypes.string,
		phone: PropTypes.string
	}).isRequired
};
