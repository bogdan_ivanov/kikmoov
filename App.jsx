import * as React from "react";
import Sentry from "sentry-expo";
import { Provider } from "react-redux";
import { AppLoading, Asset, Font, Icon } from "expo";
import { APP_DATA_STORAGE_KEY, SENTRY_DSN } from "react-native-dotenv";
import { StatusBar, StyleSheet, View, AsyncStorage } from "react-native";

import { Images } from "@assets/Images";
import { store, CredentialsManager, refreshToken } from "@store";

import { StatusBarManager } from "@utils/StatusBarManager";
import { StoreActionsHandler } from "@containers/StoreActionsHandler";

import { AppNavigator } from "./navigation/AppNavigator";

import { WelcomeScreen } from "./screens/WelcomeScreen";
import { Toaster } from "./components/Toaster";

if (process.env.NODE_ENV === "production") {
    Sentry.config(SENTRY_DSN).install();
}

/* eslint-disable-next-line import/no-default-export */
export default class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoadingComplete: false,
            welcomeScreenShown: false
        };

        StatusBarManager.setState("hidden");
    }

    render() {
        if (!this.state.isLoadingComplete) {
            return (
                <AppLoading
                    startAsync={this.loadResourcesAsync}
                    onFinish={this.handleFinishLoading}
                    onError={this.handleLoadingError}
                />
            );
        }

        return (
            <Provider store={store}>
                <StoreActionsHandler>
                    <View style={styles.container}>
                        <StatusBar translucent backgroundColor="transparent" barStyle="dark-content" />
                        {this.state.welcomeScreenShown
                            ? <AppNavigator ref={this.setContainerDispatch} />
                            : <WelcomeScreen onPress={() => this.setState({ welcomeScreenShown: true })} />
                        }
                    </View>
                    <Toaster />
                </StoreActionsHandler>
            </Provider>
        );
    }

    setContainerDispatch = (navigatorRef) => AppNavigator.dispatch = navigatorRef.dispatch;

    loadResourcesAsync = async () => {
        return Promise.all([
            Asset.loadAsync([
                require("./assets/images/robot-dev.png"),
                require("./assets/images/robot-prod.png"),
                ...Object.keys(Images).map((image) => Images[image])
            ]),
            Font.loadAsync({
                ...Icon.Ionicons.font,
                "montserrat-bold": require("./assets/fonts/Montserrat-Bold.ttf"),
                "montserrat-light": require("./assets/fonts/Montserrat-Light.ttf"),
                "montserrat-medium": require("./assets/fonts/Montserrat-Medium.ttf"),
                "montserrat-regular": require("./assets/fonts/Montserrat-Regular.ttf"),
                "montserrat-semibold": require("./assets/fonts/Montserrat-SemiBold.ttf"),
                "montserrat-extrabold": require("./assets/fonts/Montserrat-ExtraBold.ttf")
            })
        ]);
    };

    handleLoadingError = (error) => {
        /* eslint-disable-next-line no-console */
        console.warn(error);
    };

    handleFinishLoading = async () => {
        let welcomeScreenShown;
        try {
            welcomeScreenShown = await AsyncStorage.getItem(APP_DATA_STORAGE_KEY + WelcomeScreen.appDataStoragePrefix);
        } catch (error) {
            welcomeScreenShown = false;
        }

        const data = await CredentialsManager.getCredentials();

        if (!data) {
            StatusBarManager.reset();
            return this.setState({ isLoadingComplete: true, welcomeScreenShown });
        }

        const { client_secret, client_id, refresh_token } = data;

        await store.dispatch(refreshToken({ client_secret, client_id, refresh_token }));

        StatusBarManager.reset();
        this.setState({ isLoadingComplete: true, welcomeScreenShown });
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
});
