import { registerDecorator } from "react-formawesome-core/class-validator";

export function isCardExpiry(validationOptions) {
	return function (object, propertyName) {
		registerDecorator({
			name: "isCardExpiry",
			target: object.constructor,
			propertyName: propertyName,
			options: validationOptions,
			validator: {
				validate(value) {
					const month = Number(value.split("/")[0]);
					const year = Number(value.split("/")[1]);

					/* eslint-disable no-magic-numbers */
					if (month > 12 || month < 1) {
						return false;
					}

					if (year < ((new Date()).getFullYear() - 2000)) {
						return false;
					}
					/* eslint-enable no-magic-numbers */

					return true;
				}
			}
		});
	};
}
