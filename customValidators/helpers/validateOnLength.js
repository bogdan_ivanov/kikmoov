export function validateOnLength(attribute, length) {
    return function (values) {
        return values[attribute].length >= length;
    };
}
