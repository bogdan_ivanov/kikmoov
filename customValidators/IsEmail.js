import { registerDecorator } from "react-formawesome-core/class-validator";

/* eslint-disable-next-line */
export const rule = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export function IsEmail(validationOptions) {
    return function (object, propertyName) {
        registerDecorator({
            name: "IsEmail",
            target: object.constructor,
            propertyName: propertyName,
            options: validationOptions,
            validator: {
                validate(value) {
                    return rule.test(value.toString().toLowerCase());
                }
            }
        });
    };
}
