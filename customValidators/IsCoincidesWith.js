import { registerDecorator } from "react-formawesome-core/class-validator";

export function IsCoincidesWith(property, validationOptions) {
    return function (object, propertyName) {
        registerDecorator({
            name: "IsCoincidesWith",
            target: object.constructor,
            propertyName: propertyName,
            constraints: [property],
            options: validationOptions,
            validator: {
                validate(value, args) {
                    return value === args.object[property];
                }
            }
        });
    };
}
