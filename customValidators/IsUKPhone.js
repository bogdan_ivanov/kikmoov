import { registerDecorator } from "react-formawesome-core/class-validator";

const availableFormats = [
	{ reg: "^(07)+(\\d{9})", message: "07XX XXXX XXX" }, // Most common format should be first
	{ reg: "^(01)+(\\d{8})", message: "01XX XXXX XX" },
	{ reg: "^(011)+(\\d{8})", message: "011X XXXX XXX" },
	{ reg: "^(01)+(\\d{9})", message: "01XX XXXX XXX" },
	{ reg: "^(02)+(\\d{9})", message: "02XX XXXX XXX" },
	{ reg: "^(03)+(\\d{9})", message: "03XX XXXX XXX" },
	{ reg: "^(05)+(\\d{9})", message: "05XX XXXX XXX" },
	{ reg: "^(08)+(\\d{9})", message: "08XX XXXX XXX" },
	{ reg: "^(09)+(\\d{9})", message: "09XX XXXX XXX" },
	{ reg: "^(01)+(\\d)+(1)+(\\d{7})", message: "01X1 XXXX XXX" },
	{ reg: "^(013397)+(\\d{5})", message: "0133 97XX XXX" },
	{ reg: "^(013398)+(\\d{5})", message: "0133 98XX XXX" },
	{ reg: "^(013873)+(\\d{5})", message: "0138 73XX XXX" },
	{ reg: "^(015242)+(\\d{5})", message: "0152 42XX XXX" },
	{ reg: "^(015394)+(\\d{5})", message: "0153 94XX XXX" },
	{ reg: "^(015395)+(\\d{5})", message: "0153 95XX XXX" },
	{ reg: "^(015396)+(\\d{5})", message: "0153 96XX XXX" },
	{ reg: "^(016973)+(\\d{5})", message: "0169 73XX XXX" },
	{ reg: "^(016974)+(\\d{5})", message: "0169 74XX XXX" },
	{ reg: "^(016977)+(\\d{4})", message: "0169 77XX XX" },
	{ reg: "^(017683)+(\\d{4})", message: "0176 83XX XX" },
	{ reg: "^(017684)+(\\d{4})", message: "0176 84XX XX" },
	{ reg: "^(017687)+(\\d{4})", message: "0176 87XX XX" },
	{ reg: "^(019467)+(\\d{4})", message: "0194 67XX XX" },
	{ reg: "^(019755)+(\\d{4})", message: "0197 55XX XX" },
	{ reg: "^(019756)+(\\d{4})", message: "0197 56XX XX" },
	{ reg: "^(0800)+(\\d{6})", message: "0800 XXXX XX" }
];

export function IsUKPhone(validationOptions) {
	return function (object, propertyName) {
		registerDecorator({
			name: "IsUKPhone",
			target: object.constructor,
			propertyName: propertyName,
			options: validationOptions,
			validator: {
				validate(value) {
					return !!availableFormats.find(({ reg }) => (new RegExp(reg + "$")).test(value.replace(/\s/g, "")));
				},
				defaultMessage({ value }) {
					const possibleMessages = availableFormats
						.filter(({ reg }) => (new RegExp(reg.replace("+", "") + "?")).test(value.replace(/\s/g, "")));

					const mostPossibleMessage = possibleMessages
						.reduce((existMessage, { message }) => {
							if (message.replace("X").length > existMessage.length) {
								return message;
							}

							return existMessage;
						}, "") || availableFormats[0].message;

					return `Phone probably should be ${mostPossibleMessage}`;
				}
			}
		});
	};
}
