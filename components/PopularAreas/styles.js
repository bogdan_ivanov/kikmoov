import { StyleSheet, Dimensions } from "react-native";

import { Color, Font } from "@constants/UI";

/* eslint-disable-next-line no-magic-numbers */
const SIZE = Dimensions.get("window").width * .42;

export const styles = StyleSheet.create({
    rootContainer: {
        justifyContent: "space-between",
        alignItems: "stretch",

        flexDirection: "row",
        flexWrap: "wrap"
    },
    title: {
        fontFamily: Font.type.semiBold,
        fontSize: Font.size.l,

        color: Color.grayDark,

        marginBottom: 15,

        textAlign: "center",

        width: "100%"
    },
    areaImage: {
        flex: 1,

        paddingLeft: 13,
        paddingBottom: 15,

        flexDirection: "row",
        alignItems: "flex-end"
    },
    areaTitle: {
        fontFamily: Font.type.semiBold,
        fontSize: Font.size.m,
        color: "#000",

        marginTop: 5,
        marginLeft: 3
    },
    wideAreaWrapper: {
        width: "100%",
        height: SIZE,

        marginBottom: 15
    },
    secondaryAreaWrapper: {
        marginBottom: 15,

        height: SIZE,
        width: SIZE
    }
});
