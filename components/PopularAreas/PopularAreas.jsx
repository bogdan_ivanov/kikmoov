import * as React from "react";
import shortId from "shortid";
import PropTypes from "prop-types";
import { View, Text } from "react-native";

import { CacheManager } from "@utils/CacheManager";

import { AreaItem } from "./AreaItem";
import { styles } from "./styles";

const PopularAreasPropTypes = {
    areas: PropTypes.array.isRequired
};

export class PopularAreas extends React.PureComponent {
    static propTypes = PopularAreasPropTypes;

    constructor(props) {
        super(props);

        this.state = {
            loading: true
        };

        CacheManager.createTempDir("images").then(() => this.setState({ loading: false }));
    }

    render() {
        return (
            <View style={styles.rootContainer}>
                <Text style={styles.title}>Popular areas</Text>
                {!this.state.loading && this.renderAreas()}
            </View>
        );
    }

    renderAreas = () => {
        return (
            <React.Fragment>
                {this.props.areas.map((area, index) => {
                    /* eslint-disable-next-line no-magic-numbers */
                    return index % 3
                        ? (
                            <View key={shortId.generate()} style={styles.secondaryAreaWrapper}>
                                <AreaItem {...area} />
                            </View>
                        )
                        : (
                            <View key={shortId.generate()} style={styles.wideAreaWrapper}>
                                <AreaItem {...area} />
                            </View>
                        );
                })}
            </React.Fragment>
        );
    }
}
