import * as React from "react";
import PropTypes from "prop-types";
import { withNavigation } from "react-navigation";
import { Text, Image, TouchableOpacity } from "react-native";

import { CacheManager } from "@utils/CacheManager";
import { FilterTypes } from "@constants/API";

import { NavigationPropTypes } from "../../navigation/NavigationPropTypes";
import { styles } from "./styles";

const AreaItemPropTypes = {
    id: PropTypes.number,
    title: PropTypes.string,
    imgSource: PropTypes.string,

    ...NavigationPropTypes
};

@withNavigation
export class AreaItem extends React.PureComponent {
    static propTypes = AreaItemPropTypes;

    state = {
        imgSource: undefined
    };

    async componentDidMount() {
        if (this.props.imgSource) {
            this.setState({
                imgSource: await CacheManager.writeOrRead(this.props.imgSource, "images")
            });
        }
    }

    async componentDidUpdate(prevProps) {
        if (!prevProps.imgSource && this.props.imgSource) {
            /* eslint-disable-next-line react/no-did-update-set-state */
            this.setState({
                imgSource: await CacheManager.writeOrRead(this.props.imgSource, "images")
            });
        }
    }

    render() {
        return (
            <TouchableOpacity
                style={{ flex: 1 }}
                onPress={this.handlePress}
                activeOpacity={this.props.id !== undefined ? .2 : 1}
            >
                <Image resizeMode="stretch" style={styles.areaImage} source={{ uri: this.state.imgSource }} />
                <Text style={styles.areaTitle}>{this.props.title || ""}</Text>
            </TouchableOpacity>
        );
    }

    handlePress = () => {
        if (this.props.id === undefined) {
            return;
        }

        this.props.navigation.navigate("Search", {
            duration: FilterTypes.DURATION.MONTHLY,
            value: this.props.title,
            type: "area"
        });
    }
}
