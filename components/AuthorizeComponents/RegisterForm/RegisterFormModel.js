import { ModelValidator } from "react-formawesome-core";
import * as ClassValidator from "react-formawesome-core/class-validator";

import { IsEmail } from "../../../customValidators/IsEmail";
import { IsCoincidesWith } from "../../../customValidators/IsCoincidesWith";

export const emailValidationLength = 6;
export const nameValidationLength = 3;

export class RegisterFormModel {
    @ClassValidator.IsDefined({
        groups: ["firstname"],
        message: "First name is required"
    })
    @ClassValidator.IsNotEmpty({
        groups: ["firstname"],
        message: "First name is required"
    })
    firstname = undefined;

    @ClassValidator.IsDefined({
        groups: ["lastname"],
        message: "Last name is required"
    })
    @ClassValidator.IsNotEmpty({
        groups: ["lastname"],
        message: "Last name is required"
    })
    lastname = undefined;

    @ClassValidator.IsDefined({
        groups: ["email"],
        message: "email is required"
    })
    @ClassValidator.IsNotEmpty({
        groups: ["email"],
        message: "email is required"
    })
    @IsEmail({
        groups: ["email"],
        message: "invalid email"
    })
    email = undefined;

    @ClassValidator.IsDefined({
        groups: ["password"],
        message: "password is required"
    })
    @ClassValidator.IsNotEmpty({
        groups: ["password"],
        message: "password is required"
    })
    password = undefined;

    @ClassValidator.IsDefined({
        groups: ["repeatPassword"],
        message: "passwords do not match"
    })
    @ClassValidator.IsNotEmpty({
        groups: ["repeatPassword"],
        message: "passwords do not match"
    })
    @IsCoincidesWith("password", {
        groups: ["repeatPassword"],
        message: "passwords do not match"
    })
    repeatPassword = undefined
}

export const validator = new ModelValidator(RegisterFormModel);
