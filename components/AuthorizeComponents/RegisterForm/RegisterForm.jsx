import * as React from "react";
import { withNavigation } from "react-navigation";
import { Text, Keyboard, ActivityIndicator } from "react-native";
import { Form, FormGroup, Input, ErrorTip, UnparsedErrorContainer } from "react-native-formawesome";

import { Color } from "@constants/UI";
import { RFASubmitButton } from "@components/partials/RFAElements";

import { validateOnLength } from "../../../customValidators/helpers/validateOnLength";
import { NavigationPropTypes } from "../../../navigation/NavigationPropTypes";

import { formStyles } from "../formStyles";
import { signUp } from "../../../statelessActions";
import { validator, emailValidationLength, nameValidationLength } from "./RegisterFormModel";

@withNavigation
export class RegisterForm extends React.Component {
    static propTypes = NavigationPropTypes;

    componentWillUnmount() {
        validator.clear();
    }

    render() {
        return (
            <Form
                handleUnparsedErrors
                validator={validator}
                style={formStyles.form}
                onSubmit={this.handleSubmit}
                errorParser={this.errorParser}
            >
                <FormGroup attribute="firstname" validateOn={validateOnLength("firstname", nameValidationLength)}>
                    <Input
                        placeholder="first name"
                        underlineColorAndroid="transparent"
                        onChange={() => this.forceUpdate()}
                        onErrorStyles={formStyles.inputError}
                        style={[formStyles.input, { marginTop: 0 }]}
                    />
                    <ErrorTip style={formStyles.error} />
                </FormGroup>
                <FormGroup attribute="lastname" validateOn={validateOnLength("lastname", nameValidationLength)}>
                    <Input
                        placeholder="last name"
                        underlineColorAndroid="transparent"
                        onChange={() => this.forceUpdate()}
                        onErrorStyles={formStyles.inputError}
                        style={formStyles.input}
                    />
                    <ErrorTip style={formStyles.error} />
                </FormGroup>
                <FormGroup attribute="email" validateOn={validateOnLength("email", emailValidationLength)}>
                    <Input
                        placeholder="email"
                        style={formStyles.input}
                        keyboardType="email-address"
                        underlineColorAndroid="transparent"
                        onChange={() => this.forceUpdate()}
                        onErrorStyles={formStyles.inputError}
                    />
                    <ErrorTip style={formStyles.error} />
                </FormGroup>
                <FormGroup attribute="password" validateOn="change">
                    <Input
                        secureTextEntry
                        placeholder="password"
                        style={formStyles.input}
                        underlineColorAndroid="transparent"
                        onChange={() => this.forceUpdate()}
                        onErrorStyles={formStyles.inputError}
                    />
                    <ErrorTip style={formStyles.error} />
                </FormGroup>
                <FormGroup attribute="repeatPassword" validateOn={this.validateOnPassword}>
                    <Input
                        secureTextEntry
                        style={formStyles.input}
                        placeholder="re-enter password"
                        underlineColorAndroid="transparent"
                        onChange={() => this.forceUpdate()}
                        onErrorStyles={formStyles.inputError}
                    />
                    <ErrorTip style={formStyles.error} />
                </FormGroup>
                <UnparsedErrorContainer style={formStyles.errorContaier}>
                    {this.renderUnparsedError}
                </UnparsedErrorContainer>
                <RFASubmitButton
                    style={[formStyles.submitButton, this.canSubmit && { backgroundColor: Color.red }]}
                    loadingComponent={<ActivityIndicator color="#fff" size="small" />}
                >
                    <Text style={[formStyles.buttonText, this.canSubmit && { color: "#fff" }]}>Create account</Text>
                </RFASubmitButton>
            </Form>
        );
    }

    get canSubmit() {
        return Object.keys(validator.modelValues).filter((value) => value).length;
    }

    renderUnparsedError = (errorMessage) => {
        return <Text style={formStyles.errorContaierText}>{errorMessage}</Text>;
    }

    errorParser = (error) => {
        return error.response.data.errors || error.response.data.message;
    }

    validateOnPassword = (values) => {
        return values.password && values.password.length <= values.repeatPassword.length;
    }

    handleSubmit = async (values) => {
        delete values.repeatPassword;
        Keyboard.dismiss();

        await signUp(values);

        this.props.navigation.navigate("Main");
    }
}
