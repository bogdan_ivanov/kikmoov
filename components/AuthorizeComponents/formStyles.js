import { StyleSheet } from "react-native";

import { Color, buttonText, filledButton, borderButton, Font, underlineInput } from "@constants/UI";

export const formStyles = StyleSheet.create({
    form: {
        width: "100%"
    },
    input: {
        ...underlineInput,

        marginTop: 25,
        paddingBottom: 7
    },
    inputError: {
        borderColor: Color.redError,
        color: Color.redError
    },
    error: {
        color: Color.redError,
        fontFamily: Font.type.light
    },
    errorContaier: {
        fontFamily: Font.type.medium,

        backgroundColor: Color.red,
        borderRadius: 5,
        padding: 7,
        marginTop: 25
    },
    errorContaierText: {
        color: "#fff",
        textAlign: "center"
    },
    submitButton: {
        marginTop: 76,
        ...filledButton,
        backgroundColor: Color.grayLight
    },
    registerButton: {
        marginTop: 27,
        ...borderButton,
        borderColor: Color.gray
    },
    buttonText: {
        ...buttonText,
        color: Color.gray
    }
});
