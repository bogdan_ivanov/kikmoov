import PropTypes from "prop-types";

import { NavigationPropTypes } from "../../../navigation/NavigationPropTypes";

export const SignOutPropTypes = {
    signOut: PropTypes.func.isRequired,

    ...NavigationPropTypes
};
