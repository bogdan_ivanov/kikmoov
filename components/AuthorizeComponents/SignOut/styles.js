import { StyleSheet } from "react-native";

import { Font, Color, buttonGroupContainer, buttonText, filledButton, modalOverlay } from "@constants/UI";

export const styles = StyleSheet.create({
    overlay: {
        ...modalOverlay
    },
    container: {
        borderRadius: 5,
        overflow: "hidden",

        backgroundColor: "#fff"
    },
    footer: {
        ...buttonGroupContainer,
    },
    titleText: {
        fontSize: Font.size.m,
        fontFamily: Font.type.medium,

        marginTop: 35,
        marginBottom: 35,
        marginLeft: 55,
        marginRight: 55,

        textAlign: "center",

        color: Color.red
    },
    cancelButtonText: {
        ...buttonText,

        color: Color.gray
    },
    signOutButtonText: {
        ...buttonText,

        color: Color.red
    },
    separator: {
        width: 1,

        marginTop: 10,
        marginBottom: 10,

        backgroundColor: Color.grayLight
    },
    button: {
        ...filledButton,

        paddingTop: filledButton.padding + 10,
        paddingBottom: filledButton.padding + 10,

        flex: 1,

        backgroundColor: "#fff"
    }
});
