import * as React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { View, Text, Modal } from "react-native";
import Touchable from "react-native-platform-touchable";

import { signOut } from "@store/actions/security";
import { StatusBarManager } from "@utils/StatusBarManager";

import { styles } from "./styles";
import { SignOutPropTypes } from "./SignOutPropTypes";

const mapDispatchToProps = (dispatch) => bindActionCreators({
    signOut: signOut
}, dispatch);

@connect(undefined, mapDispatchToProps)
export class SignOut extends React.Component {
    static propTypes = SignOutPropTypes;

    state = {
        modalVisible: false
    };

    render() {
        return (
            <React.Fragment>
                {this.props.children(this.handleModalOpen)}
                <Modal
                    visible={this.state.modalVisible}
                    onRequestClose={this.handleModalClose}
                    animationType="fade"
                    transparent
                >
                    <View style={styles.overlay}>
                        <View style={styles.container}>
                            <Text style={styles.titleText}>Are you sure you want to sign out?</Text>
                            <View style={styles.footer}>
                                <Touchable style={styles.button} onPress={this.handleModalClose}>
                                    <Text style={styles.cancelButtonText}>Cancel</Text>
                                </Touchable>
                                <View style={styles.separator} />
                                <Touchable style={styles.button} onPress={this.handleSignOut}>
                                    <Text style={styles.signOutButtonText}>Sign out</Text>
                                </Touchable>
                            </View>
                        </View>
                    </View>
                </Modal>
            </React.Fragment>
        );
    }

    handleModalClose = () => {
        StatusBarManager.restoreState();
        this.setState({ modalVisible: false });
    }

    handleModalOpen = () => {
        StatusBarManager.setState("hidden");
        this.setState({ modalVisible: true });
    }

    handleSignOut = () => {
        this.props.signOut();

        this.handleModalClose();
        this.props.navigation.toggleDrawer();
        this.props.navigation.navigate("Main");
    }
}
