import PropTypes from "prop-types";

export const SignInWithGooglePropTypes = {
    googleSignIn: PropTypes.func.isRequired,

    onRequestStart: PropTypes.func.isRequired,
    onRequestEnd: PropTypes.func.isRequired,
};
