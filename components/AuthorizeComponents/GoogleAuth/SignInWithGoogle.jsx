import { Google } from "expo";
import * as React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { TouchableOpacity, Image, Platform } from "react-native";
import {
    GOOGLE_ANDROID_STANDALONE_APP_CLIENT_ID,
    GOOGLE_IOS_STANDALONE_APP_CLIENT_ID,
    GOOGLE_ANDROID_CLIENT_ID,
    GOOGLE_IOS_CLIENT_ID
} from "react-native-dotenv";

import { Images } from "@assets/Images";
import { googleSignIn } from "@store/actions/security";

import { styles } from "./styles";
import { SignInWithGooglePropTypes } from "./SignInWithGooglePropTypes";

const mapDispatchToProps = (dispatch) => bindActionCreators({
    googleSignIn
}, dispatch);

@connect(undefined, mapDispatchToProps)
export class SignInWithGoogle extends React.Component {
    static propTypes = SignInWithGooglePropTypes;

    render() {
        return (
            <TouchableOpacity onPress={this.handlePress}>
                <Image
                    source={Images.googleIcon}
                    style={styles.button}
                />
            </TouchableOpacity>
        );
    }

    handlePress = async () => {
        let result;

        try {
            result = await Google.logInAsync({
                androidStandaloneAppClientId: GOOGLE_ANDROID_STANDALONE_APP_CLIENT_ID,
                iosStandaloneAppClientId: GOOGLE_IOS_STANDALONE_APP_CLIENT_ID,
                androidClientId: GOOGLE_ANDROID_CLIENT_ID,
                iosClientId: GOOGLE_IOS_CLIENT_ID,
                scopes: ["profile", "email"],
                behavior: "web"
            });
        } catch (error) {
            return;
        }

        if (result.type !== "success") {
            return;
        }

        this.props.onRequestStart();
        await this.props.googleSignIn({
            id: result.user.id,
            platform: Platform.OS,
            idToken: result.idToken,
            email: result.user.email,
            firstName: result.user.givenName,
            lastName: result.user.familyName
        });
        this.props.onRequestEnd();
    }
}
