import { ModelValidator } from "react-formawesome-core";
import * as ClassValidator from "react-formawesome-core/class-validator";

import { IsEmail } from "../../../customValidators/IsEmail";

export const emailValidationLength = 6;

export class LoginFormModel {
    @ClassValidator.IsDefined({
        groups: ["email"],
        message: "email is required"
    })
    @ClassValidator.IsNotEmpty({
        groups: ["email"],
        message: "email is required"
    })
    @IsEmail({
        groups: ["email"],
        message: "invalid email"
    })
    email = undefined;

    @ClassValidator.IsDefined({
        groups: ["password"],
        message: "password is required"
    })
    @ClassValidator.IsNotEmpty({
        groups: ["password"],
        message: "password is required"
    })
    password = undefined;
}

export const validator = new ModelValidator(LoginFormModel);
