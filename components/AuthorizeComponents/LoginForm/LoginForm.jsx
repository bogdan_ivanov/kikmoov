import * as React from "react";
import { withNavigation } from "react-navigation";
import Touchable from "react-native-platform-touchable";
import { Text, Keyboard, ActivityIndicator } from "react-native";
import { Form, FormGroup, Input, ErrorTip, UnparsedErrorContainer } from "react-native-formawesome";

import { Color } from "@constants/UI";
import { RFASubmitButton } from "@components/partials/RFAElements";

import { validateOnLength } from "../../../customValidators/helpers/validateOnLength";

import { LoginFormPropTypes } from "./LoginFormPropTypes";
import { validator, emailValidationLength } from "./LoginFormModel";

import { formStyles } from "../formStyles";

@withNavigation
export class LoginForm extends React.PureComponent {
    static propTypes = LoginFormPropTypes;

    componentWillUnmount() {
        validator.clear();
    }

    render() {
        return (
            <Form
                handleUnparsedErrors
                validator={validator}
                style={formStyles.form}
                onSubmit={this.handleSubmit}
                errorParser={this.errorParser}
            >
                <FormGroup attribute="email" validateOn={validateOnLength("email", emailValidationLength)}>
                    <Input
                        placeholder="email"
                        keyboardType="email-address"
                        underlineColorAndroid="transparent"
                        onChange={() => this.forceUpdate()}
                        onErrorStyles={formStyles.inputError}
                        style={[formStyles.input, { marginTop: 0 }]}
                    />
                    <ErrorTip style={formStyles.error} />
                </FormGroup>
                <FormGroup attribute="password" validateOn="change">
                    <Input
                        secureTextEntry
                        placeholder="password"
                        style={formStyles.input}
                        underlineColorAndroid="transparent"
                        onChange={() => this.forceUpdate()}
                        onErrorStyles={formStyles.inputError}
                    />
                    <ErrorTip style={formStyles.error} />
                </FormGroup>
                <UnparsedErrorContainer style={formStyles.errorContaier}>
                    {this.renderUnparsedError}
                </UnparsedErrorContainer>
                <RFASubmitButton
                    style={[formStyles.submitButton, this.canSubmit && { backgroundColor: Color.red }]}
                    loadingComponent={<ActivityIndicator color="#fff" size="small" />}
                >
                    <Text
                        style={[formStyles.buttonText, this.canSubmit && { color: "#fff" }]}
                    >
                        Sign in
                    </Text>
                </RFASubmitButton>
                <Touchable
                    onPress={this.handleRegister}
                    style={formStyles.registerButton}
                >
                    <Text style={formStyles.buttonText}>Create account</Text>
                </Touchable>
            </Form>
        );
    }

    get canSubmit() {
        return validator.modelValues.password && validator.modelValues.email;
    }

    renderUnparsedError = (errorMessage) => {
        return <Text style={formStyles.errorContaierText}>{errorMessage}</Text>;
    }

    errorParser = (error) => {
        return error.response.data.errors || error.response.data.message;
    }

    handleRegister = () => {
        this.props.navigation.navigate("SignUp");
    }

    handleSubmit = async (values) => {
        Keyboard.dismiss();
        const response = await this.props.signIn(values);

        // Throw to form error parser
        if (response && response.error) {
            throw response.error;
        }

        this.props.navigation.navigate("Main");
    }
}
