import PropTypes from "prop-types";

import { NavigationPropTypes } from "../../../navigation/NavigationPropTypes";

export const LoginFormPropTypes = {
    signIn: PropTypes.func.isRequired,
    ...NavigationPropTypes
};
