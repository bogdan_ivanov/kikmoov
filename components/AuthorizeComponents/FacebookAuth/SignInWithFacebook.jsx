import Axios from "axios";
import * as React from "react";
import { Facebook } from "expo";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Toaster } from "react-native-toastboard";
import { FACEBOOK_APP_ID } from "react-native-dotenv";
import { TouchableOpacity, Image } from "react-native";

import { Images } from "@assets/Images";
import { facebookSignIn } from "@store/actions/security";

import { styles } from "./styles";
import { SignInWithFacebookPropTypes } from "./SignWithFacebookPropTypes";

const mapDispatchToProps = (dispatch) => bindActionCreators({
    facebookSignIn
}, dispatch);

@connect(undefined, mapDispatchToProps)
export class SignInWithFacebook extends React.Component {
    static propTypes = SignInWithFacebookPropTypes;

    render() {
        return (
            <TouchableOpacity onPress={this.handlePress}>
                <Image
                    source={Images.facebookIcon}
                    style={styles.button}
                />
            </TouchableOpacity>
        );
    }

    handlePress = async () => {
        const result = await Facebook.logInWithReadPermissionsAsync(FACEBOOK_APP_ID, {
            permissions: ["public_profile", "email"]
        });

        const { type, token } = result;

        if (type !== "success") {
            type !== "cancel" && Toaster.error({});
            return;
        }

        let response;
        try {
            response = await Axios.get("https://graph.facebook.com/me", {
                params: {
                    access_token: token,
                    fields: "name,email"
                }
            });
        } catch (error) {
            return Toaster.error(error);
        }

        this.props.onRequestStart();
        await this.props.facebookSignIn({
            authToken: token,
            id: response.data.id,
            email: response.data.email,
            lastName: response.data.name.split(" ")[1],
            firstName: response.data.name.split(" ")[0]
        });
        this.props.onRequestEnd();
    };
}
