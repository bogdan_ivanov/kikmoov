import PropTypes from "prop-types";

export const SignInWithFacebookPropTypes = {
    facebookSignIn: PropTypes.func.isRequired,

    onRequestStart: PropTypes.func.isRequired,
    onRequestEnd: PropTypes.func.isRequired,
};
