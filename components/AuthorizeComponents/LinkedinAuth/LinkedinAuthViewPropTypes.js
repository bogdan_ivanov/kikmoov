import PropTypes from "prop-types";

export const LinkedinAuthViewPropTypes = {
    onComplete: PropTypes.func.isRequired,
    linkedinSignIn: PropTypes.func.isRequired,

    onRequestStart: PropTypes.func.isRequired,
    onRequestEnd: PropTypes.func.isRequired,
};
