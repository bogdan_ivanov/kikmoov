import * as React from "react";
import { encode } from "base-64";
import { connect } from "react-redux";
import { WebView } from "react-native";
import { bindActionCreators } from "redux";
import URLSearchParams from "url-search-params";
import { LINKEDIN_REDIRECT_URL, LINKEDIN_CLIENT_ID } from "react-native-dotenv";

import { linkedinSignIn } from "@store/actions/security";

import { LinkedinAuthViewPropTypes } from "./LinkedinAuthViewPropTypes";

const mapDispatchToProps = (dispatch) => bindActionCreators({
    linkedinSignIn
}, dispatch);

@connect(undefined, mapDispatchToProps)
export class LinkedinAuthView extends React.Component {
    static uri = "https://www.linkedin.com/oauth/v2/authorization";
    static propTypes = LinkedinAuthViewPropTypes;

    CSRF = encode((new Date()).toTimeString());

    render() {
        return (
            <WebView
                {...this.props}
                source={{ uri: this.uri }}
                onNavigationStateChange={this.handleNavigationStateChange}
            />
        );
    }

    get uri() {
        return LinkedinAuthView.uri
            + `?response_type=code&redirect_uri=${LINKEDIN_REDIRECT_URL}`
            + `&client_id=${LINKEDIN_CLIENT_ID}`
            + `&state=${this.CSRF}`;
    }

    handleNavigationStateChange = async ({ url }) => {
        const redirectReg = new RegExp(`^${LINKEDIN_REDIRECT_URL}`);
        if (!redirectReg.test(url)) {
            return;
        }

        const params = url.match(/code=.*/);

        if (!params) {
            return this.props.onComplete();
        }

        const searchParams = new URLSearchParams(params[0]);

        if (!searchParams.get("code")) {
            return this.props.onComplete();
        }

        this.props.onRequestStart();
        await this.props.linkedinSignIn({ code: searchParams.get("code") });
        this.props.onRequestEnd();
    }
}
