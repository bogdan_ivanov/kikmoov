import * as React from "react";
import { TouchableOpacity, Image, Modal, View } from "react-native";

import { Images } from "@assets/Images";

import { SignInWithLinkedinPropTypes } from "./SignInWithLinkedinPropTypes";
import { LinkedinAuthView } from "./LinkedinAuthView";
import { styles } from "./styles";

export class SignInWithLinkedin extends React.Component {
    static propTypes = SignInWithLinkedinPropTypes;

    state = {
        modalOpened: false,
        canClose: true
    };

    render() {
        return (
            <React.Fragment>
                <Modal
                    animationType="fade"
                    visible={this.state.modalOpened}
                    onRequestClose={this.handleModalClose}
                >
                    <View style={styles.modalContainer}>
                        <TouchableOpacity
                            style={styles.closeButton}
                            onPress={this.handleModalClose}
                            hitSlop={{ left: 10, right: 10, top: 10, bottom: 10 }}
                        >
                            <Image
                                source={Images.cancelBooking}
                                style={styles.crossIcon}
                                resizeMode="cover"
                            />
                        </TouchableOpacity>
                        <LinkedinAuthView
                            style={styles.authView}
                            onComplete={this.handleModalClose}
                            onRequestEnd={this.props.onRequestEnd}
                            onRequestStart={this.handleRequestStart}
                        />
                    </View>
                </Modal>
                <TouchableOpacity onPress={this.handleModalOpen}>
                    <Image
                        source={Images.linkedinIcon}
                        style={styles.openButton}
                    />
                </TouchableOpacity>
            </React.Fragment>
        );
    }

    handleModalOpen = () => {
        this.setState({
            modalOpened: true
        });
    }

    handleModalClose = () => {
        if (!this.state.canClose) {
            return;
        }

        this.setState({
            modalOpened: false
        });
    }

    handleRequestStart = () => {
        this.setState({ canClose: false });
        this.props.onRequestStart();
    }

}
