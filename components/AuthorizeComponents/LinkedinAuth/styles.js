import { StyleSheet, Platform } from "react-native";

export const styles = StyleSheet.create({
    openButton: {
        width: 45,
        height: 45,
    },
    authView: {
        zIndex: 1,
        flex: 1,
        marginTop: Platform.select({ ios: 32.5, android: 12.5 }),
        marginBottom: 10,
        marginLeft: 15,
        marginRight: 15,
        borderWidth: 2,
        borderColor: "#c9c9c9",
    },
    crossIcon: {
        width: 25,
        height: 25,
        overflow: "hidden",
        flex: 1,
        alignSelf: "center",
        backgroundColor: "black",
        borderRadius: 12.5,

        tintColor: "#fff"
    },
    closeButton: {
        width: 25,
        height: 25,
        position: "absolute",
        top: Platform.select({ ios: 20, android: 0 }),
        left: 2.5,

        marginTop: 15,

        zIndex: 2
    },
    modalContainer: {
        flex: 1,

        paddingTop: 15,
        paddingBottom: 15,

        backgroundColor: "rgba(0,0,0,0.5)"
    }
});
