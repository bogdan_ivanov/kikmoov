import PropTypes from "prop-types";

export const SignInWithLinkedinPropTypes = {
    onRequestStart: PropTypes.func.isRequired,
    onRequestEnd: PropTypes.func.isRequired
};
