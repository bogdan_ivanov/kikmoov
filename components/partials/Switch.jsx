import * as React from "react";
import PropTypes from "prop-types";
import { StyleSheet, TouchableOpacity, Animated, Easing, View, Text } from "react-native";

import { Color, Font } from "@constants/UI";

const SwitchPropTypes = {
    onChange: PropTypes.func.isRequired,
    value: PropTypes.bool.isRequired
};

const switcherWidth = 20;
const Duration = {
    long: 180,
    short: 150
};

export class Switch extends React.Component {
    static propTypes = SwitchPropTypes;

    constructor(props) {
        super(props);

        this.translateX = new Animated.Value(props.value ? switcherWidth : 0);
        this.opacity = new Animated.Value(Number(props.value));
    }

    render() {
        return (
            <View style={styles.root}>
                <Text style={[styles.text, { color: this.props.value ? Color.red : Color.gray }]}>
                    {this.props.value ? "on" : "off"}
                </Text>
                <TouchableOpacity activeOpacity={1} style={styles.container} onPress={this.handlePress}>
                    <Animated.View style={[styles.track, { transform: [{ scale: this.scale }] }]} />
                    <Animated.View
                        style={[styles.thumbDefault, { transform: [{ translateX: this.translateX }] }]}
                    />
                    <Animated.View style={this.ThumbStyle} />
                </TouchableOpacity>
            </View>
        );
    }

    get ThumbStyle() {
        return [
            styles.thumbActive,
            { opacity: this.opacity, transform: [{ translateX: this.translateX }] }
        ];
    }

    get scale() {
        const androidScaleCrutch = 0.01;

        return this.opacity.interpolate({
            inputRange: [0, 1],
            outputRange: [androidScaleCrutch, 1]
        });
    }

    handlePress = () => {
        const value = !this.props.value;
        this.props.onChange(value);

        Animated.timing(this.translateX, {
            toValue: value ? switcherWidth : 0,
            duration: Duration.long,
            easing: Easing.in(),
            useNativeDriver: true
        }).start();
        Animated.timing(this.opacity, {
            toValue: Number(value),
            duration: Duration.short,
            useNativeDriver: true
        }).start();
    }
}

const styles = StyleSheet.create({
    root: {
        flexDirection: "row",
        alignItems: "center"
    },
    text: {
        fontSize: Font.size.l,
        fontFamily: Font.type.light,
        textAlign: "center",
        marginRight: 10
    },
    track: {
        position: "absolute",

        borderRadius: 13,

        top: 0,
        left: 0,
        right: 0,
        bottom: 0,

        backgroundColor: "#fcd7d0",
    },
    thumbActive: {
        position: "absolute",

        borderRadius: 10,

        marginLeft: 3,

        width: 20,
        height: 20,

        backgroundColor: Color.red,
    },
    container: {
        borderRadius: 13,
        overflow: "hidden",

        width: 46,
        padding: 3,

        position: "relative",

        backgroundColor: Color.grayLight,

        justifyContent: "center"
    },
    thumbDefault: {
        borderRadius: 10,

        width: 20,
        height: 20,

        backgroundColor: Color.gray,
    },
});
