/* eslint-disable max-lines */
import * as React from "react";
import PropTypes from "prop-types";
import { View, TouchableOpacity, Text, FlatList, StyleSheet, Platform } from "react-native";

import { Color, Font } from "@constants/UI";

export const TimeRangeSelectorPropTypes = {
    range: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
    onlyRange: PropTypes.bool,
    onChange: PropTypes.func,
    defaults: PropTypes.shape({
        min: PropTypes.string.isRequired,
        max: PropTypes.string
    }).isRequired,

    currentDate: PropTypes.number.isRequired
};

export class TimeRangeSelector extends React.Component {
    static propTypes = TimeRangeSelectorPropTypes;

    selectedRange = new Map();

    constructor(props) {
        super(props);

        // Check for increase
        props.range.reduce((prev, next, index) => {
            const hours = Number(next.substring(0, 2));
            /* eslint-disable-next-line no-magic-numbers */
            const minutes = Number(next.substring(3));
            if (
                prev.hours > hours
                || (prev.hours === hours && prev.minutes > minutes)
                || (prev.hours === hours && prev.minutes === minutes)
            ) {
                throw new Error(`Time item [${index}] should be greater than time item [${index - 1}]`);
            }
            return { hours, minutes };
        }, { hours: 0, minutes: 0 });

        this.selectedRange.set(this.props.defaults.min, true);
        this.props.defaults.max && this.selectedRange.set(this.props.defaults.max, true);

        this.state = {
            min: this.props.range.findIndex((item) => this.props.defaults.min === item),
            max: this.props.defaults.max
                ? this.props.range.findIndex((item) => this.props.defaults.max === item)
                : undefined,

            isFirstPress: false
        };

        this.props.range
            .filter((time, index) => index >= this.state.min && index <= this.state.max)
            .forEach((time) => this.selectedRange.set(time, true));
    }

    render() {
        return (
            <View style={styles.root}>
                <FlatList
                    numColumns={4}
                    scrollEnabled={false}
                    data={this.props.range}
                    renderItem={this.renderItem}
                    keyExtractor={this.keyExtractor}
                    contentContainerStyle={styles.contentContainerStyle}
                    extraData={this.state} // Perform update on setState
                />
            </View>
        );
    }

    isDisabled = (time) => {
        const selectedTime = new Date(this.props.currentDate);
        selectedTime.setUTCHours(Number(time.split(":")[0]));

        const currentTime = new Date();

        return selectedTime.getTime() < currentTime.getTime();
    }

    renderItem = ({ item, index }) => {
        const isActive = this.selectedRange.get(item);
        const isFirst = isActive && index === this.state.min;
        const isLast = (isActive && index === this.state.max) || (isActive && this.state.max === undefined);

        const itemStyle = [
            styles.itemDefault,
            isActive && styles.itemActive
        ];
        const backgroundStyles = [
            isActive && styles.itemBackgroundDefault,
            isLast && styles.itemRoundedRight,
            isFirst && styles.itemRoundedLeft,
        ];

        const isDisabled = this.isDisabled(item);

        isDisabled && itemStyle.push(styles.itemDisabled);

        return (
            <TouchableOpacity
                key={item}
                activeOpacity={1}
                style={styles.button}
                onPress={!isDisabled ? this.handlePress(item) : undefined}
            >
                <View style={backgroundStyles} />
                <Text style={itemStyle}>
                    {item}
                </Text>
            </TouchableOpacity>
        );
    }

    keyExtractor = (range) => range;

    handlePress = (selectedTime) => () => {
        let { max, min } = this.state;

        this.setState(({ isFirstPress }) => ({
            isFirstPress: !isFirstPress
        }));

        if (this.state.isFirstPress) {
            min = this.props.range.findIndex((time) => time === selectedTime);
            let delta;
            if (this.props.onlyRange) {
                delta = min === (this.props.range.length - 1) ? (min - 1) : (min + 1);
            }

            max = delta;

            this.props.range.forEach((time) => this.selectedRange.set(time, false));
            this.selectedRange.set(selectedTime, true);
        } else { // Second press
            if (this.props.onlyRange) {
                this.props.range.forEach((time) => this.selectedRange.set(time, false));

                const prevMax = max;

                max = this.props.range.findIndex((time) => time === selectedTime);

                if (max === min) {
                    max = prevMax;
                }
            } else {
                max = this.props.range.findIndex((time) => time === selectedTime);
            }
        }

        // Fix min and max after user select
        let swap;
        if (min > max) {
            swap = min;
            min = max;
            max = swap;
        }

        // Set active time on range
        this.props.range
            .filter((time, index) => index >= min && index <= max)
            .forEach((time) => this.selectedRange.set(time, true));

        this.setState({ max, min }, () => {
            this.props.onChange && this.props.onChange({
                min: this.props.range[this.state.min],
                max: this.props.range[this.state.max]
            });
        });
    }
}

const styles = StyleSheet.create({
    root: {
        backgroundColor: "#fff",
    },
    contentContainerStyle: {
        width: "100%",
    },
    itemDefault: {
        marginTop: 5,
        marginBottom: 5,

        color: Color.gray,

        fontSize: Font.size.m,
        fontFamily: Font.type.regular,

        textAlign: "center",

        paddingTop: 10,
        paddingBottom: 10,
    },
    button: {
        width: "25%",
        position: "relative",
    },
    itemBackgroundDefault: {
        position: "absolute",
        top: 5,
        left: -1,
        right: -1,
        bottom: 5,

        ...Platform.select({
            ios: {
                borderWidth: 1,
                borderColor: "transparent",
            },
            android: {}
        }),

        backgroundColor: Color.red,
    },
    itemDisabled: {
        color: Color.grayLight
    },
    itemRoundedLeft: {
        left: 7,
        borderTopLeftRadius: Font.size.m * 2,
        borderBottomLeftRadius: Font.size.m * 2,
    },
    itemRoundedRight: {
        right: 7,
        borderTopRightRadius: Font.size.m * 2,
        borderBottomRightRadius: Font.size.m * 2,
    },
    itemActive: {
        color: "#fff"
    },
});
