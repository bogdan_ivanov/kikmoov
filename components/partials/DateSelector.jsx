import * as React from "react";
import PropTypes from "prop-types";
import { Calendar } from "react-native-calendars";

import { Color, Font } from "@constants/UI";
import { DateManager } from "@utils/DateManager";

const StartDateSelectorPropTypes = {
    value: PropTypes.number.isRequired,
    onChange: PropTypes.func.isRequired
};

export const DateSelector = (props) => (
    <Calendar
        theme={theme}
        minDate={new Date()}
        onDayPress={props.onChange}
        markedDates={{ [DateManager.dateStringFromTimestamp(props.value)]: { selected: true } }}
    />
);

DateSelector.propTypes = StartDateSelectorPropTypes;

const theme = {
    todayTextColor: Color.red,
    arrowColor: Color.grayDark,
    selectedDayTextColor: "#fff",
    selectedDayBackgroundColor: Color.red,

    textDayHeaderFontFamily: Font.type.medium,
    textMonthFontFamily: Font.type.medium,
    textDayFontFamily: Font.type.medium,

    textDayFontSize: Font.size.m,
    textMonthFontSize: Font.size.m,
    textDayHeaderFontSize: Font.size.m,

    dayTextColor: Color.grayDark,
    monthTextColor: Color.grayDark,
    textDisabledColor: Color.grayLight,

    "stylesheet.calendar.header": {
        week: {
            flexDirection: "row",
            justifyContent: "space-around",
            backgroundColor: "#f9f9f9",
            paddingTop: 10,
            paddingBottom: 5
        },
        header: {
            backgroundColor: "#f9f9f9",
            flexDirection: "row",
            justifyContent: "space-between",
            paddingLeft: 10,
            paddingRight: 10,
            alignItems: "center"
        }
    },
    "stylesheet.calendar.main": {
        container: {
            backgroundColor: "#fff"
        }
    }
};
