import shortId from "shortid";
import * as React from "react";
import PropTypes from "prop-types";
import { FlatList, View, StyleSheet, Dimensions, Image } from "react-native";

import { Images } from "@assets/Images";
import { Color, shadowArea } from "@constants/UI";

const ITEM_WIDTH_PERCENTAGE = 0.5;

const RowListPropTypes = {
	items: PropTypes.array.isRequired,
	children: PropTypes.func.isRequired
};

export class RowList extends React.Component {
	static propTypes = RowListPropTypes;

	constructor(props) {
		super(props);

		this.state = {
			isMomentumScroll: false,
			index: 0
		};

		// Approximate initial value
		this.itemWidth = Dimensions.get("window").width * ITEM_WIDTH_PERCENTAGE;
	}

	render() {
		if (!this.props.items.length) {
			return null;
		}

		return (
			<View style={styles.root} onLayout={this.handleContainerLayout}>
				{this.shouldRenderLeftArrow && (
					<Image source={Images.backButton} style={styles.arrowLeft} resizeMode="contain" />
				)}
				<FlatList
					ref={this.getScroller}

					horizontal
					bounces={false}
					showsHorizontalScrollIndicator={false}
					scrollEnabled={this.props.items.length > 2}

					data={this.props.items}

					renderItem={this.renderItem}
					keyExtractor={this.keyExtractor}

					onScrollEndDrag={this.handleScrollEndDrag}

					contentContainerStyle={styles.container}
					ItemSeparatorComponent={() => <View style={styles.separator} />}
				/>
				{this.shouldRenderRightArrow && (
					<Image source={Images.backButton} style={styles.arrowRight} resizeMode="contain" />
				)}
			</View>
		);
	}

	get shouldRenderLeftArrow() {
		return this.props.items.length > 2 && this.state.index > 0;
	}

	get shouldRenderRightArrow() {
		return this.props.items.length > 2 && this.state.index < this.props.items.length - 2;
	}

	getScroller = (ref) => {
		this.scroller = ref;
	}

	handleContainerLayout = ({ nativeEvent: { layout: { width } } }) => {
		this.itemWidth = width * ITEM_WIDTH_PERCENTAGE;
		this.forceUpdate();
    }

	keyExtractor = (item, i) => i.toString();

	handleScrollEndDrag = (event) => {
		if (this.state.isMomentumScroll) {
			return;
		}

		const index = Math.round(event.nativeEvent.contentOffset.x / this.itemWidth);

		if (index >= 0 && index < this.props.items.length - 1) {
			this.scroller.scrollToIndex({ animated: true, index });
			this.setState({ index });
		}
	}

	renderItem = (item) => {
		return (
			<View
				key={shortId.generate()}
				style={{ width: this.itemWidth }}
			>
				{this.props.children(item)}
			</View>
		);
	}
}

const styles = StyleSheet.create({
	separator: {
		width: 1,

		height: "100%",

		backgroundColor: Color.grayLight
	},
	arrowLeft: {
		position: "absolute",
		left: 5,
		top: "50%",

		width: 10,
		height: 10,

		tintColor: Color.red,

		transform: [{ translateY: -5 }],

		zIndex: 1
	},
	arrowRight: {
		position: "absolute",
		right: 5,
		top: "50%",

		width: 10,
		height: 10,

		tintColor: Color.red,

		transform: [{ translateY: -5 }, { rotateZ: "180deg" }],

		zIndex: 1
	},
	container: {
		paddingTop: 5,
		paddingBottom: 5
	},
	root: {
		...shadowArea,
		borderRadius: 5,

		marginTop: 5,
		marginBottom: 5,
		marginLeft: 1.5,
		marginRight: 1.5,

		width: "99%",

		alignSelf: "center",
		alignItems: "center",

		position: "relative"
	}
});
