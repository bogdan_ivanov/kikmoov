import PropTypes from "prop-types";

export const SelectPropTypes = {
    options: PropTypes.arrayOf(PropTypes.shape({
        label: PropTypes.any.isRequired,
        value: PropTypes.any.isRequired
    }).isRequired).isRequired,
    renderLabel: PropTypes.func,
    onChange: PropTypes.func,

    selectedContainerStyles: PropTypes.any,
    optionsContainerStyles: PropTypes.any,
    style: PropTypes.any
};
