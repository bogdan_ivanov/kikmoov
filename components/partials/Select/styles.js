import { StyleSheet } from "react-native";

import { triangle, Color, shadowArea, Font } from "@constants/UI";

export const shadowOffset = 10;

export const styles = StyleSheet.create({
    root: {
        width: "100%",

        flexDirection: "row",

        padding: shadowOffset,

        overflow: "hidden"
    },
    container: {
        flex: 1,

        backgroundColor: "#fff"
    },
    selectedContainer: {
        ...shadowArea,

        borderRadius: 5,

        width: "100%",

        backgroundColor: "#fff",

        paddingLeft: 12,
        paddingRight: 12,

        justifyContent: "space-between",
        alignItems: "center",
        flexDirection: "row"
    },
    triangleDefault: {
        ...triangle({ type: "bottom", height: 10, size: 10, color: Color.gray }),

        position: "absolute",
        top: 0,
        right: 0,

        marginTop: 5
    },
    triangleActive: {
        ...triangle({ type: "bottom", height: 10, size: 10, color: Color.red }),

        position: "absolute",
        top: 0,
        right: 0,

        marginTop: 5
    },
    triangleContainer: {
        overflow: "hidden",

        width: 20,
        height: 20
    },
    optionsContainer: {
        paddingTop: shadowOffset,
        paddingBottom: shadowOffset,

        paddingLeft: 12,
        paddingRight: 12,
    },
    item: {
        marginTop: 5,
        marginBottom: 5,
        paddingBottom: 2,

        fontFamily: Font.type.regular,
        fontSize: Font.size.m
    }
});
