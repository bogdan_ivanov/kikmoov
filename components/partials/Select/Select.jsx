import * as React from "react";
import { Animated, View, TouchableOpacity, Text } from "react-native";

import { SelectPropTypes } from "./SelectPropTypes";
import { shadowOffset, styles } from "./styles";

export class Select extends React.Component {
    static propTypes = SelectPropTypes;

    constructor(props) {
        super(props);

        this.state = {
            selected: props.options[0].value,
            toggled: false
        };

        this.height = new Animated.Value(0);
        this.opacity = new Animated.Value(0);
    }

    componentDidUpdate(prevProps, prevState) {
        if (!this.isInitialized(prevState) && this.isInitialized(this.state)) {
            Animated.timing(this.height, {
                toValue: this.rootHeight + (shadowOffset * 2),
                duration: 1
            }).start();
        }
    }

    render() {
        return (
            <Animated.View
                style={[
                    styles.root,
                    this.props.style,
                    { opacity: Number(this.isInitialized(this.state)) },
                    this.isInitialized(this.state) && { height: this.height }
                ]}
            >
                <View style={styles.container}>
                    <TouchableOpacity
                        onPress={this.toggle}
                        onLayout={this.handleSelectedLayout}
                        style={[styles.selectedContainer, this.props.selectedContainerStyles]}
                    >
                        {this.selectedLabel}
                        <View style={styles.triangleContainer}>
                            <Animated.View
                                style={[
                                    styles.triangleDefault,
                                    { opacity: this.invertOpacity },
                                    { transform: [{ rotateZ: this.rotate }] }
                                ]}
                            />
                            <Animated.View
                                style={[
                                    styles.triangleActive,
                                    { opacity: this.opacity },
                                    { transform: [{ rotateZ: this.rotate }] }
                                ]}
                            />
                        </View>
                    </TouchableOpacity>
                    <View
                        style={[styles.optionsContainer, this.props.optionsContainerStyles]}
                        onLayout={this.handleOptionsLayout}
                    >
                        {this.mappedLabels}
                    </View>
                </View>
            </Animated.View>
        );
    }

    get rootHeight() {
        return this.state.toggled
            ? this.state.selectedContainerHeight + this.state.optionsContanerHeight
            : this.state.selectedContainerHeight;
    }

    get selectedLabel() {
        const label = this.props.options.find(({ value }) => this.state.selected === value).label;

        if (this.props.renderLabel) {
            return this.props.renderLabel(label, { isActive: true, isHead: true });
        }

        return <Text style={styles.item}>{label}</Text>;
    }

    get mappedLabels() {
        if (this.props.renderLabel) {
            return this.props.options.map(({ label, value }) => (
                <TouchableOpacity onPress={this.handlePress(value)} key={value}>
                    {this.props.renderLabel(label, { isActive: value === this.state.selected, isHead: false })}
                </TouchableOpacity>
            ));
        }

        return this.props.options.map(({ label, value }) => (
            <TouchableOpacity onPress={this.handlePress(value)} key={value}>
                <Text style={styles.item}>{label}</Text>
            </TouchableOpacity>
        ));
    }

    get rotate() {
        return this.opacity.interpolate({
            inputRange: [0, 1],
            outputRange: ["0deg", "180deg"]
        });
    }

    get invertOpacity() {
        return this.opacity.interpolate({
            inputRange: [0, 1],
            outputRange: [1, 0]
        });
    }

    isInitialized = (state) => {
        return !!state.selectedContainerHeight && !!state.optionsContanerHeight;
    }

    handlePress = (selected) => () => {
        this.props.onChange && this.props.onChange(selected);
        this.setState({ selected }, this.toggle);
    }

    toggle = () => {
        this.setState(({ toggled }) => ({
            toggled: !toggled
        }), () => {
            Animated.timing(this.height, {
                toValue: this.rootHeight + (shadowOffset * 2),
                duration: 250
            }).start();
            Animated.timing(this.opacity, {
                toValue: Number(this.state.toggled),
                duration: 250
            }).start();
        });
    }

    handleOptionsLayout = ({ nativeEvent: { layout: { height } } }) => {
        if (this.state.optionsContanerHeight > height) {
            return;
        }

        this.setState({ optionsContanerHeight: height });
    }

    handleSelectedLayout = ({ nativeEvent: { layout: { height } } }) => {
        if (this.state.selectedContainerHeight > height) {
            return;
        }

        this.setState({ selectedContainerHeight: height });
    }
}
