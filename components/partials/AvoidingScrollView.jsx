import * as React from "react";
import { View, KeyboardAvoidingView, Animated, StyleSheet, Dimensions, Platform, Keyboard } from "react-native";

export class AvoidingScrollView extends React.Component {
    static defaultProps = {
        contentContainerStyle: {}
    };

    state = {
        offset: 0
    };

    render() {
        return (
            <View style={[styles.container, this.props.style]} onLayout={this.handleLayout}>
                <KeyboardAvoidingView
                    behavior="padding"
                    style={styles.keyboard}
                    onPress={Keyboard.dismiss}
                    keyboardVerticalOffset={this.state.offset}
                >
                    <Animated.ScrollView
                        style={styles.scroll}
                        scrollEventThrottle={1}
                        keyboardShouldPersistTaps="handled"
                        ref={this.props.scrollRef}
                        {...this.props}
                    >
                        {this.props.children}
                    </Animated.ScrollView>
                </KeyboardAvoidingView>
            </View>
        );
    }

    handleLayout = ({ nativeEvent: { layout: { height } } }) => {
        this.setState({
            offset: Platform.select({
                android: -height,
                ios: Dimensions.get("window").height - height
            })
        });
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff"
    },
    keyboard: {
        flex: 1
    },
    scroll: {
        flex: 1
    }
});
