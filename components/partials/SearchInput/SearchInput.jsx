import Axios from "axios";
import shortId from "shortid";
import * as React from "react";
import { Toaster } from "react-native-toastboard";
import Touchable from "react-native-platform-touchable";
import {
    Image,
    Text,
    View,
    TextInput,
    ScrollView,
    TouchableOpacity,
    ActivityIndicator
} from "react-native";

import { getSuggestion } from "@statelessActions";

import { Images } from "@assets/Images";
import { Color } from "@constants/UI";

import { styles } from "./styles";

export class SearchInput extends React.Component {
    /* eslint-disable no-magic-numbers */
    static sessionToken = Date.now();
    static delay = 500;
    static getDerivedStateFromProps(props, state) {
        if (!(props.value || "").length && state.suggestions.length) {
            return { ...state, suggestions: [] };
        }

        return state;
    }
    /* eslint-enable no-magic-numbers */

    constructor(props) {
        super(props);

        this.state = {
            suggestions: [],
            loadingSuggestions: false,
        };
        this.timer = undefined;
        this.cancelToken = Axios.CancelToken.source();
    }

    componentWillUnmount() {
        this.cancelToken.cancel();

        clearTimeout(this.timer);
    }

    render() {
        const { onChangeText, createRef, value, style, children, ...restProps } = this.props;

        return (
            <View style={[styles.searchSection, style]}>
                <View style={styles.inputContainer}>
                    {this.state.loadingSuggestions
                        ? <ActivityIndicator color={Color.red} style={styles.icon} />
                        : (
                            <TouchableOpacity onPress={this.props.onSubmitEditing}>
                                <Image style={styles.icon} source={Images.search} />
                            </TouchableOpacity>
                        )
                    }
                    <TextInput
                        hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}

                        onChangeText={this.handleTextChange}

                        underlineColorAndroid="transparent"
                        placeholder="enter location"

                        placeholderTextColor={Color.gray}
                        style={styles.input}
                        ref={createRef}
                        value={value}

                        {...restProps}
                    />
                    {this.shouldRenderClearButton && <this.ClearButton />}
                    {children}
                </View>
                {!!this.state.suggestions.length && <this.Suggestions />}
            </View>
        );
    }

    get shouldRenderClearButton() {
        return !!(this.props.value || "").length;
    }

    Suggestions = () => (
        <View style={styles.suggestionsContainer}>
            <ScrollView
                style={styles.suggestionsContainerScroll}
                keyboardShouldPersistTaps="always"
                removeClippedSubviews
            >
                {this.state.suggestions.map(({ value, type }, index) => (
                    <Touchable
                        key={shortId.generate()}
                        onPress={this.handleSuggestionItemPress(value, type)}
                        style={[~index % 2 && styles.suggestionItemDark]}
                    >
                        <View style={styles.suggestionItemWrap}>
                            <Text numberOfLines={1} style={styles.suggestionItem}>{value}</Text>
                            <Text style={styles.suggestionItemType}>{type}</Text>
                        </View>
                    </Touchable>
                ))}
            </ScrollView>
        </View>
    )

    ClearButton = () => (
        <TouchableOpacity
            onPress={this.handleClearText}
            hitSlop={{ left: 10, right: 10, top: 10, bottom: 10 }}
        >
            <Image style={styles.icon} source={Images.clear} />
        </TouchableOpacity>
    );

    handleTextChange = (text) => {
        const type = this.state.suggestions[0] ? this.state.suggestions[0].type : this.props.valueType;

        this.props.onChangeText(text, type);

        clearTimeout(this.timer);
        this.timer = setTimeout(this.processSearchSuggestions, SearchInput.delay);
    }

    handleSuggestionItemPress = (item, type) => () => {
        this.props.onChangeText(item, type);

        this.setState({ suggestions: [] }, this.props.onSubmitEditing);
    }

    handleClearText = () => this.props.onChangeText("", this.props.valueType);

    processSearchSuggestions = async () => {
        if ((this.props.value || "").length < 2) {
            return;
        }

        this.setState({ loadingSuggestions: true });

        let response;
        try {
            response = await getSuggestion(
                { address: this.props.value, sessionToken: SearchInput.sessionToken },
                this.cancelToken.token
            );
        } catch (error) {
            !Axios.isCancel(error) && Toaster.error(error);

            return this.setState({ loadingSuggestions: false });
        }

        this.setState({ loadingSuggestions: false, suggestions: response.data });
    }
}
