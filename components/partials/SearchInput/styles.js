import { StyleSheet } from "react-native";

import { Color, Font, shadowArea } from "@constants/UI";

/* eslint-disable-next-line no-magic-numbers */
const ITEM_HEIGHT = Font.size.m + 20;
const ITEM_COUNT = 5;
const CONTAINER_MARGIN = 15;

export const styles = StyleSheet.create({
	suggestionsContainerScroll: {
		maxHeight: (ITEM_HEIGHT * ITEM_COUNT) - CONTAINER_MARGIN
	},
	suggestionsContainer: {
		borderTopWidth: 1,
		borderTopColor: Color.grayLight,
		borderBottomLeftRadius: 20,
		borderBottomRightRadius: 20,

		paddingTop: 1,

		overflow: "hidden"
	},
	suggestionItem: {
		fontFamily: Font.type.regular,
		fontSize: Font.size.m,

		marginLeft: 34,

		paddingRight: 10,

		width: "70%"
	},
	suggestionItemDark: {
		backgroundColor: Color.darkWhite,

		margin: -1
	},
	suggestionItemType: {
		fontFamily: Font.type.regular,
		fontSize: Font.size.s,

		color: Color.red
	},
	suggestionItemWrap: {
		height: ITEM_HEIGHT,
		paddingRight: 10,

		margin: -1,

		flexDirection: "row",
		alignItems: "center"
	},
	inputContainer: {
		flexDirection: "row",

		paddingLeft: 10,
		paddingRight: 10
	},
	searchSection: {
		...shadowArea,

		shadowRadius: 3,

		marginTop: CONTAINER_MARGIN,
		marginBottom: CONTAINER_MARGIN,

		paddingTop: 8,

		borderRadius: 20
	},
	input: {
		fontFamily: Font.type.medium,
		fontSize: Font.size.m,

		color: Color.grayDark,

		height: 20,

		flex: 1,
		flexGrow: 1,

		paddingRight: 5,

		marginLeft: 10,
		marginBottom: 8
	},
	icon: {
		width: 20,
		height: 20
	}
});
