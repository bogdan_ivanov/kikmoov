import * as React from "react";
import PropTypes from "prop-types";
import { View, TouchableOpacity, Image, TextInput, StyleSheet } from "react-native";

import { Color, Font } from "@constants/UI";
import { Images } from "@assets/Images";

const NumericInputPropTypes = {
	limit: PropTypes.number,
	onChange: PropTypes.func.isRequired,
	value: PropTypes.number.isRequired
};

const NumericInputDefaultProps = {
	limit: 99,
};

export class NumericInput extends React.Component {
	static propTypes = NumericInputPropTypes;
	static defaultProps = NumericInputDefaultProps;

	render() {
		return (
			<View style={styles.root}>
				<TouchableOpacity
					style={[styles.button, !this.canIncrease && { borderColor: Color.gray }]}
					activeOpacity={this.canIncrease ? .2 : 1}
					onPress={this.increase}
				>
					<Image
						style={[styles.image, !this.canIncrease && { tintColor: Color.gray }]}
						source={Images.plus}
						resizeMode="contain"
					/>
				</TouchableOpacity>
				<TextInput
					returnKeyType="done"
					keyboardType="number-pad"
					underlineColorAndroid="transparent"

					style={styles.input}
					onChangeText={this.handleChange}
					value={String(this.props.value).padStart(2, "0")}
				/>
				<TouchableOpacity
					style={[styles.button, !this.canDecrease && { borderColor: Color.gray }]}
					activeOpacity={this.canDecrease ? .2 : 1}
					onPress={this.decrease}
				>
					<Image
						style={[styles.image, !this.canDecrease && { tintColor: Color.gray }]}
						source={Images.minus}
						resizeMode="contain"
					/>
				</TouchableOpacity>
			</View>
		);
	}

	get canIncrease() {
		return this.props.value < this.props.limit;
	}

	get canDecrease() {
		return this.props.value > 0;
	}

	handleChange = (value) => {
		let transformedValue = value;
		if (value < 0) {
			transformedValue = 0;
		} else if (value > this.props.limit) {
			transformedValue = this.props.limit;
		}

		this.props.onChange(Number(transformedValue) || 0);
	}

	increase = () => {
		if (!this.canIncrease) {
			return;
		}

		this.props.onChange(this.props.value + 1);
	}

	decrease = () => {
		if (!this.canDecrease) {
			return;
		}

		this.props.onChange(this.props.value - 1);
	}
}

export const styles = StyleSheet.create({
	root: {
		flexDirection: "row",
		alignItems: "center"
	},
	button: {
		width: 45,
		height: 45,

		borderRadius: 22.5,

		borderWidth: 1,
		borderColor: Color.red,

		alignItems: "center",
		justifyContent: "center",
	},
	image: {
		width: 17
	},
	input: {
		marginLeft: 6,
		marginRight: 6,

		width: 60,
		height: 39,

		borderRadius: 25,

		fontSize: Font.size.m,
		fontFamily: Font.type.medium,

		textAlign: "center",

		backgroundColor: Color.grayLight
	}
});
