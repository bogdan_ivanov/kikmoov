import * as React from "react";
import PropTypes from "prop-types";

const MultiSwitcherPropTypes = {
    items: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
    children: PropTypes.func.isRequired,
    defaultValue: PropTypes.string,
    onChange: PropTypes.func
};

export class MultiSwitcher extends React.Component {
    static propTypes = MultiSwitcherPropTypes;

    constructor(props) {
        super(props);

        this.items = {};

        props.items.forEach((item) => {
            this.items[item] = {
                handler: this.createSwitchHandler(item),
                active: false
            };
        });

        if (props.defaultValue) {
            this.items[props.defaultValue].active = true;
        }
    }

    render() {
        return this.props.children(this.items);
    }

    createSwitchHandler = (currentItem) => () => {
        Object.keys(this.items).forEach((item) => {
            this.items[item].active = false;
        });

        this.items[currentItem].active = true;

        this.props.onChange && this.props.onChange(currentItem);
        this.forceUpdate();
    }
}
