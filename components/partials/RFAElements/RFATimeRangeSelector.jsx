import * as React from "react";
import PropTypes from "prop-types";
import Touchable from "react-native-platform-touchable";
import { FormGroupContext } from "react-formawesome-core";
import { Modal, Text, View, Image, TouchableOpacity, StyleSheet } from "react-native";

import { Images } from "@assets/Images";
import { Color, buttonText, Font, filledButton, modalOverlay } from "@constants/UI";

import { StatusBarManager } from "@utils/StatusBarManager";

import { TimeRangeSelector } from "@components/partials/TimeRangeSelector";

export class RFATimeRangeSelector extends React.Component {
	static propTypes = {
		label: PropTypes.string.isRequired,
		headerTitle: PropTypes.string.isRequired,

		onChange: PropTypes.func
	};

	range = [
		"08:00", "09:00", "10:00", "11:00",
		"12:00", "13:00", "14:00", "15:00",
		"16:00", "17:00", "18:00", "19:00"
	];

	state = {
		modalVisible: false,
	};

	render() {
		return (
			<FormGroupContext.Consumer>
				{this.renderChildren}
			</FormGroupContext.Consumer>
		);
	}

	renderChildren = (formGroupContext) => {
		const { min, max } = JSON.parse(formGroupContext.value || "{}");

		const { label, headerTitle, ...selectorProps } = this.props;

		return (
			<React.Fragment>
				<TouchableOpacity style={styles.openButton} onPress={this.handleOpenModal}>
					<Text style={styles.titleText}>{this.props.label}</Text>
					<View style={styles.arrowContainer}>
						<Text style={styles.titleText}>
							{min}
							{!!max && ` - ${max}`}
						</Text>
						<Image resizeMode="stretch" style={styles.arrow} source={Images.backButton} />
					</View>
				</TouchableOpacity>
				<Modal
					onRequestClose={this.handleCloseModal}
					visible={this.state.modalVisible}
					animationType="fade"
					transparent
				>
					<View style={styles.modalRoot}>
						<View style={styles.modalContainer}>
							<View style={styles.headerContainer}>
								<Text style={styles.headerText}>{this.props.headerTitle}</Text>
							</View>
							<TimeRangeSelector
								{...selectorProps}
								onChange={this.handleChange(formGroupContext)}
								defaults={{ min, max }}
								range={this.range}
							/>
							<Touchable style={styles.closeButton} onPress={this.handleCloseModal}>
								<Text style={styles.closeButtonText}>Done</Text>
							</Touchable>
						</View>
					</View>
				</Modal>
			</React.Fragment>
		);
	}

	handleCloseModal = () => {
		StatusBarManager.restoreState();
		this.setState({ modalVisible: false });
	}

	handleOpenModal = () => {
		StatusBarManager.setState("hidden");
		this.setState({ modalVisible: true });
	}

	handleChange = (formGroupContext) => ({ min, max }) => {
		this.props.onChange && this.props.onChange({ min, max });

		formGroupContext.onChange(JSON.stringify({ min, max }));
	}
}

const styles = StyleSheet.create({
	modalRoot: {
		...modalOverlay
	},
	modalContainer: {
		borderRadius: 5,
		overflow: "hidden",

		backgroundColor: "#fff"
	},
	arrow: {
		width: 4,
		height: 7,

		tintColor: "#000",

		transform: [{ rotateZ: "180deg" }],

		marginLeft: 4
	},
	openButton: {
		flexDirection: "row",
		justifyContent: "space-between"
	},
	arrowContainer: {
		flexDirection: "row",
		alignItems: "center"
	},
	titleText: {
		fontFamily: Font.type.regular,
		fontSize: Font.size.m
	},
	closeButton: {
		...filledButton,

		padding: 12,

		backgroundColor: "#fff",

		borderTopLeftRadius: 0,
		borderTopRightRadius: 0,

		borderWidth: 0,
		borderTopWidth: 1,
		borderColor: Color.grayLight,

		marginTop: 10
	},
	closeButtonText: {
		...buttonText,

		color: Color.red
	},
	headerContainer: {
		backgroundColor: Color.grayLight,

		alignItems: "center",

		paddingTop: 15,
		paddingBottom: 15,

		marginBottom: 15
	},
	headerText: {
		color: Color.gray,

		fontFamily: Font.type.regular,
		fontSize: Font.size.m
	}
});
