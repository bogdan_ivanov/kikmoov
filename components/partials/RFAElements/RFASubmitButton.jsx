import * as React from "react";
import Touchable from "react-native-platform-touchable";
import { SubmitButton, SubmitButtonPropTypes } from "react-native-formawesome";

/* eslint-disable */
export class RFASubmitButton extends SubmitButton {
	static propTypes = SubmitButtonPropTypes;

	renderChildren = (context) => {
		const { loadingComponent, onPress, ...restProps } = this.props;

		return (
			<Touchable {...restProps} onPress={this.getHandleSubmit(context)}>
				{loadingComponent && context.loading ? loadingComponent : this.props.children}
			</Touchable>
		);
	}
}
