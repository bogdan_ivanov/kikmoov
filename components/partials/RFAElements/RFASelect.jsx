import * as React from "react";
import PropTypes from "prop-types";
import { View, Text, StyleSheet } from "react-native";
import { FormGroupContext } from "react-formawesome-core";

import { Select } from "@components/partials/Select";
import { Font, Color } from "@constants/UI";

const RFASelectPropTypes = {
	options: PropTypes.arrayOf(PropTypes.shape({
		label: PropTypes.any.isRequired,
		value: PropTypes.any.isRequired
	}).isRequired).isRequired,

	selectedContainerStyles: PropTypes.any,
	optionsContainerStyles: PropTypes.any,
	style: PropTypes.any
};

export class RFASelect extends React.Component {
	static propTypes = RFASelectPropTypes;

	render() {
		return (
			<FormGroupContext.Consumer>
				{this.renderChildren}
			</FormGroupContext.Consumer>
		);
	}

	renderChildren = (formGroupContext) => {
		return (
			<Select
				options={this.props.options}
				renderLabel={this.renderLabel}
				onChange={this.handleChange(formGroupContext)}

				style={this.props.style}
				optionsContainerStyles={this.props.optionsContainerStyles}
				selectedContainerStyles={this.props.selectedContainerStyles}
			/>
		);
	}

	renderLabel = (label, { isActive, isHead }) => (
		<View key={label} style={[styles.container, isHead && styles.containerHead]}>
			{!isHead && <View style={[styles.circle, isActive && styles.circleActive]} />}
			<View style={styles.itemWrap}>
				<Text style={[styles.label, isActive && styles.labelActive, isHead && styles.headLabel]}>
					{label}
				</Text>
			</View>
		</View>
	)

	handleChange = (formGroupContext) => (value) => {
		formGroupContext.onChange(value);
	}
}

const styles = StyleSheet.create({
	containerHead: {
	},
	container: {
		flexDirection: "row",
		alignItems: "center",
	},
	circle: {
		width: 17,
		height: 17,

		marginRight: 7,

		borderRadius: 8.5,

		borderWidth: 2.5,
		borderColor: Color.grayLight
	},
	circleActive: {
		borderWidth: 5,
		borderColor: Color.red
	},
	itemWrap: {
		paddingTop: 7,
		paddingBottom: 7
	},
	label: {
		fontSize: Font.size.m,
		fontFamily: Font.type.regular,

		color: Color.gray
	},
	labelActive: {
		fontFamily: Font.type.medium,

		color: "#000"
	},
	headLabel: {
		fontFamily: Font.type.regular
	}
});
