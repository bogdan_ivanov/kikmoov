import * as React from "react";
import PropTypes from "prop-types";
import { FormGroupContext } from "react-formawesome-core";
import { View, Text, TouchableOpacity, StyleSheet } from "react-native";

import { MultiSwitcher } from "@components/partials/MultiSwitcher";
import { Color, Font } from "@constants/UI";

const RFAMultiSwitcherPropTypes = {
	items: PropTypes.arrayOf(PropTypes.string).isRequired,
	labels: PropTypes.object.isRequired,
	onChange: PropTypes.func
};

export class RFAMultiSwitcher extends React.Component {
	static propTypes = RFAMultiSwitcherPropTypes;

	render() {
		return (
			<FormGroupContext.Consumer>
				{this.renderChildren}
			</FormGroupContext.Consumer>
		);
	}

	renderChildren = (formGroupContext) => {
		return (
			<MultiSwitcher
				onChange={this.handleChange(formGroupContext)}
				defaultValue={formGroupContext.value}
				items={this.props.items}
			>
				{(items) => (
					<View style={styles.container}>
						{Object.keys(items).map((item) => (
							<TouchableOpacity style={styles.item} key={item} onPress={items[item].handler}>
								<View style={[styles.circleDefault, items[item].active && styles.circleActive]} />
								<Text style={[styles.itemText, items[item].active && styles.itemTextActive]}>
									{this.props.labels[item]}
								</Text>
							</TouchableOpacity>
						))}
					</View>
				)}
			</MultiSwitcher>
		);
	}

	handleChange = (formGroupContext) => (value) => {
		formGroupContext.onChange(value);
		this.props.onChange && this.props.onChange(value);
	}
}

const styles = StyleSheet.create({
	circleDefault: {
		width: 17,
		height: 17,

		borderRadius: 8.5,
		borderWidth: 5,
		borderColor: Color.grayLight,

		backgroundColor: Color.grayLight,

		marginRight: 5
	},
	circleActive: {
		borderColor: Color.red,

		backgroundColor: "#fff"
	},
	item: {
		flexDirection: "row",
		alignItems: "center",

		marginLeft: 15,
		marginRight: 15
	},
	itemText: {
		fontFamily: Font.type.regular,
		fontSize: Font.size.s,

		color: Color.gray
	},
	itemTextActive: {
		fontFamily: Font.type.medium,
		fontSize: Font.size.s,

		color: "#000"
	},
	container: {
		flexDirection: "row",
		justifyContent: "space-between",
	}
});
