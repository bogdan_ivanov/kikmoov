import * as React from "react";
import PropTypes from "prop-types";
import Touchable from "react-native-platform-touchable";
import { FormGroupContext } from "react-formawesome-core";
import { TouchableOpacity, Text, Modal, View, Image, StyleSheet } from "react-native";

import { DateManager } from "@utils/DateManager";
import { StatusBarManager } from "@utils/StatusBarManager";

import { Images } from "@assets/Images";
import { Color, filledButton, buttonText, Font, modalOverlay } from "@constants/UI";

import { DateSelector as Calendar } from "@components/partials/DateSelector";

export class RFADateSelector extends React.PureComponent {
	static propTypes = {
		label: PropTypes.string,
		onChange: PropTypes.func
	};

	state = {
		modalVisible: false
	};

	render() {
		return (
			<FormGroupContext.Consumer>
				{this.renderChildren}
			</FormGroupContext.Consumer>
		);
	}

	renderChildren = (formGroupContext) => {
		return (
			<React.Fragment>
				<TouchableOpacity style={styles.openButton} onPress={this.handleOpenModal}>
					<Text style={styles.titleText}>{this.props.label}</Text>
					<View style={styles.arrowContainer}>
						<Text style={styles.titleText}>
							{DateManager.parsedDateFromTimestamp(formGroupContext.value)}
						</Text>
						<Image resizeMode="stretch" style={styles.arrow} source={Images.backButton} />
					</View>
				</TouchableOpacity>
				<Modal
					onRequestClose={this.handleCloseModal}
					visible={this.state.modalVisible}
					animationType="fade"
					transparent
				>
					<View style={styles.modalRoot}>
						<View style={styles.modalContainer}>
							<Calendar
								value={formGroupContext.value}
								onChange={this.handleChange(formGroupContext)}
							/>
							<Touchable style={styles.cancelButton} onPress={this.handleCloseModal}>
								<Text style={styles.cancelButtonText}>Done</Text>
							</Touchable>
						</View>
					</View>
				</Modal>
			</React.Fragment>
		);
	}

	handleCloseModal = () => {
		StatusBarManager.restoreState();
		this.setState({ modalVisible: false });
	}

	handleOpenModal = () => {
		StatusBarManager.setState("hidden");
		this.setState({ modalVisible: true });
	}

	handleChange = (formGroupContext) => ({ timestamp }) => {
		this.props.onChange && this.props.onChange(timestamp);
		formGroupContext.onChange(timestamp);
	}
}

const styles = StyleSheet.create({
	modalRoot: {
		...modalOverlay
	},
	modalContainer: {
		borderRadius: 5,
		overflow: "hidden",

		backgroundColor: "#fff"
	},
	cancelButton: {
		...filledButton,

		padding: 12,

		backgroundColor: "#fff",

		borderTopLeftRadius: 0,
		borderTopRightRadius: 0,

		borderWidth: 0,
		borderTopWidth: 1,
		borderColor: Color.grayLight
	},
	cancelButtonText: {
		...buttonText,

		color: Color.red
	},
	arrow: {
		width: 4,
		height: 7,

		tintColor: "#000",

		transform: [{ rotateZ: "180deg" }],

		marginLeft: 4
	},
	openButton: {
		flexDirection: "row",
		justifyContent: "space-between"
	},
	arrowContainer: {
		flexDirection: "row",
		alignItems: "center"
	},
	titleText: {
		fontFamily: Font.type.regular,
		fontSize: Font.size.m
	}
});
