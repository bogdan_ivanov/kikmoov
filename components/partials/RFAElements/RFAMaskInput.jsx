import * as React from "react";
import { TextInputMask } from "react-native-masked-text";
import { Input, InputPropTypes } from "react-native-formawesome";

/* eslint-disable */
export class RFAMaskInput extends Input {
	static propTypes = InputPropTypes;

	renderChildren = (context) => {
		const { onChangeText, onBlur, onFocus, value, style, ...inputProps } = this.props;

		return (
			<TextInputMask
				{...inputProps}
				ref={this.handleRegisterElementControlWrapped(context)}
				onChangeText={this.getHandleChange(context)}
				onFocus={this.getHandleFocus(context)}
				onBlur={this.getHandleBlur(context)}
				style={this.getStyle(context)}
				value={context.value}
			/>
		);
	}

	handleRegisterElementControlWrapped = (context) => (maskElement) => {
		this.handleRegisterElementControl(context)(maskElement && maskElement.getElement());
	}

}
