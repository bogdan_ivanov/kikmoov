import shortId from "shortid";
import * as React from "react";
import PropTypes from "prop-types";
import { Animated, View, TouchableOpacity, Text, FlatList, StyleSheet, Dimensions } from "react-native";

import { Color, Font, containerStatic } from "@constants/UI";

const width = Dimensions.get("window").width - (containerStatic.paddingLeft + containerStatic.paddingRight);

export class ExpandableFlatList extends React.Component {
    static propTypes = {
        items: PropTypes.array.isRequired,
        children: PropTypes.func.isRequired,

        spaceBetweenItems: PropTypes.number,
        itemsPerRow: PropTypes.number
    };

    static defaultProps = {
        spaceBetweenItems: 10,
        itemsPerRow: 6
    }

    constructor(props) {
        super(props);

        this.state = {
            toggled: false
        };

        this.itemHeight = (width - (props.spaceBetweenItems * props.itemsPerRow)) / props.itemsPerRow;
        this.height = new Animated.Value(this.getExpandedHeight(1));
    }

    render() {
        return (
            <React.Fragment>
                <Animated.View style={[this.styles.container, { height: this.height }]}>
                    <FlatList
                        scrollEnabled={false}
                        data={this.props.items}
                        renderItem={this.renderItem}
                        keyExtractor={this.keyExtractor}
                        numColumns={this.props.itemsPerRow}
                        contentContainerStyle={this.styles.listContainer}
                    />
                </Animated.View>
                {this.shouldRenderViewButton && (
                    <TouchableOpacity onPress={this.handlePress}>
                        <Text style={this.styles.buttonText}>
                            {this.state.toggled ? "roll up <" : "view all >"}
                        </Text>
                    </TouchableOpacity>
                )}
            </React.Fragment>
        );
    }

    get shouldRenderViewButton() {
        return this.props.items.length > this.props.itemsPerRow;
    }

    get styles() {
        return styles({
            itemHeight: this.itemHeight,
            spaceBetweenItems: this.props.spaceBetweenItems
        });
    }

    getExpandedHeight = (length) => {
        let rowCount = length / this.props.itemsPerRow;

        if (Math.round(rowCount) < rowCount) {
            rowCount = Math.round(rowCount) + 1;
        } else {
            rowCount = Math.round(rowCount);
        }

        return (rowCount * this.itemHeight) + (rowCount * this.props.spaceBetweenItems);
    }

    renderItem = (item) => {
        return (
            <View key={shortId.generate()} style={this.styles.item}>
                {this.props.children(item)}
            </View>
        );
    }

    keyExtractor = (item, i) => i

    handlePress = () => {
        this.setState(({ toggled }) => ({
            toggled: !toggled
        }), () => {
            Animated.timing(this.height, {
                toValue: this.state.toggled
                    ? this.getExpandedHeight(this.props.items.length)
                    : this.getExpandedHeight(1),
                duration: 250
            }).start();
        });
    }
}

const styles = ({ itemHeight, spaceBetweenItems }) => StyleSheet.create({
    item: {
        width: itemHeight,
        height: itemHeight,

        margin: spaceBetweenItems / 2
    },
    container: {
        flexDirection: "row",

        width: "100%",

        overflow: "hidden"
    },
    buttonText: {
        color: Color.red,

        fontSize: Font.size.s,
        fontFamily: Font.type.regular,

        textAlign: "center"
    },
    listContainer: {
        width
    }
});
