import * as React from "react";
import PropTypes from "prop-types";

const IdenterminateCheckboxPropTypes = {
	defaultValue: PropTypes.string,
};

export class IdenterminateCheckbox extends React.Component {
	static propTypes = IdenterminateCheckboxPropTypes;

	constructor(props) {
		super(props);

		this.state = {
			selected: props.defaultValue
		};
	}

	render() {
		return this.props.children(this.state.selected, this.onStateChange);
	}

	onStateChange = (selected) => () => {
		if (this.state.selected === selected) {
			this.setState({ selected: undefined });
		} else {
			this.setState({ selected });
		}
	}
}
