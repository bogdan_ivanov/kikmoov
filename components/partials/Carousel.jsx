import shortid from "shortid";
import * as React from "react";
import PropTypes from "prop-types";
import { ScrollView, View, StyleSheet, ViewPagerAndroid, Platform } from "react-native";

const CarouselPropTypes = {
    renderItem: PropTypes.func.isRequired,
    itemsList: PropTypes.array.isRequired,
    children: PropTypes.func.isRequired,
};

export class Carousel extends React.PureComponent {
    static propTypes = CarouselPropTypes;

    state = {
        currentSlide: 0,
        width: undefined
    };

    render() {
        if (Platform.OS === "android") {
            return (
                <React.Fragment>
                    <ViewPagerAndroid
                        removeClippedSubviews
                        style={this.props.style}
                        onLayout={this.handleLayout}
                        initialPage={this.state.currentSlide}
                        onPageSelected={this.handlePageSelected}
                    >
                        {this.props.itemsList.map((item, index) => (
                            /* eslint-disable-next-line react/no-array-index-key */
                            <View key={index}>
                                {this.props.renderItem(item, index)}
                            </View>
                        ))}
                    </ViewPagerAndroid>
                    {this.props.children(this.Dots)}
                </React.Fragment>
            );
        }

        return (
            <React.Fragment>
                <ScrollView
                    contentContainerStyle={{ width: `${this.props.itemsList.length}00%` }}
                    onMomentumScrollEnd={this.handleMomentumScrollEnd}
                    showsHorizontalScrollIndicator={false}
                    onLayout={this.handleLayout}
                    style={this.props.style}
                    overScrollMode="never"
                    removeClippedSubviews
                    bounces={false}
                    pagingEnabled
                    horizontal
                >
                    {this.props.itemsList.map(this.props.renderItem)}
                </ScrollView>
                {this.props.children(this.Dots)}
            </React.Fragment>
        );
    }

    handleLayout = ({ nativeEvent: { layout: { width } } }) => {
        if (this.state.width) {
            return;
        }

        this.setState({
            width
        });
    }

    Dots = () => {
        return this.props.itemsList.map((x, i) => (
            <View
                key={shortid.generate()}
                style={[styles.dot, this.state.currentSlide === i && styles.dotActive]}
            />
        ));
    }

    handlePageSelected = (event) => {
        this.setState({ currentSlide: event.nativeEvent.position });
    }

    handleMomentumScrollEnd = (event) => {
        const currentSlide = Math.round(event.nativeEvent.contentOffset.x / this.state.width);

        if (currentSlide !== this.state.currentSlide) {
            this.setState({ currentSlide });
        }
    }

}

const styles = StyleSheet.create({
    dot: {
        width: 10,
        height: 10,
        marginLeft: 5,
        marginRight: 5,
        borderRadius: 5,
        backgroundColor: "rgba(255, 255, 255, 0.2)"
    },
    dotActive: {
        backgroundColor: "#fff"
    }
});
