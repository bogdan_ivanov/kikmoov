import * as React from "react";
import { withNavigation } from "react-navigation";
import hoistNonReactStatics from "hoist-non-react-statics";

import { StatusBarManager } from "@utils/StatusBarManager";

import { BookingViewControlsContext } from "./BookingViewControlsContext";
import { NavigationPropTypes } from "../../../../../navigation/NavigationPropTypes";

@withNavigation
export class BookingViewControlsProvider extends React.Component {
    static propTypes = NavigationPropTypes;

    state = {
        invateAttendeeModalToggled: false,
        addToCalendarModalToggled: false,
        cancelModalToggled: false
    };

    constructor(props) {
        super(props);

        props.navigation.setParams({
            toggleCancelModal: this.toggleCancelModal
        });
    }

    componentWillUnmount() {
        StatusBarManager.restoreState();

        if (this.props.navigation.state.params) {
            delete this.props.navigation.state.params.toggleCancelModal;
        }
    }

    render() {
        return (
            <BookingViewControlsContext.Provider value={this.childContex}>
                {this.props.children}
            </BookingViewControlsContext.Provider>
        );
    }

    get childContex() {
        return {
            toggleInvateAttendeeModal: this.toggleInvateAttendeeModal,
            toggleAddToCalendarModal: this.toggleAddToCalendarModal,
            toggleCancelModal: this.toggleCancelModal,

            ...this.state
        };
    }

    toggleInvateAttendeeModal = () => {
        this.setState(({ invateAttendeeModalToggled }) => ({
            invateAttendeeModalToggled: !invateAttendeeModalToggled
        }));
    }

    toggleAddToCalendarModal = () => {
        this.setState(({ addToCalendarModalToggled }) => ({
            addToCalendarModalToggled: !addToCalendarModalToggled
        }));
    }

    toggleCancelModal = () => {
        this.setState(({ cancelModalToggled }) => ({
            cancelModalToggled: !cancelModalToggled
        }), () => {
            if (this.state.cancelModalToggled) {
                StatusBarManager.setState("hidden");
            } else {
                StatusBarManager.restoreState();
            }
        });
    }
}

export function withBookingViewControlsProvider(WrappedComponent) {
    function Enhanced(props) {
        return (
            <BookingViewControlsProvider>
                <WrappedComponent {...props} />
            </BookingViewControlsProvider>
        );
    }

    return hoistNonReactStatics(Enhanced, WrappedComponent);
}
