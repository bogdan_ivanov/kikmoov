import * as React from "react";
import PropTypes from "prop-types";
import hoistNonReactStatics from "hoist-non-react-statics";

export const BookingViewControlsContext = React.createContext({
    toggleInvateAttendeeModal: () => undefined,
    toggleAddToCalendarModal: () => undefined,
    toggleCancelModal: () => undefined,

    invateAttendeeModalToggled: false,
    addToCalendarModalToggled: false,
    cancelModalToggled: false
});

export const BookingViewControlsContextPropTypes = {
    toggleInvateAttendeeModal: PropTypes.func.isRequired,
    toggleAddToCalendarModal: PropTypes.func.isRequired,
    toggleCancelModal: PropTypes.func.isRequired,

    invateAttendeeModalToggled: PropTypes.bool.isRequired,
    addToCalendarModalToggled: PropTypes.bool.isRequired,
    cancelModalToggled: PropTypes.bool.isRequired
};

export function withBookingViewControlsContext(WrappedComponent) {
    class Enhanced extends React.Component {
        render() {
            return (
                <BookingViewControlsContext.Consumer>
                    {this.renderChildren}
                </BookingViewControlsContext.Consumer>
            );
        }

        renderChildren = (context) => {
            return <WrappedComponent {...this.props} bookingViewControlsContext={context} />;
        }
    }

    return hoistNonReactStatics(Enhanced, WrappedComponent);
}
