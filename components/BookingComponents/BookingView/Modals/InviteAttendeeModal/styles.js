import { StyleSheet } from "react-native";

import { Color, Font, buttonGroupContainer, buttonText, filledButton, modalOverlay } from "@constants/UI";

export const styles = StyleSheet.create({
    modal: {
        ...modalOverlay
    },
    container: {
        borderRadius: 5,

        backgroundColor: "#fff"
    },
    titleText: {
        color: Color.red,

        fontSize: Font.size.l,
        fontFamily: Font.type.medium,

        textAlign: "center",

        margin: 20,
    },
    footer: {
        ...buttonGroupContainer
    },
    separator: {
        width: 1,

        height: "100%",

        backgroundColor: Color.grayLight
    },
    input: {
        height: 25,
        paddingBottom: 7,

        marginTop: 25,
        marginBottom: 45,
        marginLeft: 30,
        marginRight: 30,

        borderBottomWidth: 1,
        borderColor: Color.grayLight,
        fontFamily: Font.type.medium,
        fontSize: Font.size.l,

        color: "#000"
    },
    button: {
        ...filledButton,

        flex: 1,

        backgroundColor: "#fff"
    },
    cancelButtonText: {
        ...buttonText,

        color: Color.gray
    },
    addButtonText: {
        ...buttonText,

        color: Color.red
    }
});
