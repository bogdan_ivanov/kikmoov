import * as React from "react";
import PropTypes from "prop-types";
import Touchable from "react-native-platform-touchable";
import {
    Text,
    View,
    Modal,
    Platform,
    TextInput,
    ActivityIndicator,
    KeyboardAvoidingView
} from "react-native";

import { Color } from "@constants/UI";

import {
    withBookingViewControlsContext,
    BookingViewControlsContextPropTypes
} from "../../Providers";

import { styles } from "./styles";

@withBookingViewControlsContext
export class InviteAttendeeModal extends React.Component {
    static propTypes = {
        bookingViewControlsContext: PropTypes.shape(BookingViewControlsContextPropTypes).isRequired,
    };

    state = {
        loading: false,
        email: ""
    };

    render() {
        return (
            <Modal
                visible={this.props.bookingViewControlsContext.invateAttendeeModalToggled}
                animationType="fade"
                transparent
            >
                <KeyboardAvoidingView
                    style={styles.modal}
                    behavior={Platform.select({ ios: "padding", android: "height" })}
                >
                    <View style={styles.container}>
                        <Text style={styles.titleText}>Invite attendee</Text>
                        <TextInput
                            onChangeText={this.handleTextChange}
                            value={this.state.email}

                            underlineColorAndroid="transparent"
                            style={styles.input}
                            placeholder="email"
                        />
                        <View style={styles.footer}>
                            <Touchable
                                style={styles.button}
                                onPress={this.props.bookingViewControlsContext.toggleInvateAttendeeModal}
                            >
                                <Text style={styles.cancelButtonText}>Cancel</Text>
                            </Touchable>
                            <View style={styles.separator} />
                            <Touchable style={styles.button} onPress={this.handleAdd}>
                                {this.AddButton}
                            </Touchable>
                        </View>
                    </View>
                </KeyboardAvoidingView>
            </Modal>
        );
    }

    get AddButton() {
        return this.state.loading
            ? <ActivityIndicator size="small" color={Color.red} />
            : <Text style={styles.addButtonText}>Add</Text>;
    }

    handleTextChange = (email) => {
        this.setState({ email });
    }

    handleAdd = async () => {
        this.setState({ loading: true });

        // await this.props.bookingViewContext.onInviteAttendee(this.state.email);

        this.setState({ loading: false });
        this.props.bookingViewControlsContext.toggleInvateAttendeeModal();
    }
}
