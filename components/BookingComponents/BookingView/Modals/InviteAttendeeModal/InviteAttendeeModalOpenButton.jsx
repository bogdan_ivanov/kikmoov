import * as React from "react";
import PropTypes from "prop-types";
import { StyleSheet, View } from "react-native";
import Touchable from "react-native-platform-touchable";

import { filledButton, Color, shadowArea } from "@constants/UI";

import {
    withBookingViewControlsContext,
    BookingViewControlsContextPropTypes
} from "../../Providers/BookingViewControlsProvider";

@withBookingViewControlsContext
export class InviteAttendeeModalOpenButton extends React.Component {
    static propTypes = {
        bookingViewControlsContext: PropTypes.shape(BookingViewControlsContextPropTypes).isRequired
    };

    render() {
        return (
            <Touchable
                onPress={this.props.bookingViewControlsContext.toggleInvateAttendeeModal}
                style={styles.shadowContainer}
            >
                <View style={styles.button}>
                    {this.props.children}
                </View>
            </Touchable>
        );
    }
}

const styles = StyleSheet.create({
    shadowContainer: {
        padding: 4
    },
    button: {
        ...filledButton,
        ...shadowArea,

        borderRadius: 20,
        borderWidth: 1,
        borderColor: Color.grayLight,

        backgroundColor: "#fff"
    }
});
