import * as React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { withNavigation } from "react-navigation";
import Touchable from "react-native-platform-touchable";
import { View, Text, Modal, Image, ActivityIndicator } from "react-native";

import { Color } from "@constants/UI";
import { Images } from "@assets/Images";
import { cancelBooking, cancelViewing } from "@store/actions";

import { bindCancelToken } from "@store/utils/bindCancelToken";

import {
    withBookingViewControlsContext,
    BookingViewControlsContextPropTypes
} from "../../Providers";
import { NavigationPropTypes } from "../../../../../navigation/NavigationPropTypes";
import { styles } from "./styles";

const mapDispatchToProps = bindCancelToken({
    cancelBooking,
    cancelViewing
});

@withNavigation
@withBookingViewControlsContext
@connect(undefined, mapDispatchToProps)
export class CancelBookingModal extends React.Component {
    static propTypes = {
        bookingViewControlsContext: PropTypes.shape(BookingViewControlsContextPropTypes).isRequired,
        type: PropTypes.oneOf(["viewing", "booking"]).isRequired,
        id: PropTypes.number.isRequired,

        cancelBooking: PropTypes.func.isRequired,
        cancelViewing: PropTypes.func.isRequired,

        ...NavigationPropTypes
    };

    state = {
        loading: false
    };

    componentWillUnmount() {
        this.props.cancelBooking.cancel();
        this.props.cancelViewing.cancel();
    }

    render() {
        return (
            <Modal
                transparent
                animationType="fade"
                visible={this.props.bookingViewControlsContext.cancelModalToggled}
                onRequestClose={this.props.bookingViewControlsContext.toggleCancelModal}
            >
                <View style={styles.modal}>
                    <View style={styles.container}>
                        <Text style={styles.titleText}>
                            Are you sure you want to cancel this booking?
                        </Text>
                        <Text style={styles.infoText}>
                            Full refund if you cancel 24 hours before the reservation start time.
                        </Text>
                        <View>
                            {this.state.loading
                                ? (
                                    <ActivityIndicator
                                        style={{ marginBottom: 25 }}
                                        color={Color.redError}
                                        size="small"
                                    />
                                )
                                : (
                                    <React.Fragment>
                                        <Touchable style={styles.button} onPress={this.handleCancel}>
                                            <React.Fragment>
                                                <Image style={styles.image} source={Images.cancelBooking} />
                                                <Text style={styles.cancelButtonText}>Cancel booking</Text>
                                            </React.Fragment>
                                        </Touchable>
                                        <Touchable onPress={this.handleNavigate} style={styles.button}>
                                            <Text style={styles.viewButtonText}>View policy...</Text>
                                        </Touchable>
                                        <Touchable
                                            style={styles.button}
                                            onPress={this.props.bookingViewControlsContext.toggleCancelModal}
                                        >
                                            <Text style={styles.backButtonText}>Back</Text>
                                        </Touchable>
                                    </React.Fragment>
                                )}
                        </View>
                    </View>
                </View>
            </Modal>
        );
    }

    handleNavigate = () => {
        this.props.bookingViewControlsContext.toggleCancelModal();

        this.props.navigation.navigate("TermsAndConditions", { textType: "privacy" });
    }

    handleCancel = async () => {
        this.setState({ loading: true });

        const response = this.props.type === "viewing"
            ? await this.props.cancelViewing(this.props.id)
            : await this.props.cancelBooking(this.props.id);

        if (
            this.props.type === "viewing" && this.props.cancelViewing.canceled
            || this.props.type === "booking" && this.props.cancelBooking.canceled
        ) {
            return;
        }

        this.setState({ loading: false });
        this.props.bookingViewControlsContext.toggleCancelModal();

        if (!response || !response.error) {
            this.props.navigation.navigate("Bookings");
        }
    }
}
