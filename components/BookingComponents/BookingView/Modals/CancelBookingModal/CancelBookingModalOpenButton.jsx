import * as React from "react";
import PropTypes from "prop-types";
import { withNavigation } from "react-navigation";
import Touchable from "react-native-platform-touchable";

import { NavigationPropTypes } from "../../../../../navigation/NavigationPropTypes";

@withNavigation
export class CancelBookingModalOpenButton extends React.Component {
    static propTypes = {
        canCancel: PropTypes.bool.isRequired,

        ...NavigationPropTypes,
    };

    render() {
        if (!this.props.canCancel) {
            return null;
        }

        return (
            <Touchable style={this.props.style} onPress={this.handlePress}>
                {this.props.children}
            </Touchable>
        );
    }

    handlePress = () => this.props.navigation.state.params.toggleCancelModal();
}
