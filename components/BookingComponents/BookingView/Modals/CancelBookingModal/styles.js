import { StyleSheet } from "react-native";

import { Color, Font, filledButton, buttonText, modalOverlay } from "@constants/UI";

export const styles = StyleSheet.create({
    modal: {
        ...modalOverlay
    },
    container: {
        borderRadius: 5,

        backgroundColor: "#fff"
    },
    titleText: {
        color: Color.red,

        fontSize: Font.size.l,
        fontFamily: Font.type.medium,

        textAlign: "center",

        marginLeft: 20,
        marginRight: 20,
        marginTop: 35
    },
    infoText: {
        fontSize: Font.size.m,
        fontFamily: Font.type.medium,

        textAlign: "center",

        margin: 20,
    },
    button: {
        ...filledButton,

        flexDirection: "row",
        justifyContent: "center",

        padding: 15,

        borderRadius: 0,

        borderTopWidth: 1,
        borderTopColor: Color.grayLight
    },
    cancelButtonText: {
        ...buttonText,

        color: Color.redError
    },
    viewButtonText: {
        ...buttonText,

        color: Color.red
    },
    backButtonText: {
        ...buttonText,

        color: Color.gray
    },
    image: {
        width: 20,
        height: 20,

        marginRight: 7,
        marginLeft: -20
    }
});
