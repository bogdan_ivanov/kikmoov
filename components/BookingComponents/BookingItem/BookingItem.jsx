import * as React from "react";
import { withNavigation } from "react-navigation";
import Touchable from "react-native-platform-touchable";
import { View, Image, Text, TouchableOpacity } from "react-native";

import { Color } from "@constants/UI";
import { WorkspaceTypesLabels } from "@constants/API";

import { DateManager } from "@utils/DateManager";
import { NumberManager } from "@utils/NumberManager";

import { BookingItemPropTypes } from "./BookingItemPropTypes";
import { styles } from "./styles";

const imageOpacity = .8;

@withNavigation
export class BookingItem extends React.Component {
    static propTypes = BookingItemPropTypes;

    render() {
        return (
            <View style={styles.root}>
                <View style={styles.container}>
                    <View style={styles.shadow}>
                        <TouchableOpacity onPress={this.handleNavigateIndepth} style={styles.contentContainer}>
                            <Text style={[styles.dateText, this.getDisabledStyles()]}>
                                {this.dates}
                            </Text>
                            <Image
                                source={{ uri: this.props.coverImageUrl }}
                                style={[styles.image, this.getDisabledStyles("", imageOpacity)]}
                            />
                            <View style={styles.typeContainer}>
                                <Text style={[styles.typeText, this.getDisabledStyles()]}>
                                    {this.typeText}
                                </Text>
                            </View>
                            <Text style={[styles.nameText, this.getDisabledStyles()]}>
                                {this.props.locationName}
                            </Text>
                            <Text style={[styles.locationText, this.getDisabledStyles()]}>
                                {this.props.locationAddress}
                            </Text>
                        </TouchableOpacity>
                        {!this.props.isPast && (
                            <Touchable style={styles.button} onPress={this.handleNavigateDetails}>
                                <Text style={styles.buttonText}>VIEW BOOKING DETAILS</Text>
                            </Touchable>
                        )}
                    </View>
                </View>
                <View style={styles.triangleShadow} />
                <View style={styles.triangleBackground} />
                <View style={styles.dotContainer}>
                    <View style={[styles.dot, this.getDisabledStyles("backgroundColor")]} />
                </View>
            </View>
        );
    }

    get typeText() {
        const size = this.props.size ? `${NumberManager.abbreviate(this.props.size)} sq ft` : "";
        const capacity = this.props.quantity || this.props.capacity;

        const count = capacity ? `${NumberManager.abbreviate(capacity)} ppl` : "";

        return [
            WorkspaceTypesLabels[this.props.type],
            (size || count) && " - ",
            size && count ? `${size} | ${count}` : (size || count)
        ].join("");
    }

    get dates() {
        return DateManager.parsedDateTimeFromTimestamp(
            DateManager.fromUNIX(this.props.startTime),
            this.props.endTime ? DateManager.fromUNIX(this.props.endTime) : undefined
        );
    }

    getDisabledStyles = (style = "color", opacity = 1) => {
        if (opacity < 1) {
            return this.props.isPast
                ? { opacity }
                : undefined;
        }

        return this.props.isPast
            ? { [style]: Color.gray }
            : undefined;
    }

    handleNavigateDetails = () => {
        this.props.navigation.navigate("Booking", {
            bookingItem: {
                itemType: this.props.itemType,
                itemId: this.props.itemId
            }
        });
    }

    handleNavigateIndepth = () => {
        this.props.navigation.navigate("Workspace", {
            workspaceId: this.props.workspaceId
        });
    }

}
