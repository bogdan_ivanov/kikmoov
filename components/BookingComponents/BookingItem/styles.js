import { StyleSheet } from "react-native";

import { image, Font, Color, shadowArea, triangle, buttonText, filledButton } from "@constants/UI";

const trackWidth = 40;
/* eslint-disable-next-line no-magic-numbers */
const triangleSize = trackWidth / 3;

export const styles = StyleSheet.create({
    root: {
        flex: 1,

        position: "relative"
    },
    container: {
        flexDirection: "row",

        paddingTop: 4,
        paddingBottom: 25,
        paddingRight: 17,
        paddingLeft: trackWidth,

        elevation: 0
    },
    contentContainer: {
        paddingTop: 8,
        paddingBottom: 8,
        paddingLeft: 12,
        paddingRight: 12,
    },
    shadow: {
        ...shadowArea,

        borderRadius: 7,
        flex: 1
    },
    triangleBackground: {
        ...triangle({ size: triangleSize, height: triangleSize, color: "#fff", type: "left" }),

        position: "absolute",
        left: trackWidth - triangleSize + 1,
        top: triangleSize * 2
    },
    triangleShadow: {
        ...triangle({ size: triangleSize + 1, height: triangleSize + 1, color: Color.grayLight, type: "left" }),

        position: "absolute",
        left: trackWidth - triangleSize,
        top: triangleSize * 2 - 1
    },
    dotContainer: {
        height: triangleSize * 2,
        width: triangleSize,

        position: "absolute",
        left: triangleSize - 2,
        top: triangleSize * 2,

        backgroundColor: "#fff",

        alignItems: "center",
        justifyContent: "center"
    },
    dot: {
        width: triangleSize,
        height: triangleSize,

        borderRadius: triangleSize / 2,

        backgroundColor: Color.blue
    },
    image: {
        ...image,

        marginTop: 7,
        marginBottom: 7
    },
    typeContainer: {
        flexDirection: "row",
        alignItems: "center"
    },
    dotInfo: {
        width: 3,
        height: 3,

        borderRadius: 1.5,

        marginLeft: 4,
        marginRight: 4,

        backgroundColor: Color.red,
    },
    lineInfo: {
        width: 1.5,
        height: "80%",

        marginLeft: 4,
        marginRight: 4,

        backgroundColor: Color.red,
    },
    dateText: {
        fontFamily: Font.type.medium,
        fontSize: Font.size.s
    },
    typeText: {
        fontFamily: Font.type.medium,
        fontSize: Font.size.s,

        color: Color.red,
    },
    nameText: {
        fontFamily: Font.type.medium,
        fontSize: Font.size.l
    },
    locationText: {
        fontFamily: Font.type.medium,
        fontSize: Font.size.s,

        color: Color.gray
    },
    button: {
        ...filledButton,

        padding: 12,

        borderRadius: 0,
        borderTopColor: Color.grayLight,
        borderTopWidth: 1,

        marginTop: 15
    },
    buttonText: {
        ...buttonText,

        fontSize: Font.size.s,

        color: Color.gray
    }
});
