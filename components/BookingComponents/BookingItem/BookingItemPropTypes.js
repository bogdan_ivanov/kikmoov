import PropTypes from "prop-types";

import { NavigationPropTypes } from "../../../navigation/NavigationPropTypes";

export const BookingItemPropTypes = {
    itemType: PropTypes.oneOf(["viewing", "booking"]).isRequired,
    itemId: PropTypes.number.isRequired,
    isPast: PropTypes.bool.isRequired,

    startTime: PropTypes.number.isRequired,
    status: PropTypes.string.isRequired,
    endTime: PropTypes.number,

    type: PropTypes.string.isRequired,
    coverImageUrl: PropTypes.string,
    capacity: PropTypes.number,
    quantity: PropTypes.number,
    size: PropTypes.number,

    locationAddress: PropTypes.string.isRequired,
    locationName: PropTypes.string.isRequired,

    workspaceId: PropTypes.number.isRequired,

    ...NavigationPropTypes
};
