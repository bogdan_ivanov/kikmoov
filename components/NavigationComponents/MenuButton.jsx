import * as React from "react";
import PropTypes from "prop-types";
import { TouchableOpacity, Image } from "react-native";

import { Images } from "@assets/Images";

export const MenuButton = (props) => (
    <TouchableOpacity
        onPress={props.onPress}
        hitSlop={{ left: 10, right: 10, top: 10, bottom: 10 }}
    >
        <Image
            style={[{ marginLeft: 15, width: 24, height: 18 }, props.white && { tintColor: "#fff" }]}
            source={Images.menuIcon}
        />
    </TouchableOpacity>
);

MenuButton.propTypes = {
    onPress: PropTypes.func.isRequired,

    white: PropTypes.bool
};
