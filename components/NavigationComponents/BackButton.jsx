import React from "react";
import PropTypes from "prop-types";
import { TouchableOpacity, Image } from "react-native";

import { Images } from "@assets/Images";

export const BackButton = ({ onPress, white }) => (
  <TouchableOpacity
    onPress={onPress}
    hitSlop={{ left: 10, right: 10, top: 10, bottom: 10 }}
  >
    <Image
      style={{ marginLeft: 15, width: 16, height: 24 }}
      source={white ? Images.backButtonWhite : Images.backButton}
    />
  </TouchableOpacity>
);

BackButton.propTypes = {
  onPress: PropTypes.func.isRequired,
  white: PropTypes.bool
};
