import * as React from "react";
import PropTypes from "prop-types";
import { Text, Image, StyleSheet, View, Dimensions, FlatList, TouchableWithoutFeedback } from "react-native";

import { Images } from "@assets/Images";
import { Color, Font } from "@constants/UI";

import { withSearchContext, SearchContextPropTypes } from "./Providers/SearchProvider";
import { withSearchControlsContext } from "./Providers/SearchControlsProvider";

const SearchResultsPropTypes = {
    children: PropTypes.func.isRequired,
    outerContainerChildren: PropTypes.func,
    searchContext: PropTypes.shape(SearchContextPropTypes).isRequired
};

@withSearchContext
@withSearchControlsContext
export class SearchResults extends React.Component {
    static propTypes = SearchResultsPropTypes;

    render() {
        if (!this.props.searchContext.searchResults.length) {
            return (
                <TouchableWithoutFeedback accessible={false} onPress={this.props.searchControlsContext.closeSortView}>
                    <View style={styles.root}>
                        <Image resizeMode="contain" source={Images.emptySearchIcon} style={styles.image} />
                        <Text style={styles.text}>Sorry, no results</Text>
                        <Text style={styles.text}>Try widening your search filters</Text>
                    </View>
                </TouchableWithoutFeedback>
            );
        }

        const { children, searchContext, outerContainerChildren, ...scrollViewProps } = this.props;

        return (
            <React.Fragment>
                <FlatList
                    onTouchStart={this.props.searchControlsContext.closeSortView}
                    keyExtractor={(item) => item.workspace.id.toString()}
                    data={this.props.searchContext.searchResults}
                    renderItem={this.props.children}
                    {...scrollViewProps}
                />
                {outerContainerChildren && outerContainerChildren(this.props.searchContext.searchResults)}
            </React.Fragment>
        );
    }
}

const styles = StyleSheet.create({
    root: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    image: {
        width: Dimensions.get("window").width / 2,
        height: Dimensions.get("window").width / 2,

        marginBottom: 25
    },
    text: {
        color: Color.grayDark,

        fontFamily: Font.type.medium,
        fontSize: Font.size.m
    }
});
