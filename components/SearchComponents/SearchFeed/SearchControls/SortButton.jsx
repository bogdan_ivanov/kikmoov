import * as React from "react";
import { withNavigation } from "react-navigation";

import { TouchableOpacity, Image } from "react-native";

import { Images } from "@assets/Images";

import { styles } from "./styles";
import { NavigationPropTypes } from "../../../../navigation/NavigationPropTypes";

@withNavigation
export class SortButton extends React.Component {
    static propTypes = NavigationPropTypes;

    render() {
        if (this.props.navigation.state.params.filtersViewOpened) {
            return null;
        }

        return (
            <TouchableOpacity
                hitSlop={{ left: 10, right: 10, top: 10, bottom: 10 }}
                onPress={this.handlePress}
                style={styles.sortButton}
            >
                <Image resizeMode="stretch" style={styles.sortIcon} source={Images.sortButton} />
            </TouchableOpacity>
        );
    }

    handlePress = () => this.props.navigation.state.params.onSortButtonClick();
}
