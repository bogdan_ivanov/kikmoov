import * as React from "react";
import { View } from "react-native";

import { FilterButton } from "./FilterButton";
import { SortButton } from "./SortButton";

import { styles } from "./styles";

export const SearchControls = () => (
    <View style={styles.container}>
        <FilterButton />
        <SortButton />
    </View>
);
