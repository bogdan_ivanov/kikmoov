import * as React from "react";
import { withNavigation } from "react-navigation";
import { TouchableOpacity, Image } from "react-native";

import { Images } from "@assets/Images";

import { styles } from "./styles";
import { NavigationPropTypes } from "../../../../navigation/NavigationPropTypes";

@withNavigation
export class FilterButton extends React.Component {
    static propTypes = NavigationPropTypes;

    render() {
        return (
            <TouchableOpacity
                onPress={this.handlePress}
                style={styles.filterButton}
                hitSlop={{ left: 10, right: 10, top: 10, bottom: 10 }}
            >
                <Image resizeMode="stretch" style={styles.filterIcon} source={Images.filterButton} />
            </TouchableOpacity>
        );
    }

    handlePress = () => this.props.navigation.state.params.onFiltersButtonClick();
}
