import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
    filterIcon: {
        width: 24,
        height: 22,

        tintColor: "#fff"
    },
    sortIcon: {
        height: 23,
        width: 4.5,

        tintColor: "#fff"
    },
    filterButton: {
        marginRight: 15
    },
    sortButton: {
        width: 24,
        alignItems: "center"
    },
    container: {
        flexDirection: "row",
        paddingRight: 13
    }
});
