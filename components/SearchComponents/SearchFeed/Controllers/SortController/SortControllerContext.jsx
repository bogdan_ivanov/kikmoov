import * as React from "react";
import hoistNonReactStatics from "hoist-non-react-statics";

export const SortControllerContext = React.createContext({
    onSortChange: () => undefined
});

export function withSortController(WrappedComponent) {
    class Enhanced extends React.Component {
        render() {
            return (
                <SortControllerContext.Consumer>
                    {this.renderChildren}
                </SortControllerContext.Consumer>
            );
        }

        renderChildren = (context) => {
            return <WrappedComponent {...this.props} sortContext={context} />;
        }
    }

    return hoistNonReactStatics(Enhanced, WrappedComponent);
}
