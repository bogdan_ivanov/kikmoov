import * as React from "react";
import PropTypes from "prop-types";

import { SortTypes, SortLabels } from "@constants/API";

import { SortControllerContext } from "./SortControllerContext";

import { withSearchContext, SearchContextPropTypes } from "../../Providers/SearchProvider/SearchContext";

@withSearchContext
export class SortController extends React.Component {
    static propTypes = {
        searchContext: PropTypes.shape(SearchContextPropTypes).isRequired
    };

    static sortList = [
        { title: SortLabels.DATE.NEWEST, value: SortTypes.DATE.NEWEST },
        { title: SortLabels.DATE.OLDEST, value: SortTypes.DATE.OLDEST },
        { title: SortLabels.PRICE.LOWEST, value: SortTypes.PRICE.LOWEST },
        { title: SortLabels.PRICE.HIGHEST, value: SortTypes.PRICE.HIGHEST }
    ];

    render() {
        return (
            <SortControllerContext.Provider value={this.childContext}>
                {this.props.children}
            </SortControllerContext.Provider>
        );
    }

    get childContext() {
        return {
            ...this.props.searchContext.sortParams,

            onSortChange: this.handleSortChange
        };
    }

    handleSortChange = (sortBy) => {
        this.props.searchContext.setSortParams({ sortBy });
        this.forceUpdate();

        return this.props.searchContext.processSearch();
    }
}
