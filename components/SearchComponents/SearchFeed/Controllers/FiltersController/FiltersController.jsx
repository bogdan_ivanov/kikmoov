import * as React from "react";

import { FiltersControllerContext } from "./FiltersControllerContext";

import { withSearchContext, SearchContextPropTypes } from "../../Providers/SearchProvider/SearchContext";

@withSearchContext
export class FiltersController extends React.Component {
    static propTypes = SearchContextPropTypes;

    render() {
        return (
            <FiltersControllerContext.Provider value={this.childContext}>
                {this.props.children}
            </FiltersControllerContext.Provider>
        );
    }

    get childContext() {
        return {
            ...this.props.searchContext.filterParams,

            onFacilitiesChange: this.handleFacilitiesChange,
            onPriceRangeChange: this.handlePriceRangeChange,
            onQuantityChange: this.handleQuantityChange,
            onCapacityChange: this.handleCapacityChange,
            onDurationChange: this.handleDurationChange,
            onAddressChange: this.handleAddressChange,
            onTypeChange: this.handleTypeChange,
            onDateChange: this.handleDateChange,

            resetFilters: this.props.searchContext.resetFilters,

            processSearch: this.props.searchContext.processSearch
        };
    }

    handleCapacityChange = (capacity) => {
        this.props.searchContext.setFiltersParams({ capacity });
        this.forceUpdate();
    }

    handleQuantityChange = (quantity) => {
        this.props.searchContext.setFiltersParams({ quantity });
        this.forceUpdate();
    }

    handleAddressChange = (value, valueType) => {
        this.props.searchContext.setFiltersParams({ value, valueType });
        this.forceUpdate();
    }

    handleFacilitiesChange = (facilities) => {
        this.props.searchContext.setFiltersParams({ facilities });
        this.forceUpdate();
    }

    handleDurationChange = (duration) => {
        this.props.searchContext.setFiltersParams({ duration, type: undefined });
        this.forceUpdate();
    }

    handleTypeChange = (type) => {
        this.props.searchContext.setFiltersParams({ type });
        this.forceUpdate();
    }

    handlePriceRangeChange = (priceRange) => {
        this.props.searchContext.setFiltersParams({ ...priceRange });
        this.forceUpdate();
    }

    handleDateChange = (availableFrom) => {
        this.props.searchContext.setFiltersParams({ availableFrom });
        this.forceUpdate();
    }
}
