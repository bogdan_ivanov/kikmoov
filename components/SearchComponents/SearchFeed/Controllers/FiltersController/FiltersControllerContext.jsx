import * as React from "react";
import PropTypes from "prop-types";
import hoistNonReactStatics from "hoist-non-react-statics";

export const FiltersControllerContext = React.createContext({
    onPriceRangeChange: () => undefined,
    onFacilitiesChange: () => undefined,
    onQuantityChange: () => undefined,
    onCapacityChange: () => undefined,
    onDurationChange: () => undefined,
    onAddressChange: () => undefined,
    onTypeChange: () => undefined,
    onDateChange: () => undefined,

    resetFilters: () => undefined,
    processSearch: () => undefined
});

export const FiltersControllerContextPropTypes = {
    onPriceRangeChange: PropTypes.func,
    onFacilitiesChange: PropTypes.func,
    onQuantityChange: PropTypes.func,
    onCapacityChange: PropTypes.func,
    onDurationChange: PropTypes.func,
    onAddressChange: PropTypes.func,
    onTypeChange: PropTypes.func,
    onDateChange: PropTypes.func,

    resetFilters: PropTypes.func,
    processSearch: PropTypes.func
};

export function withFiltersController(WrappedComponent) {
    class Enhanced extends React.Component {
        render() {
            return (
                <FiltersControllerContext.Consumer>
                    {this.renderChildren}
                </FiltersControllerContext.Consumer>
            );
        }

        renderChildren = (context) => {
            return <WrappedComponent {...this.props} filtersContext={context} />;
        }
    }

    return hoistNonReactStatics(Enhanced, WrappedComponent);
}
