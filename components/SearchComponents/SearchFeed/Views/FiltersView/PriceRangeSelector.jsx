import Axios from "axios";
import * as React from "react";
import PropTypes from "prop-types";
import { Toaster } from "react-native-toastboard";
import MultiSlider from "@ptomasroos/react-native-multi-slider";
import { Dimensions, View, Text, ActivityIndicator, InteractionManager } from "react-native";

import { Color } from "@constants/UI";
import { getPriceRange } from "@statelessActions";
import { NumberManager } from "@utils/NumberManager";

import { styles } from "./styles";

const sliderBorderOffset = 35;
const PriceRangeSelectorPropTypes = {
    value: PropTypes.shape({
        minPrice: PropTypes.number.isRequired,
        maxPrice: PropTypes.number.isRequired
    }),
    onChange: PropTypes.func.isRequired
};
const PriceRangeSelectorDefaultProps = {
    value: {
        minPrice: 0,
        maxPrice: 0
    }
};

export class PriceRangeSelector extends React.Component {
    static propTypes = PriceRangeSelectorPropTypes;
    static defaultProps = PriceRangeSelectorDefaultProps;

    state = {};

    cancelToken = Axios.CancelToken.source();

    componentDidMount() {
        this.cancellableLoading = InteractionManager.runAfterInteractions(async () => {
            let response;
            try {
                response = await getPriceRange(this.cancelToken.token);
            } catch (error) {
                if (Axios.isCancel(error)) {
                    return;
                }
                return Toaster.error(error);
            }

            this.setState({
                min: Math.floor(response.data.min),
                max: Math.floor(response.data.max)
            }, () => {
                if (!this.props.value.maxPrice) {
                    return this.props.onChange({
                        minPrice: this.state.min,
                        maxPrice: this.state.max
                    });
                }

                // Assign to new price range from API
                const maxPrice = this.props.value.maxPrice > this.state.max
                    ? this.state.max
                    : this.props.value.maxPrice;
                const minPrice = this.props.value.minPrice < this.state.min
                    ? this.state.min
                    : this.props.value.minPrice;

                this.props.onChange({ minPrice, maxPrice });
            });
        });
    }

    componentWillUnmount() {
        this.cancellableLoading && this.cancellableLoading.cancel();
        this.cancelToken.cancel();
    }

    render() {
        if (!this.state.max) {
            return (
                <View style={{ height: 50, marginTop: 30 }}>
                    <ActivityIndicator size="small" color={Color.gray} />
                </View>
            );
        }

        return (
            <MultiSlider
                markerOffsetY={-22}
                markerOffsetX={-26}
                trackStyle={styles.sliderTrack}
                selectedStyle={styles.sliderRange}
                containerStyle={styles.containerStyle}
                markerContainerStyle={styles.markerContainerStyle}
                sliderLength={Dimensions.get("window").width - (sliderBorderOffset * 2)}

                isMarkersSeparated
                customMarkerLeft={this.markerLeft}
                customMarkerRight={this.markerRight}

                onValuesChangeFinish={this.handleChange}

                step={this.step}
                min={this.state.min}
                max={this.state.max}
                values={[this.valueMin, this.valueMax]}
            />
        );
    }

    get step() {
        /* eslint-disable no-magic-numbers */
        if (this.state.max.toString().length >= 7) {
            return 100;
        } else {
            return 1;
        }
        /* eslint-enable no-magic-numbers */
    }

    get valueMin() {
        return this.props.value.minPrice || this.state.min;
    }

    get valueMax() {
        return this.props.value.maxPrice || this.state.max;
    }

    markerLeft = ({ currentValue }) => {
        const value = NumberManager.abbreviate(currentValue);
        return (
            <View style={[styles.handlerOverflowWrapper, { width: this.getTextWidth(value) }]}>
                <View style={styles.tipOverflowWrapper}>
                    <View style={styles.sliderTip}>
                        <Text numberOfLines={1} style={styles.sliderTipText}>
                            £
                            {value}
                        </Text>
                    </View>
                </View>
                <View style={styles.triangleShadowFrom} />
                <View style={styles.triangleBackgroundFrom} />
                <View style={styles.sliderHandle} />
            </View>
        );
    }

    markerRight = ({ currentValue }) => {
        const value = NumberManager.abbreviate(currentValue);
        return (
            <View style={[styles.handlerOverflowWrapper, { width: this.getTextWidth(value) }]}>
                <View style={styles.sliderHandle} />
                <View style={[styles.tipOverflowWrapper, { top: "auto", bottom: 0 }]}>
                    <View style={styles.sliderTip}>
                        <Text numberOfLines={1} style={styles.sliderTipText}>
                            £
                            {value}
                        </Text>
                    </View>
                </View>
                <View style={styles.triangleShadowTo} />
                <View style={styles.triangleBackgroundTo} />
            </View>
        );
    }

    handleChange = (values) => {
        this.props.onChange({ minPrice: values[0], maxPrice: values[1] });
    }

    getTextWidth = (currentValue) => {
        /* eslint-disable-next-line no-magic-numbers */
        const length = (currentValue.toString().length + 2.5) * 10;

        return `${length}%`;
    }
}
