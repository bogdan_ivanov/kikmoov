import * as React from "react";
import PropTypes from "prop-types";
import { Text } from "react-native";
import Touchable from "react-native-platform-touchable";

import { FilterTypes, FilterLabels } from "@constants/API";

import { styles } from "./styles";

export const DurationSelector = (props) => (
    <React.Fragment>
        <Touchable
            onPress={() => props.onChange(FilterTypes.DURATION.MONTHLY)}
            activeOpacity={props.value === FilterTypes.DURATION.MONTHLY ? 1 : 0.2}
            style={[
                styles.timeButtonDefault,
                props.value === FilterTypes.DURATION.MONTHLY && styles.timeButtonActive]
            }
        >
            <Text
                style={[
                    styles.timeButtonTextDefault,
                    props.value === FilterTypes.DURATION.MONTHLY && styles.timeButtonTextActive]
                }
            >
                {FilterLabels.DURATION.MONTHLY}
            </Text>
        </Touchable>
        <Touchable
            onPress={() => props.onChange(FilterTypes.DURATION.HOURLY)}
            activeOpacity={props.value === FilterTypes.DURATION.HOURLY ? 1 : 0.2}
            style={[
                styles.timeButtonDefault,
                props.value === FilterTypes.DURATION.HOURLY && styles.timeButtonActive]
            }
        >
            <Text
                style={[
                    styles.timeButtonTextDefault,
                    props.value === FilterTypes.DURATION.HOURLY && styles.timeButtonTextActive]
                }
            >
                {FilterLabels.DURATION.HOURLY}
            </Text>
        </Touchable>
    </React.Fragment>
);

DurationSelector.propTypes = {
    onChange: PropTypes.func.isRequired,
    value: PropTypes.string.isRequired
};
