import * as React from "react";
import PropTypes from "prop-types";
import Touchable from "react-native-platform-touchable";
import { View, SafeAreaView, Text, ScrollView } from "react-native";

import { containerStatic } from "@constants/UI";

import { SearchInput } from "@components/partials/SearchInput";
import { NumericInput } from "@components/partials/NumericInput";

import { withSearchControlsContext, SearchControlsContextPropTypes } from "../../Providers";
import { withFiltersController, FiltersControllerContextPropTypes } from "../../Controllers";

import { PriceRangeSelector } from "./PriceRangeSelector";
import { FacilitiesSelector } from "./FacilitiesSelector";
import { StartDateSelector } from "./StartDateSelector";
import { DurationSelector } from "./DurationSelector";
import { TypeSelector } from "./TypeSelector";

import { styles } from "./styles";

@withFiltersController
@withSearchControlsContext
export class FiltersView extends React.Component {
    static propTypes = {
        searchControlsContext: PropTypes.shape(SearchControlsContextPropTypes).isRequired,
        filtersContext: PropTypes.shape(FiltersControllerContextPropTypes).isRequired
    };

    render() {
        if (!this.props.searchControlsContext.filtersViewOpened) {
            return null;
        }

        return (
            <SafeAreaView style={styles.root}>
                <ScrollView keyboardShouldPersistTaps="handled">
                    <View style={styles.lightBackground}>
                        <View style={containerStatic}>
                            <SearchInput
                                duration={this.props.filtersContext.duration}
                                style={{ marginRight: 2, marginLeft: 2 }}
                                value={this.props.filtersContext.value}
                                valueType={this.props.filtersContext.valueType}
                                onChangeText={this.props.filtersContext.onAddressChange}
                            />
                        </View>
                        <View style={styles.buttonContainer}>
                            <DurationSelector
                                value={this.props.filtersContext.duration}
                                onChange={this.props.filtersContext.onDurationChange}
                            />
                        </View>
                        <View style={[styles.buttonContainer, { marginTop: 12 }]}>
                            <TypeSelector
                                value={this.props.filtersContext.type}
                                duration={this.props.filtersContext.duration}
                                onChange={this.props.filtersContext.onTypeChange}
                            />
                        </View>
                    </View>
                    <View style={styles.darkBackground}>
                        <View style={styles.labelWrapper}>
                            <Text style={styles.labelText}>Start date</Text>
                        </View>
                        <View style={[styles.buttonContainer, { justifyContent: "center" }]}>
                            <StartDateSelector
                                value={this.props.filtersContext.availableFrom}
                                onChange={this.props.filtersContext.onDateChange}
                            />
                        </View>
                    </View>
                    <View style={styles.lightBackground}>
                        <View style={styles.labelWrapper}>
                            <Text style={styles.labelText}>{this.amountSelector.label}</Text>
                        </View>
                        <View style={{ alignItems: "center" }}>
                            <NumericInput value={this.amountSelector.value} onChange={this.amountSelector.onChange} />
                        </View>
                    </View>
                    {this.props.filtersContext.minPrice !== this.props.filtersContext.maxPrice && (
                        <View style={styles.darkBackground}>
                            <View style={[styles.labelWrapper, { marginBottom: 0 }]}>
                                <Text style={styles.labelText}>Price range</Text>
                            </View>
                            <PriceRangeSelector
                                value={this.props.filtersContext.maxPrice ? {
                                    minPrice: this.props.filtersContext.minPrice,
                                    maxPrice: this.props.filtersContext.maxPrice
                                } : undefined}
                                onChange={this.props.filtersContext.onPriceRangeChange}
                            />
                        </View>
                    )}
                    <View style={[styles.labelWrapper, { marginTop: 5 }]}>
                        <Text style={styles.labelText}>Facilities</Text>
                    </View>
                    <FacilitiesSelector
                        value={this.props.filtersContext.facilities}
                        onChange={this.props.filtersContext.onFacilitiesChange}
                    />
                </ScrollView>
                <View style={styles.footer}>
                    <Touchable
                        style={styles.footerClearButtonDefault}
                        onPress={this.props.filtersContext.resetFilters}
                    >
                        <Text style={styles.footerClearButtonTextDefault}>Clear</Text>
                    </Touchable>
                    <Touchable
                        style={styles.footerApplyButtonDefault}
                        onPress={this.handleApplyPress}
                    >
                        <Text style={styles.footerApplyButtonTextDefault}>Apply</Text>
                    </Touchable>
                </View>
            </SafeAreaView>
        );
    }

    get amountSelector() {
        return {
            label: "Capacity",
            value: this.props.filtersContext.capacity || 0,
            onChange: this.props.filtersContext.onCapacityChange
        };
    }

    handleApplyPress = () => {
        this.props.searchControlsContext.closeFiltersView();
        this.props.filtersContext.processSearch();
    }
}
