import Axios from "axios";
import * as React from "react";
import PropTypes from "prop-types";
import { Toaster } from "react-native-toastboard";
import Touchable from "react-native-platform-touchable";
import { View, ActivityIndicator, Text, InteractionManager, Image } from "react-native";

import { Color, containerStatic } from "@constants/UI";

import { fetchFacilitiesList } from "@statelessActions";

import { ExpandableFlatList } from "@components/partials/ExpandableFlatList";

import { styles } from "./styles";

const FacilitiesSelectorPropTypes = {
    value: PropTypes.array.isRequired,
    onChange: PropTypes.func.isRequired
};

export class FacilitiesSelector extends React.Component {
    static propTypes = FacilitiesSelectorPropTypes;

    state = {
        availableFacilites: undefined,
        loading: true,
    };

    cancelToken = Axios.CancelToken.source();

    componentDidMount() {
        this.cancellableLoading = InteractionManager.runAfterInteractions(async () => {
            let response;
            try {
                response = await fetchFacilitiesList(this.cancelToken.token);
            } catch (error) {
                if (Axios.isCancel(error)) {
                    return;
                }
                return Toaster.error(error);
            }

            this.setState({ availableFacilites: response.data, loading: false });
        });
    }

    componentWillUnmount() {
        this.cancellableLoading && this.cancellableLoading.cancel();
        this.cancelToken.cancel();
    }

    render() {
        if (this.state.loading) {
            return (
                <View style={{ padding: 24 }}>
                    <ActivityIndicator size="small" color={Color.gray} />
                </View>
            );
        }

        return (
            <View style={[containerStatic, { paddingBottom: 24, paddingTop: 5 }]}>
                <ExpandableFlatList itemsPerRow={4} spaceBetweenItems={5} items={this.state.availableFacilites}>
                    {this.renderItems}
                </ExpandableFlatList>
            </View>
        );
    }

    renderItems = (data) => {
        const isActive = this.props.value.includes(data.item.slug);

        return (
            <Touchable
                style={[styles.facilityItem, isActive && { backgroundColor: "#fff" }]}
                onPress={this.handleItemPress(data.item.slug)}
            >
                <React.Fragment>
                    <Image
                        resizeMode="contain"
                        source={{ uri: data.item.appIcon }}
                        style={[styles.image, { tintColor: isActive ? Color.red : Color.gray }]}
                    />
                    <Text style={[styles.text, isActive && { color: Color.red }]}>{data.item.name}</Text>
                </React.Fragment>
            </Touchable>
        );
    }

    handleItemPress = (item) => () => {
        const found = this.props.value.findIndex((selected) => selected === item);

        // Select
        if (!~found) {
            return this.props.onChange([
                ...this.props.value,
                item
            ]);
        }

        // Remove selection
        this.props.onChange([
            ...this.props.value.slice(0, found),
            ...this.props.value.slice(found + 1)
        ]);
    }
}
