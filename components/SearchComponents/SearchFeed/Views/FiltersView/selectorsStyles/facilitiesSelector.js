import { Color, Font } from "@constants/UI";

export const facilitiesSelector = {
    facilityItem: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",

        paddingLeft: 1,
        paddingRight: 1,
        paddingTop: 10,
        paddingBottom: 5,

        borderRadius: 5,

        backgroundColor: Color.darkWhite
    },
    image: {
        width: "50%",
        height: "50%",

        marginBottom: 10
    },
    text: {
        color: Color.gray,

        fontFamily: Font.type.medium,
        fontSize: Font.size.xxs,

        textAlign: "center"
    }
};
