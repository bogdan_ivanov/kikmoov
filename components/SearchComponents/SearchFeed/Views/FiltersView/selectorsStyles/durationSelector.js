import { filledButton, Color, buttonText } from "@constants/UI";

export const durationSelector = {
    timeButtonDefault: {
        ...filledButton,

        width: "47.5%",

        backgroundColor: Color.grayLight
    },
    timeButtonTextDefault: {
        ...buttonText,

        color: Color.gray
    },
    timeButtonActive: {
        backgroundColor: Color.red
    },
    timeButtonTextActive: {
        color: "#fff"
    }
};
