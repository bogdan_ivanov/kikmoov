import { buttonText, Color, filledButton } from "@constants/UI";

export const typeSelector = {
    areaButtonDefault: {
        ...filledButton,

        width: "47.5%",

        backgroundColor: Color.grayLight
    },
    areaButtonTextDefault: {
        ...buttonText,

        marginTop: 5,

        color: Color.gray
    },
    areaButtonActive: {
        backgroundColor: Color.red
    },
    areaButtonTextActive: {
        color: "#fff"
    }
};
