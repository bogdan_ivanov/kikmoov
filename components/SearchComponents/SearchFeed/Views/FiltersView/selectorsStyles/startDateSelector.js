import {
    buttonGroupContainer,
    modalOverlay,
    filledButton,
    buttonText,
    Color,
    Font
} from "@constants/UI";

export const startDateSelector = {
    calendarInput: {
        backgroundColor: "#fff",

        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 12,
        paddingBottom: 12,

        borderRadius: 40,

        flexDirection: "row"
    },
    calendarInputText: {
        fontFamily: Font.type.medium,
        fontSize: Font.size.m,

        marginLeft: 15,
        marginRight: 10
    },
    calendarRoot: {
        ...modalOverlay,

        padding: 20,
    },
    caledarContainer: {
        borderRadius: 5,

        overflow: "hidden",

        backgroundColor: "#fff"
    },
    calendarHeader: {
        flexDirection: "row",
        justifyContent: "center",

        backgroundColor: Color.red,

        padding: 20
    },
    calendarFooter: {
        ...buttonGroupContainer,

        marginTop: 10
    },
    calendarButton: {
        ...filledButton,

        flex: 1,

        backgroundColor: "#fff"
    },
    calendarButtonTextCancel: {
        ...buttonText,

        marginTop: 10,
        marginBottom: 10,

        color: Color.gray
    },
    separator: {
        width: 1,

        marginTop: 10,
        marginBottom: 10,

        backgroundColor: Color.grayLight
    },
    calendarButtonTextDone: {
        ...buttonText,

        marginTop: 10,
        marginBottom: 10,

        color: Color.red
    },
    calendarHeaderText: {
        color: "#fff",

        fontFamily: Font.type.regular,
        fontSize: Font.size.s,

        textAlign: "center",

        margin: 2.5,
        marginBottom: 0,
        marginTop: 0
    }
};
