import { shadowArea, Color, Font, triangle } from "@constants/UI";

const triangleBackground = (type) => ({
    ...triangle({ size: 7, height: 7, color: "#fff", type }),

    position: "absolute",

    left: "50%",

    transform: [{ translateX: -5 }]
});

const triangleShadow = (type) => ({
    ...triangle({ size: 8, height: 8, color: Color.grayLight, type }),

    position: "absolute",

    left: "50%",

    transform: [{ translateX: -6 }]
});

export const priceRangeSelector = {
    triangleBackgroundTo: {
        ...triangleBackground("top"),

        bottom: 25,
    },
    triangleShadowTo: {
        ...triangleShadow("top"),

        bottom: 25.5,
    },
    triangleBackgroundFrom: {
        ...triangleBackground("bottom"),

        top: 25,
    },
    triangleShadowFrom: {
        ...triangleShadow("bottom"),

        top: 25.5
    },
    sliderTrack: {
        height: 3,

        backgroundColor: Color.gray,

        borderRadius: 1.5
    },
    sliderRange: {
        backgroundColor: Color.red,
    },
    sliderTipText: {
        textAlign: "center",

        fontFamily: Font.type.regular
    },
    handlerOverflowWrapper: {
        alignItems: "center",

        position: "relative",

        /* eslint-disable-next-line no-magic-numbers */
        minWidth: Font.size.m * 2.5,

        padding: 2,
    },
    sliderHandle: {
        ...shadowArea,

        width: 20,
        height: 20,

        backgroundColor: Color.red,

        borderWidth: 2,
        borderColor: "#fff",
        borderRadius: 10,

        marginTop: 35,
        marginBottom: 35
    },
    sliderTip: {
        ...shadowArea,

        width: "100%",

        padding: 2,

        borderRadius: 5
    },
    tipOverflowWrapper: {
        position: "absolute",
        top: 0,
        left: 0,
        right: 0,

        padding: 3,

        alignItems: "center",

        elevation: 0
    },
    markerContainerStyle: {
        height: "auto",
        width: 100
    },
    containerStyle: {
        alignSelf: "center",

        paddingTop: 60,
        paddingBottom: 60,
        paddingRight: 40,
        paddingLeft: 40
    }
};
