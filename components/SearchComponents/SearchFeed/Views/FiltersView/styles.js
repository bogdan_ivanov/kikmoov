import { StyleSheet } from "react-native";

import { filledButton, buttonText, shadowArea, Color, Font, containerStatic } from "@constants/UI";

import * as SelectorsStyles from "./selectorsStyles";

export const styles = StyleSheet.create({
    footer: {
        ...shadowArea,
        shadowOffset: {
            width: 0,
            height: -3
        },
        shadowRadius: 1,
        shadowOpacity: .5,

        justifyContent: "space-between",
        alignItems: "stretch",
        flexDirection: "row",

        padding: 20
    },
    footerClearButtonDefault: {
        ...filledButton,

        width: "39%",

        backgroundColor: "#fff"
    },
    footerClearButtonTextDefault: {
        ...buttonText,

        color: Color.gray
    },
    footerApplyButtonDefault: {
        width: "59%",

        ...filledButton,

        backgroundColor: Color.red
    },
    footerApplyButtonTextDefault: {
        ...buttonText,

        color: "#fff"
    },
    root: {
        position: "absolute",

        backgroundColor: "#fff",

        zIndex: 2,
        elevation: 2,

        top: 0,
        bottom: 0,

        width: "100%"
    },
    labelWrapper: {
        ...containerStatic,

        marginBottom: 10
    },
    labelText: {
        fontFamily: Font.type.medium,
        fontSize: Font.size.l
    },
    buttonContainer: {
        ...containerStatic,

        justifyContent: "space-between",
        alignItems: "stretch",
        flexDirection: "row"
    },
    darkBackground: {
        backgroundColor: Color.darkWhite,

        marginTop: 15,
        marginBottom: 15,

        paddingTop: 15,
        paddingBottom: 15
    },
    lightBackground: {
        marginBottom: 15,
        marginTop: 15
    },

    ...SelectorsStyles.priceRangeSelector,
    ...SelectorsStyles.facilitiesSelector,
    ...SelectorsStyles.startDateSelector,
    ...SelectorsStyles.durationSelector,
    ...SelectorsStyles.typeSelector
});
