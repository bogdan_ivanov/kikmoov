import * as React from "react";
import PropTypes from "prop-types";
import { Image, Text } from "react-native";
import Touchable from "react-native-platform-touchable";

import { Images } from "@assets/Images";

import { Color } from "@constants/UI";
import { FilterTypes, FilterLabels } from "@constants/API";

import { styles } from "./styles";

const AreaSelector = (props) => {
    switch (props.duration) {
        case FilterTypes.DURATION.HOURLY: {
            const isActive = props.value === FilterTypes.AREA.MEETING;

            return (
                <Touchable
                    style={[styles.areaButtonDefault, isActive && styles.areaButtonActive]}
                    onPress={() => props.onChange(isActive ? undefined : FilterTypes.AREA.MEETING)}
                >
                    <React.Fragment>
                        <Image
                            resizeMode="cover"
                            source={Images.meetingRoom}
                            style={{ width: 22, height: 22, tintColor: isActive ? "#fff" : Color.gray }}
                        />
                        <Text style={[styles.areaButtonTextDefault, isActive && styles.areaButtonTextActive]}>
                            {FilterLabels.AREA.MEETING}
                        </Text>
                    </React.Fragment>
                </Touchable>
            );
        }
        case FilterTypes.DURATION.MONTHLY: {
            const isActive = props.value === FilterTypes.AREA.PRIVATE;

            return (
                <Touchable
                    style={[styles.areaButtonDefault, isActive && styles.areaButtonActive]}
                    onPress={() => props.onChange(isActive ? undefined : FilterTypes.AREA.PRIVATE)}
                >
                    <React.Fragment>
                        <Image
                            resizeMode="cover"
                            source={Images.privateOffice}
                            style={{ width: 22, height: 22, tintColor: isActive ?  "#fff" : Color.gray }}
                        />
                        <Text style={[styles.areaButtonTextDefault, isActive && styles.areaButtonTextActive]}>
                            {FilterLabels.AREA.PRIVATE}
                        </Text>
                    </React.Fragment>
                </Touchable>
            );
        }
        default: {
            return null;
        }
    }
};

export const TypeSelector = (props) => (
    <React.Fragment>
        <Touchable
            style={[styles.areaButtonDefault, props.value === FilterTypes.AREA.DESK && styles.areaButtonActive]}
            onPress={() => props.onChange(props.value === FilterTypes.AREA.DESK ? undefined : FilterTypes.AREA.DESK)}
        >
            <React.Fragment>
                <Image
                    resizeMode="cover"
                    source={Images.desk}
                    style={{
                        width: 27, height: 19.28,
                        tintColor: props.value === FilterTypes.AREA.DESK ?  "#fff" : Color.gray
                    }}
                />
                <Text
                    style={[
                        styles.areaButtonTextDefault,
                        props.value === FilterTypes.AREA.DESK && styles.areaButtonTextActive
                    ]}
                >
                    {FilterLabels.AREA.DESK}
                </Text>
            </React.Fragment>
        </Touchable>
        {AreaSelector(props)}
    </React.Fragment>
);

TypeSelector.propTypes = {
    value: PropTypes.string,
    onChange: PropTypes.func.isRequired
};
