import * as React from "react";
import PropTypes from "prop-types";
import Touchable from "react-native-platform-touchable";
import { View, Text, Modal, TouchableOpacity, Image } from "react-native";

import { Images } from "@assets/Images";

import { Font } from "@constants/UI";

import { DateManager } from "@utils/DateManager";
import { StatusBarManager } from "@utils/StatusBarManager";

import { DateSelector } from "@components/partials/DateSelector";

import { styles } from "./styles";

const StartDateSelectorPropTypes = {
    value: PropTypes.number,
    onChange: PropTypes.func.isRequired
};

export class StartDateSelector extends React.PureComponent {
    static propTypes = StartDateSelectorPropTypes;

    constructor(props) {
        super(props);

        this.state = {
            modalOpened: false,
            timestamp: Date.now()
        };
    }

    render() {
        return (
            <React.Fragment>
                <TouchableOpacity
                    onPress={this.handleOpenModal}
                    style={styles.calendarInput}
                >
                    <Image
                        resizeMode="stretch"
                        source={Images.calendar}
                        style={{ width: 18, height: 18 }}
                    />
                    <Text style={styles.calendarInputText}>
                        {DateManager.parsedDateFromTimestamp(this.props.value)}
                    </Text>
                </TouchableOpacity>
                <Modal
                    transparent
                    animationType="fade"
                    visible={this.state.modalOpened}
                    onRequestClose={this.handleCloseModal}
                >
                    <View style={styles.calendarRoot}>
                        <View style={styles.caledarContainer}>
                            <View style={styles.calendarHeader}>
                                <Text style={[styles.calendarHeaderText, { fontFamily: Font.type.light }]}>
                                    Start date:
                                </Text>
                                <Text style={styles.calendarHeaderText}>
                                    {DateManager.parsedDateFromTimestamp(this.state.timestamp)}
                                </Text>
                            </View>
                            <DateSelector
                                value={this.state.timestamp}
                                onChange={this.handleDayPress}
                            />
                            <View style={styles.calendarFooter}>
                                <Touchable style={styles.calendarButton} onPress={this.handleCloseModal}>
                                    <Text style={styles.calendarButtonTextCancel}>Cancel</Text>
                                </Touchable>
                                <View style={styles.separator} />
                                <Touchable style={styles.calendarButton} onPress={this.handleDone}>
                                    <Text style={styles.calendarButtonTextDone}>Done</Text>
                                </Touchable>
                            </View>
                        </View>
                    </View>
                </Modal>
            </React.Fragment>
        );
    }

    handleDayPress = ({ timestamp }) => {
        this.setState({ timestamp });
    }

    handleOpenModal = () => {
        StatusBarManager.setState("hidden");
        this.setState({ modalOpened: true });
    }

    handleCloseModal = () => {
        StatusBarManager.restoreState();
        this.setState({ modalOpened: false });
    }

    handleDone = () => {
        this.props.onChange(this.state.timestamp);

        this.handleCloseModal();
    }
}
