import * as React from "react";
import { View, Text, Animated } from "react-native";

import { withSearchControlsContext } from "../../Providers";
import { withSortController } from "../../Controllers";

import { styles } from "./styles";
import { MappedSortList } from "./MappedSortList";

@withSortController
@withSearchControlsContext
export class SortView extends React.Component {
    opacity = new Animated.Value(0);

    state = {
        visible: false
    };

    componentDidUpdate(prevProps) {
        if (prevProps.searchControlsContext.sortViewOpened !== this.props.searchControlsContext.sortViewOpened) {
            Animated.timing(this.opacity, {
                toValue: Number(this.props.searchControlsContext.sortViewOpened),
                duration: 250
            }).start(() => {
                this.setState({
                    visible: this.props.searchControlsContext.sortViewOpened
                });
            });
        }
    }

    render() {
        if (!this.state.visible && !this.props.searchControlsContext.sortViewOpened) {
            return null;
        }

        return (
            <Animated.View style={[styles.overflowWrapper, { opacity: this.opacity }]}>
                <View style={styles.root}>
                    <View style={styles.shadow}>
                        <Text style={styles.title}>sort by:</Text>
                        <MappedSortList {...this.props} />
                    </View>
                </View>
                <View style={styles.triangleShadow} />
                <View style={styles.triangleBackground} />
            </Animated.View>
        );
    }

}
