import { StyleSheet, Platform } from "react-native";

import { Color, shadowArea, Font, triangle } from "@constants/UI";

export const styles = StyleSheet.create({
    overflowWrapper: {
        position: "absolute",

        right: 24,

        top: 0,

        zIndex: 3,
        elevation: 3,
    },
    root: {
        marginTop: 20,
        padding: 1,
        paddingBottom: 4,

        width: 170,

        elevation: 0
    },
    shadow: {
        ...shadowArea,

        borderRadius: 5,
        borderTopRightRadius: 0,

        paddingTop: 15,
        paddingBottom: 27
    },
    triangleBackground: {
        ...triangle({ color: "#fff", size: 20, height: 20, type: "bottomRight" }),

        position: "absolute",
        top: 2,
        right: Platform.select({ ios: 2, android: 1 }),
    },
    triangleShadow: {
        ...triangle({ color: Color.grayLight, size: 21, height: 21, type: "bottomRight" }),

        position: "absolute",
        top: 0,
        right: Platform.select({ ios: 1, android: 0 }),
    },
    title: {
        fontSize: Font.size.m,
        fontFamily: Font.type.light,

        marginLeft: 27
    },
    itemWrapper: {
        flexDirection: "row",
        alignItems: "center",

        marginTop: 20,
        paddingLeft: 27
    },
    itemDefault: {
        fontSize: Font.size.m,
        fontFamily: Font.type.medium,

        color: Color.gray,
    },
    itemActive: {
        fontSize: Font.size.m,
        fontFamily: Font.type.medium,

        color: Color.red,
    },
    dot: {
        width: 6.5,
        height: 6.5,

        borderRadius: 3.25,

        backgroundColor: Color.red,

        marginLeft: -15.75,
        marginRight: 9.5
    }
});
