import * as React from "react";
import { TouchableOpacity, Text, View } from "react-native";

import { SortController } from "../../Controllers";
import { styles } from "./styles";

export const MappedSortList = (props) => {
    const onItemPress = (value) => async () => {
        await props.sortContext.onSortChange(value);
        props.searchControlsContext.closeSortView();
    };

    const WrapperProps = (value) => ({
        ...value !== props.sortContext.sortBy
            ? { onPress: onItemPress(value) }
            : { activeOpacity: 1 }
    });

    return SortController.sortList.map(({ title, value }) => (
        <TouchableOpacity
            style={styles.itemWrapper}
            {...WrapperProps(value)}
            key={value}
        >
            {value === props.sortContext.sortBy && <View style={styles.dot} />}
            <Text
                style={value === props.sortContext.sortBy ? styles.itemActive : styles.itemDefault}
            >
                {title}
            </Text>
        </TouchableOpacity>
    ));
};
