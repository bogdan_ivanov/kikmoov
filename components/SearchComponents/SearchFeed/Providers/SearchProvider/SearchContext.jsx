import * as React from "react";
import PropTypes from "prop-types";
import hoistNonReactStatics from "hoist-non-react-statics";

export const SearchContext = React.createContext({
    setSortParams: () => undefined,
    setFilerParams: () => undefined,

    resetFilters: () => undefined,
    proccessSearch: () => undefined,

    sortParams: {},
    filterParams: {},

    searchResults: []
});

export const SearchContextPropTypes = {
    setSortParams: PropTypes.func,
    setFilerParams: PropTypes.func,

    resetFilters: PropTypes.func,
    proccessSearch: PropTypes.func,

    sortParams: PropTypes.object,
    filterParams: PropTypes.object,

    searchResults: PropTypes.array
};

export function withSearchContext(WrappedComponent) {
    class Enhanced extends React.Component {
        render() {
            return (
                <SearchContext.Consumer>
                    {this.renderChildren}
                </SearchContext.Consumer>
            );
        }

        renderChildren = (context) => {
            return <WrappedComponent {...this.props} searchContext={context} />;
        }
    }

    return hoistNonReactStatics(Enhanced, WrappedComponent);
}
