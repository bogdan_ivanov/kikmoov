import Axios from "axios";
import * as React from "react";
import PropTypes from "prop-types";
import { Toaster } from "react-native-toastboard";
import { ActivityIndicator, View, InteractionManager } from "react-native";

import { Color } from "@constants/UI";
import { SortTypes, FilterTypes } from "@constants/API";

import { search } from "@statelessActions";

import { SortController, FiltersController } from "../../Controllers";

import { SearchContext } from "./SearchContext";

const SearchProviderPropTypes = {
    initialValue: PropTypes.string.isRequired,
    initialValueType: PropTypes.string.isRequired,
    initialDuration: PropTypes.string.isRequired,

    topLayoutChildren: PropTypes.element
};

export class SearchProvider extends React.Component {
    static propTypes = SearchProviderPropTypes;

    constructor(props) {
        super(props);

        this.sortParams = this.initialSortParams;
        this.filterParams = this.initialFilterParams;

        this.state = {
            loading: true,
            searchResults: []
        };
    }

    componentDidMount() {
        this.cancellableLoading = InteractionManager.runAfterInteractions(this.processSearch);
    }

    componentWillUnmount() {
        this.cancellableLoading && this.cancellableLoading.cancel();
        this.cancelToken && this.cancelToken.cancel();
    }

    render() {
        return (
            <SearchContext.Provider value={this.childContext}>
                <FiltersController>
                    <SortController>
                        {this.props.topLayoutChildren || null}
                        {this.renderChildren()}
                    </SortController>
                </FiltersController>
            </SearchContext.Provider>
        );
    }

    get childContext() {
        return {
            setSortParams: this.setSortParams,
            setFiltersParams: this.setFiltersParams,

            sortParams: this.sortParams,
            filterParams: this.filterParams,

            resetFilters: this.handleResetFilters,
            processSearch: this.processSearch,

            searchResults: this.state.searchResults
        };
    }

    get sortQuery() {
        return Object.keys(SortTypes)
            .find((type) =>
                Object.keys(SortTypes[type])
                    .map((value) => SortTypes[type][value])
                    .includes(this.sortParams.sortBy)
            );
    }

    get initialSortParams() {
        return {
            sortBy: SortController.sortList[0].value
        };
    }

    get initialFilterParams() {
        return {
            valueType: this.props.initialValueType,
            duration: this.props.initialDuration,
            value: this.props.initialValue,
            availableFrom: Date.now(),
            minPrice: 0,
            facilities: []
        };
    }

    renderChildren = () => {
        if (this.state.loading) {
            return (
                <View style={{ flex: 1, justifyContent: "center" }}>
                    <ActivityIndicator size="large" color={Color.gray} />
                </View>
            );
        }

        return this.props.children;
    }

    processSearch = async () => {
        this.cancelToken && this.cancelToken.cancel();
        this.cancelToken = Axios.CancelToken.source();

        this.setState({ loading: true });

        const filterParams = this.cleanUpParams({
            ...this.filterParams,
            [this.filterParams.valueType]: this.filterParams.value
        });

        let response;
        try {
            response = await search(
                { ...filterParams, [this.sortQuery.toLowerCase()]: this.sortParams.sortBy },
                this.cancelToken.token
            );
        } catch (error) {
            if (Axios.isCancel(error)) {
                return;
            }

            return Toaster.error(error);
        }

        this.setState({ loading: false, searchResults: response.data.results });
    }

    handleResetFilters = () => {
        this.filterParams = {
            ...this.initialFilterParams,
            value: ""
        };

        this.forceUpdate();
    }

    setFiltersParams = (params) => {
        Object.assign(this.filterParams, params);
    }

    setSortParams = (params) => {
        Object.assign(this.sortParams, params);
    }

    cleanUpParams = (filterParams) => {
        switch (filterParams.type) {
            case FilterTypes.AREA.DESK: {
                delete filterParams.capacity;
                break;
            }
            case FilterTypes.AREA.PRIVATE:
            case FilterTypes.AREA.MEETING: {
                delete filterParams.quantity;
                break;
            }
            default: {
                delete filterParams.type;
                filterParams.capacity = 0;
            }
        }

        delete filterParams.valueType;
        delete filterParams.value;

        return filterParams;
    }
}
