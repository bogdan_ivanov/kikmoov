import * as React from "react";
import { withNavigation } from "react-navigation";
import hoistNonReactStatics from "hoist-non-react-statics";

import { SearchControlsContext, SearchControlsContextInitialState } from "./SearchControlsContext";
import { NavigationPropTypes } from "../../../../../navigation/NavigationPropTypes";

@withNavigation
export class SearchControlsProvider extends React.Component {
    static propTypes = NavigationPropTypes;

    constructor(props) {
        super(props);

        this.state = SearchControlsContextInitialState;

        props.navigation.setParams({
            filtersViewOpened: this.state.filtersViewOpened,
            onSortButtonClick: this.handleSortButtonClick,
            onFiltersButtonClick: this.handleFiltersButtonClick
        });
    }

    componentWillUnmount() {
        if (this.props.navigation.state.params) {
            delete this.props.navigation.state.params.filtersViewOpened;
            delete this.props.navigation.state.params.onSortButtonClick;
            delete this.props.navigation.state.params.onFiltersButtonClick;
        }
    }

    render() {
        return (
            <SearchControlsContext.Provider value={this.childContext}>
                {this.props.children}
            </SearchControlsContext.Provider>
        );
    }

    get childContext() {
        return {
            ...this.state,

            onSortButtonClick: this.handleSortButtonClick,
            onFiltersButtonClick: this.handleFiltersButtonClick,

            closeSortView: this.handleCloseSortView,
            closeFiltersView: this.handleCloseFiltersView
        };
    }

    handleCloseSortView = () => {
        this.setState({ sortViewOpened: false });
    }

    handleCloseFiltersView = () => {
        this.setState({ filtersViewOpened: false }, () => {
            this.props.navigation.setParams({
                filtersViewOpened: this.state.filtersViewOpened,
            });
        });
    }

    handleSortButtonClick = () => {
        if (this.state.filtersViewOpened && !this.state.sortViewOpened) {
            return;
        }

        this.setState(({ sortViewOpened }) => ({
            sortViewOpened: !sortViewOpened
        }));
    }

    handleFiltersButtonClick = () => {
        this.setState(({ filtersViewOpened }) => ({
            filtersViewOpened: !filtersViewOpened
        }), () => {
            this.props.navigation.setParams({
                filtersViewOpened: this.state.filtersViewOpened,
            });

            if (this.state.filtersViewOpened && this.state.sortViewOpened) {
                this.setState({ sortViewOpened: false });
            }
        });
    }
}

export function withSearchControlsProvider(WrappedComponent) {
    function Enhanced(props) {
        return (
            <SearchControlsProvider>
                <WrappedComponent {...props} />
            </SearchControlsProvider>
        );
    }

    return hoistNonReactStatics(Enhanced, WrappedComponent);
}
