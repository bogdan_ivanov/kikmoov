import * as React from "react";
import PropTypes from "prop-types";
import hoistNonReactStatics from "hoist-non-react-statics";

export const SearchControlsContextInitialState = {
    sortViewOpened: false,
    filtersViewOpened: false
};

export const SearchControlsContext = React.createContext({
    ...SearchControlsContextInitialState,

    onFiltersButtonClick: () => undefined,
    onSortButtonClick: () => undefined,

    closeFiltersView: () => undefined,
    closeSortView: () => undefined
});

export const SearchControlsContextPropTypes = {
    sortViewOpened: PropTypes.bool,
    filtersViewOpened: PropTypes.bool,

    onFiltersButtonClick: PropTypes.func,
    onSortButtonClick: PropTypes.func,

    closeFiltersView: PropTypes.func,
    closeSortView: PropTypes.func
};

export function withSearchControlsContext(WrappedComponent) {
    class Enhanced extends React.Component {
        render() {
            return (
                <SearchControlsContext.Consumer>
                    {this.renderChildren}
                </SearchControlsContext.Consumer>
            );
        }

        renderChildren = (context) => {
            return <WrappedComponent {...this.props} searchControlsContext={context} />;
        }
    }

    return hoistNonReactStatics(Enhanced, WrappedComponent);
}
