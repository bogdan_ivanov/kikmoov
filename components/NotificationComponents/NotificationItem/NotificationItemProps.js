import PropTypes from "prop-types";

import { NavigationPropTypes } from "../../../navigation/NavigationPropTypes";

export const ActionType = {
	VIEWING_TODAY: "VIEWING_TODAY",
	VIEWING_ACCEPTED: "VIEWING_ACCEPTED",
	BOOKING_ACCEPTED: "BOOKING_ACCEPTED",
	EXISTING_SAVED_WORKSPACES: "EXISTING_SAVED_WORKSPACES",
	ONE_DAY_BEFORE_BOOKING_HOURLY: "ONE_DAY_BEFORE_BOOKING_HOURLY",
	ONE_DAY_BEFORE_BOOKING_MONTHLY: "ONE_DAY_BEFORE_BOOKING_MONTHLY",
	DO_NOT_APP_USE_MORE_THAN_MONTH: "DO_NOT_APP_USE_MORE_THAN_MONTH"
};

export const NotificationItemPropTypes = {
	message: PropTypes.string.isRequired,
	createdAt: PropTypes.number.isRequired,
	action: PropTypes.shape({
		actionType: PropTypes.string.isRequired,
		id: PropTypes.number
	}).isRequired,

	...NavigationPropTypes
};
