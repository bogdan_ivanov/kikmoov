import { StyleSheet } from "react-native";

import { shadowArea, Font, Color, buttonText } from "@constants/UI";

export const styles = StyleSheet.create({
	container: {
		padding: 1,
		paddingBottom: 4,

		marginBottom: 4
	},
	shadow: {
		...shadowArea,

		borderRadius: 10,

		padding: 10
	},
	message: {
		fontFamily: Font.type.medium,
		fontSize: Font.size.m
	},
	messageBold: {
		fontFamily: Font.type.semiBold,
		fontSize: Font.size.m
	},
	separator: {
		width: "100%",

		height: 1,

		marginTop: 10,
		marginBottom: 10,

		backgroundColor: Color.grayLight
	},
	buttonText: {
		...buttonText,

		color: Color.red
	}
});
