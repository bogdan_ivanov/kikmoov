import * as React from "react";
import shortId from "shortid";
import { withNavigation } from "react-navigation";
import { TouchableOpacity, Text, View } from "react-native";

import { NotificationItemPropTypes, ActionType } from "./NotificationItemProps";
import { styles } from "./styles";

@withNavigation
export class NotificationItem extends React.Component {
	static propTypes = NotificationItemPropTypes;

	render() {
		const { navigate, button } = this.getActionDescription();

		return (
			<TouchableOpacity style={styles.container} onPress={navigate}>
				<View style={styles.shadow}>
					<Text style={styles.message}>{this.parseMessage(this.props.message)}</Text>
					{button && (
						<React.Fragment>
							<View style={styles.separator} />
							<Text style={styles.buttonText}>{button}</Text>
						</React.Fragment>
					)}
				</View>
			</TouchableOpacity>
		);
	}

	parseMessage = (message) => {
		return message.split(/\[|\]/g).map((item, index) => {
			if (index % 2) {
				return <Text key={shortId.generate()} style={styles.messageBold}>{item}</Text>;
			}

			return item ? item : null;
		});
	}

	getActionDescription = () => {
		switch (this.props.action.actionType) {
			case ActionType.VIEWING_TODAY:
			case ActionType.VIEWING_ACCEPTED: {
				return {
					navigate: () => this.props.navigation.navigate("Booking", {
						bookingItem: {
							itemType: "viewing",
							itemId: this.props.action.id
						}
					}),
					button: "View viewing"
				};
			}
			case ActionType.BOOKING_ACCEPTED:
			case ActionType.ONE_DAY_BEFORE_BOOKING_HOURLY:
			case ActionType.ONE_DAY_BEFORE_BOOKING_MONTHLY: {
				return {
					navigate: () => this.props.navigation.navigate("Booking", {
						bookingItem: {
							itemType: "booking",
							itemId: this.props.action.id
						}
					}),
					button: "View booking"
				};
			}
			case ActionType.EXISTING_SAVED_WORKSPACES: {
				return {
					navigate: () => this.props.navigation.navigate("SavedWorkspaces"),
					button: "View saved workspaces"
				};
			}
			case ActionType.DO_NOT_APP_USE_MORE_THAN_MONTH: {
				return {
					navigate: () => this.props.navigation.navigate("Main", { focus: true }),
					button: "Start browsing"
				};
			}
			default: {
				return {
					navigate: () => this.props.navigation.navigate("Main")
				};
			}
		}
	}
}
