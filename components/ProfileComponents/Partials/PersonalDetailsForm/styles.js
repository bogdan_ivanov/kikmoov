import { StyleSheet } from "react-native";

import { Color, filledButton, buttonText, Font, backgroundInput } from "@constants/UI";

export const styles = StyleSheet.create({
    form: {
        width: "100%"
    },
    input: {
       ...backgroundInput,

       marginTop: 25,
       padding: 7
    },
    inputError: {
        color: Color.redError
    },
    error: {
        color: Color.redError,
        fontFamily: Font.type.light,
    },
    submitButtonDisabled: {
        marginTop: 30,
        ...filledButton,
        backgroundColor: Color.grayLight
    },
    submitButtonEnabled: {
        marginTop: 30,
        ...filledButton,
        backgroundColor: Color.red
    },
    submitButtonTextDisabled: {
        ...buttonText,
        color: Color.gray
    },
    submitButtonTextEnabled: {
        ...buttonText,
        color: "#fff"
    },
    formGroupWrap: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "stretch"
    }
});
