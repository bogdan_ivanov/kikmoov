import * as React from "react";
import PropTypes from "prop-types";
import deepEqual from "deep-equal";
import { View, ActivityIndicator, Text } from "react-native";
import { Form, FormGroup, Input, ErrorTip } from "react-native-formawesome";

import { RFASubmitButton } from "@components/partials/RFAElements";

import { withProfileDetailsContext, ProfileDetailsConextPropTypes } from "../../Providers";

import { validator } from "./PersonalDetailsFormModel";
import { styles } from "./styles";

@withProfileDetailsContext
export class PersonalDetailsForm extends React.Component {
    static propTypes = {
        profileDetailsContext: PropTypes.shape(ProfileDetailsConextPropTypes).isRequired
    };

    state = {
        shadowModel: {}
    };

    componentDidMount() {
        this.syncData();

        this.setState({ shadowModel: validator.modelValues });
    }

    componentWillUnmount() {
        validator.clear();

        this.props.profileDetailsContext.onUpdateDetails.cancel();
    }

    render() {
        return (
            <Form
                style={styles.form}
                validator={validator}
                onSubmit={this.handleSubmit}
            >
                <View style={styles.formGroupWrap}>
                    <FormGroup style={{ width: "47%" }} attribute="firstname" validateOn="blur">
                        <Input
                            style={styles.input}
                            placeholder="First name"
                            onChangeText={this.handleChange}
                            onErrorStyles={styles.inputError}
                            underlineColorAndroid="transparent"
                        />
                        <ErrorTip style={styles.error} />
                    </FormGroup>
                    <FormGroup style={{ width: "47%" }} attribute="lastname" validateOn="blur">
                        <Input
                            style={styles.input}
                            placeholder="Last name"
                            onChangeText={this.handleChange}
                            onErrorStyles={styles.inputError}
                            underlineColorAndroid="transparent"
                        />
                        <ErrorTip style={styles.error} />
                    </FormGroup>
                </View>
                <FormGroup attribute="email" validateOn="blur">
                    <Input
                        style={styles.input}
                        placeholder="email"
                        onChangeText={this.handleChange}
                        onErrorStyles={styles.inputError}
                        underlineColorAndroid="transparent"
                    />
                    <ErrorTip style={styles.error} />
                </FormGroup>
                <FormGroup attribute="phone" validateOn="blur">
                    <Input
                        style={styles.input}
                        onErrorStyles={styles.inputError}
                        underlineColorAndroid="transparent"

                        onChangeText={this.handleChange}

                        placeholder="Phone number"
                        keyboardType="phone-pad"
                        returnKeyType="done"
                    />
                    <ErrorTip style={styles.error} />
                </FormGroup>
                <FormGroup style={{ marginTop: 25 }} attribute="companyName" validateOn="blur">
                    <Input
                        style={styles.input}
                        placeholder="Company (optional)"
                        onErrorStyles={styles.inputError}
                        onChangeText={this.handleChange}
                        underlineColorAndroid="transparent"
                    />
                    <ErrorTip style={styles.error} />
                </FormGroup>
                <RFASubmitButton
                    activeOpacity={this.isChanged ? 0.2 : 1}
                    loadingComponent={<ActivityIndicator size="small" color="#fff" />}
                    style={this.isChanged ? styles.submitButtonEnabled : styles.submitButtonDisabled}
                >
                    <Text style={this.isChanged ? styles.submitButtonTextEnabled : styles.submitButtonTextDisabled}>
                        Update
                    </Text>
                </RFASubmitButton>
            </Form>
        );
    }

    get isChanged() {
        return !deepEqual(this.state.shadowModel, validator.modelValues);
    }

    syncData = () => {
        const { pushStatus, ...defaults } = this.props.profileDetailsContext.profileDetails;

        validator.setDefaults(defaults);
        validator.dropToDefaults();
    }

    handleChange = () => {
        this.forceUpdate();
    }

    handleSubmit = async (modelValues) => {
        if (!this.isChanged) {
            return;
        }

        const response = await this.props.profileDetailsContext.onUpdateDetails(modelValues);

        if (this.props.profileDetailsContext.onUpdateDetails.canceled) {
            return {
                cancelUpdate: true
            };
        }

        if (response && response.error) {
            throw response.error;
        }

        this.syncData();

        this.setState({ shadowModel: modelValues });
    }
}
