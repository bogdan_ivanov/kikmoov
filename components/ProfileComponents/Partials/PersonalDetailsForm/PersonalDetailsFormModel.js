import * as ClassValidator from "react-formawesome-core/class-validator";
import { ModelValidator } from "react-formawesome-core";

import { IsEmail } from "../../../../customValidators/IsEmail";

/* eslint-disable no-magic-numbers */
class PersonalDetailsFormModel {
    @ClassValidator.MinLength(3, {
        groups: ["firstname"],
        message: "First name should have length more than 2 chars"
    })
    firstname = undefined;

    @ClassValidator.MinLength(3, {
        groups: ["lastname"],
        message: "Last name should have length more than 2 chars"
    })
    lastname = undefined;

    @IsEmail({
        groups: ["email"],
        message: "invalid email"
    })
    email = undefined;

    @ClassValidator.ValidateIf((obj) => obj.phone && obj.phone.length, {
        groups: ["phone"],
    })
    phone = undefined;

    @ClassValidator.MinLength(3, {
        groups: ["companyName"],
        message: "Company name should have length more than 2 chars"
    })
    @ClassValidator.ValidateIf((obj) => obj.companyName && obj.companyName.length, {
        groups: ["companyName"],
    })
    companyName = undefined;
}
/* eslint-enable no-magic-numbers */

export const validator = new ModelValidator(PersonalDetailsFormModel);
