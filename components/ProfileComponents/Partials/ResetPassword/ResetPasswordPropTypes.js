import PropTypes from "prop-types";

export const ResetPasswordPropTypes = {
	onSuccess: PropTypes.func
};
