import * as ClassValidator from "react-formawesome-core/class-validator";
import { ModelValidator } from "react-formawesome-core";

import { IsCoincidesWith } from "../../../../customValidators/IsCoincidesWith";

class ResetPasswordModel {
    @ClassValidator.IsDefined({
        groups: ["oldPassword"],
        message: "Required filed"
    })
    @ClassValidator.IsNotEmpty({
        groups: ["oldPassword"],
        message: "Required filed"
    })
    oldPassword = undefined;

    @ClassValidator.IsDefined({
        groups: ["newPassword"],
        message: "Required filed"
    })
    @ClassValidator.IsNotEmpty({
        groups: ["newPassword"],
        message: "Required filed"
    })
    newPassword = undefined;

    @IsCoincidesWith("newPassword", {
        groups: ["confirmNewPassword"],
        message: "Password does not match"
    })
    @ClassValidator.IsDefined({
        groups: ["confirmNewPassword"],
        message: "Required filed"
    })
    @ClassValidator.IsNotEmpty({
        groups: ["confirmNewPassword"],
        message: "Required filed"
    })
    confirmNewPassword = undefined;
}

export const validator = new ModelValidator(ResetPasswordModel);
