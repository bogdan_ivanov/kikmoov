import Axios from "axios";
import * as React from "react";
import { Toaster } from "react-native-toastboard";
import { ActivityIndicator, Text, View } from "react-native";
import { Form, FormGroup, Input, ErrorTip, UnparsedErrorContainer } from "react-native-formawesome";

import { Color } from "@constants/UI";
import { updatePassword } from "@statelessActions";
import { RFASubmitButton } from "@components/partials/RFAElements";

import { styles } from "./styles";
import { validator } from "./ResetPasswordModel";
import { ResetPasswordPropTypes } from "./ResetPasswordPropTypes";

export class ResetPassword extends React.Component {
    static propTypes = ResetPasswordPropTypes;

    cancelToken = Axios.CancelToken.source();

    componentWillUnmount() {
        this.cancelToken.cancel();
        validator.clear();
    }

    render() {
        return (
            <View style={styles.formContainer}>
                <Form
                    style={styles.form}
                    validator={validator}
                    handleUnparsedErrors
                    onSubmit={this.handleSubmit}
                    errorParser={this.errorParser}
                >
                    <Text style={styles.title}>Reset password</Text>
                    <FormGroup style={{ marginBottom: 20 }} attribute="oldPassword" validateOn="blur">
                        <Input
                            underlineColorAndroid="transparent"
                            placeholder="Current password"
                            style={styles.input}
                            secureTextEntry
                        />
                        <ErrorTip style={styles.error} />
                    </FormGroup>
                    <FormGroup attribute="newPassword" validateOn="blur">
                        <Input
                            underlineColorAndroid="transparent"
                            placeholder="New password"
                            style={styles.input}
                            secureTextEntry
                        />
                        <ErrorTip style={styles.error} />
                    </FormGroup>
                    <FormGroup attribute="confirmNewPassword" validateOn="blur">
                        <Input
                            underlineColorAndroid="transparent"
                            placeholder="Re-enter new password"
                            style={styles.input}
                            secureTextEntry
                        />
                        <ErrorTip style={styles.error} />
                    </FormGroup>
                    <UnparsedErrorContainer style={styles.errorContaier}>
                        {this.renderUnparsedError}
                    </UnparsedErrorContainer>
                    <RFASubmitButton
                        style={styles.resetButton}
                        loadingComponent={<ActivityIndicator size="small" color={Color.gray} />}
                    >
                        <Text style={styles.resetButtonText}>
                            Reset
                        </Text>
                    </RFASubmitButton>
                </Form>
            </View>
        );
    }

    errorParser = (error) => {
        return error.response.data.errors || error.response.data.message;
    }

    renderUnparsedError = (errorMessage) => {
        return <Text style={styles.errorContaierText}>{errorMessage}</Text>;
    }

    handleSubmit = async (modelValues) => {
        delete modelValues.confirmNewPassword;

        try {
            await updatePassword(modelValues, this.cancelToken.token);
        } catch (error) {
            if (Axios.isCancel(error)) {
                return {
                    cancelUpdate: true
                };
            }

            throw error;
        }

        if (this.props.onSuccess) {
            return this.props.onSuccess();
        }

        Toaster.success("Password reset!");
        validator.clear();
    }
}
