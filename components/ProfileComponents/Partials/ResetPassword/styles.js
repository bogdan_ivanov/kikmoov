import { StyleSheet } from "react-native";

import { Color, shadowArea, filledButton, buttonText, Font, backgroundInput } from "@constants/UI";

export const styles = StyleSheet.create({
    formContainer: {
        ...shadowArea,

        padding: 20,

        borderRadius: 5,

        width: "100%",

        backgroundColor: "#fff"
    },
    input: {
        ...backgroundInput,

        marginTop: 20
    },
    inputError: {
        borderColor: Color.redError,
        color: Color.redError
    },
    error: {
        color: Color.redError,
        fontFamily: Font.type.light,
    },
    resetButton: {
        ...filledButton,

        marginTop: 45,

        backgroundColor: Color.grayLight
    },
    resetButtonText: {
        ...buttonText,
        fontSize: Font.size.m,

        color: Color.gray
    },
    title: {
        fontFamily: Font.type.medium,
        fontSize: Font.size.m
    },
    errorContaier: {
        fontFamily: Font.type.medium,

        backgroundColor: Color.red,
        borderRadius: 5,
        padding: 7,
        marginTop: 25
    },
    errorContaierText: {
        color: "#fff",
        textAlign: "center"
    }
});
