import * as React from "react";
import { Text, Image } from "react-native";
import { withNavigation } from "react-navigation";
import Touchable from "react-native-platform-touchable";

import { Images } from "@assets/Images";

import { NavigationPropTypes } from "../../../../navigation/NavigationPropTypes";

import { styles } from "./styles";
import { commonStyles } from "../../commonStyles";

@withNavigation
export class PrivacyPolicy extends React.Component {
	static propTypes = NavigationPropTypes;

	render() {
		return (
			<Touchable style={commonStyles.button} onPress={this.handleOpen}>
				<React.Fragment>
					<Image resizeMode="stretch" style={styles.iconPrivacy} source={Images.iconPrivacy} />
					<Text style={commonStyles.buttonText}>Privacy policy</Text>
				</React.Fragment>
			</Touchable>
		);
	}

	handleOpen = () => {
		this.props.navigation.navigate("TermsAndConditions", { textType: "privacy" });
	}
}
