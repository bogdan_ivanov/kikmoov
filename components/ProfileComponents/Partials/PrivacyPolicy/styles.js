import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
	iconPrivacy: {
		width: 20,
		height: 20,

		marginRight: 10
	}
});
