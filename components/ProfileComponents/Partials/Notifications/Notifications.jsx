import * as React from "react";
import PropTypes from "prop-types";
import { View, Text } from "react-native";

import { Switch } from "@components/partials/Switch";

import { styles } from "./styles";
import { commonStyles } from "../../commonStyles";

import { withProfileDetailsContext, ProfileDetailsConextPropTypes } from "../../Providers";

@withProfileDetailsContext
export class Notifications extends React.Component {
    static propTypes = {
        profileDetailsContext: PropTypes.shape(ProfileDetailsConextPropTypes).isRequired
    };

    render() {
        return (
            <View style={[commonStyles.button, styles.container]}>
                <Text style={styles.text}>Notifications</Text>
                <Switch
                    value={this.props.profileDetailsContext.profileDetails.pushStatus}
                    onChange={this.props.profileDetailsContext.togglePushStatus}
                />
            </View>
        );
    }
}
