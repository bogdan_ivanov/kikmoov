import { StyleSheet } from "react-native";

import { buttonText, Font } from "@constants/UI";

export const styles = StyleSheet.create({
    container: {
        justifyContent: "space-between",
        alignItems: "center",
        flexDirection: "row"
    },
    text: {
        ...buttonText,

        fontSize: Font.size.l,
    }
});
