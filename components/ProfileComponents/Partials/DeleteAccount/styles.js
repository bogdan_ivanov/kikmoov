import { StyleSheet } from "react-native";

import { Color, filledButton, buttonText, buttonGroupContainer, Font, modalOverlay } from "@constants/UI";

export const styles = StyleSheet.create({
    iconDelete: {
        width: 20,
        height: 20,

        marginRight: 10
    },
    modalRoot: {
        ...modalOverlay
    },
    modalContainer: {
        borderRadius: 5,
        overflow: "hidden",

        backgroundColor: "#fff",

        alignItems: "center",
    },
    iconWarning: {
        width: 35,
        height: 35,

        marginTop: 20
    },
    textWarning: {
        color: Color.redError,

        fontFamily: Font.type.semiBold,
        fontSize: Font.size.l,

        textAlign: "center",

        marginTop: 20,
        marginBottom: 15,

        width: 210
    },
    textInfo: {
        fontFamily: Font.type.medium,

        fontSize: Font.size.m,
    },
    backButton: {
        ...filledButton,

        backgroundColor: "#fff",

        flex: 1
    },
    backButtonText: {
        ...buttonText,

        marginTop: 10,
        marginBottom: 10,

        color: Color.gray
    },
    deleteButton: {
        ...filledButton,

        backgroundColor: "#fff",

        flex: 1
    },
    deleteButtonText: {
        ...buttonText,

        marginTop: 10,
        marginBottom: 10,

        color: Color.redError
    },
    buttonContainer: {
        ...buttonGroupContainer,

        marginTop: 25
    },
    separator: {
        width: 1,

        marginTop: 10,
        marginBottom: 10,

        backgroundColor: Color.grayLight
    }
});
