import * as React from "react";
import PropTypes from "prop-types";
import Touchable from "react-native-platform-touchable";
import { Text, Image, Modal, View, ActivityIndicator } from "react-native";

import { StatusBarManager } from "@utils/StatusBarManager";
import { Images } from "@assets/Images";
import { Color } from "@constants/UI";

import { styles } from "./styles";
import { commonStyles } from "../../commonStyles";

import { withProfileDetailsContext, ProfileDetailsConextPropTypes } from "../../Providers";

@withProfileDetailsContext
export class DeleteAccount extends React.Component {
    static propTypes = {
        profileDetailsContext: PropTypes.shape(ProfileDetailsConextPropTypes).isRequired
    };

    state = {
        loading: false,
        modalVisible: false
    };

    componentWillUnmount() {
        StatusBarManager.restoreState();

        this.props.profileDetailsContext.onDeleteAccount.cancel();
    }

    render() {
        return (
            <React.Fragment>
                <Touchable style={commonStyles.button} onPress={this.handleOpenModal}>
                    <React.Fragment>
                        <Image resizeMode="stretch" style={styles.iconDelete} source={Images.iconDelete} />
                        <Text style={commonStyles.buttonText}>
                            Delete account
                        </Text>
                    </React.Fragment>
                </Touchable>
                <Modal
                    onRequestClose={this.handleCloseModal}
                    visible={this.state.modalVisible}
                    animationType="fade"
                    transparent
                >
                    <View style={styles.modalRoot}>
                        <View style={styles.modalContainer}>
                            <Image resizeMode="stretch" style={styles.iconWarning} source={Images.iconWarning} />
                            <Text style={styles.textWarning}>Are you sure you want to delete your account?</Text>
                            <Text style={styles.textInfo}>Important information may be lost.</Text>
                            <View style={styles.buttonContainer}>
                                {this.state.loading
                                    ? (
                                        <View style={styles.deleteButton}>
                                            <ActivityIndicator size="small" color={Color.redError} />
                                        </View>
                                    )
                                    : (
                                        <React.Fragment>
                                            <Touchable
                                                style={styles.backButton}
                                                onPress={this.handleCloseModal}
                                            >
                                                <Text style={styles.backButtonText}>Back</Text>
                                            </Touchable>
                                            <View style={styles.separator} />
                                            <Touchable style={styles.deleteButton} onPress={this.handleDelete}>
                                                <Text style={styles.deleteButtonText}>Delete account</Text>
                                            </Touchable>
                                        </React.Fragment>
                                    )}
                            </View>
                        </View>
                    </View>
                </Modal>
            </React.Fragment>
        );
    }

    handleOpenModal = () => {
        StatusBarManager.setState("hidden");
        this.setState({ modalVisible: true });
    }

    handleCloseModal = () => {
        StatusBarManager.restoreState();
        this.setState({ modalVisible: false });
        this.props.profileDetailsContext.onDeleteAccount.cancel();
    }

    handleDelete = async () => {
        this.setState({ loading: true });

        try {
            await this.props.profileDetailsContext.onDeleteAccount();
        } catch (error) {
            StatusBarManager.restoreState();
            this.setState({ modalVisible: false });

            return;
        }
    }
}
