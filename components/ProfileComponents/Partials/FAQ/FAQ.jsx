import * as React from "react";
import { API_URL } from "react-native-dotenv";
import { Toaster } from "react-native-toastboard";
import { Linking, Text, Image } from "react-native";
import Touchable from "react-native-platform-touchable";

import { Images } from "@assets/Images";

import { styles } from "./styles";
import { commonStyles } from "../../commonStyles";

export class FAQ extends React.Component {
    render() {
        return (
            <Touchable style={commonStyles.button} onPress={this.handleOpen}>
                <React.Fragment>
                    <Image resizeMode="stretch" style={styles.iconFaq} source={Images.iconFaq} />
                    <Text style={commonStyles.buttonText}>
                        FAQ
                    </Text>
                </React.Fragment>
            </Touchable>
        );
    }

    handleOpen = async () => {
        try {
            await Linking.openURL(`${API_URL}/how-it-works#faq`);
        } catch (error) {
            return Toaster.error(`Couldn't open link ${API_URL}`);
        }
    }
}
