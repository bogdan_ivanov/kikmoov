import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
    iconFaq: {
        width: 20,
        height: 20,

        marginRight: 10
    }
});
