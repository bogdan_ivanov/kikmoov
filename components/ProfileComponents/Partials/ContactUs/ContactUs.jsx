import Axios from "axios";
import * as React from "react";
import { Toaster } from "react-native-toastboard";
import { SUPPORT_EMAIL, SUPPORT_PHONE } from "react-native-dotenv";
import { ActivityIndicator, TouchableOpacity, Linking, Text, View } from "react-native";
import { Form, FormGroup, Input, ErrorTip, UnparsedErrorContainer } from "react-native-formawesome";

import { contactUs } from "@statelessActions";
import { RFASubmitButton } from "@components/partials/RFAElements";

import { styles } from "./styles";
import { validator } from "./ContactUsModel";
import { ContactUsPropTypes } from "./ContactUsPropTypes";

export class ContactUs extends React.Component {
    static propTypes = ContactUsPropTypes;

    state = {
        hasValue: false
    };

    cancelToken = Axios.CancelToken.source();

    componentWillUnmount() {
        this.cancelToken.cancel();

        validator.clear();
    }

    render() {
        return (
            <React.Fragment>
                <View style={styles.contentContainer}>
                    <Text style={styles.titleText}>Call Kikmoov</Text>
                    <TouchableOpacity onPress={this.handlePhonePress}>
                        <Text style={styles.actionText}>{SUPPORT_PHONE}</Text>
                    </TouchableOpacity>
                    <Text style={[styles.titleText, { marginTop: 12 }]}>Email</Text>
                    <TouchableOpacity onPress={this.handleEmailPress}>
                        <Text style={styles.actionText}>{SUPPORT_EMAIL}</Text>
                    </TouchableOpacity>
                </View>
                <Form
                    handleUnparsedErrors
                    validator={validator}
                    onSubmit={this.handleSubmit}
                    errorParser={this.errorParser}
                    style={styles.contentContainer}
                >
                    <Text style={styles.titleText}>Got a question for Kikmoov?</Text>
                    <FormGroup attribute="message">
                        <Input
                            multiline
                            blurOnSubmit

                            onChangeText={this.handleChange}

                            style={styles.input}
                            onErrorStyles={styles.inputError}

                            height={120}
                            textAlignVertical="top"
                            underlineColorAndroid="transparent"
                            placeholder="Type your enquiry here..."
                        />
                        <ErrorTip style={styles.error} />
                    </FormGroup>
                    <UnparsedErrorContainer style={styles.errorContaier}>
                        {this.renderUnparsedError}
                    </UnparsedErrorContainer>
                    <RFASubmitButton
                        style={[styles.buttonDefault, this.state.hasValue && styles.buttonActive]}
                        loadingComponent={<ActivityIndicator color="#fff" size="small" />}
                    >
                        <Text
                            style={[
                                styles.buttonTextDefault,
                                this.state.hasValue && styles.buttonTextActive
                            ]}
                        >
                            Send
                        </Text>
                    </RFASubmitButton>
                </Form>
            </React.Fragment>
        );
    }

    handleEmailPress = async () => {
        try {
            await Linking.openURL(`mailto: ${SUPPORT_EMAIL}`);
        } catch (error) {
            return Toaster.error(`Couldn't open link '${SUPPORT_EMAIL}'`);
        }
    }

    handlePhonePress = async () => {
        try {
            await Linking.openURL(`tel: ${SUPPORT_PHONE}`);
        } catch (error) {
            return Toaster.error(`Couldn't open link '${SUPPORT_PHONE}'`);
        }
    }

    errorParser = (error) => {
        return error.response.data.errors || error.response.data.message;
    }

    renderUnparsedError = (errorMessage) => {
        return <Text style={styles.errorContaierText}>{errorMessage}</Text>;
    }

    handleChange = (value) => {
        this.setState({ hasValue: !!value });
    }

    handleSubmit = async (modelValues) => {
        try {
            await contactUs(modelValues, this.cancelToken.token);
        } catch (error) {
            if (Axios.isCancel(error)) {
                return {
                    cancelUpdate: true
                };
            }

            throw error;
        }

        if (this.props.onSuccess) {
            return this.props.onSuccess();
        }

        Toaster.success("Message sent!");
        validator.clear();
        this.setState({ hasValue: false });
    }
}
