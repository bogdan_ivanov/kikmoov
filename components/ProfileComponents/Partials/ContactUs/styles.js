import { StyleSheet } from "react-native";

import { Color, shadowArea, filledButton, buttonText, Font, backgroundInput } from "@constants/UI";

export const styles = StyleSheet.create({
    contentContainer: {
        ...shadowArea,

        padding: 15,

        marginTop: 10,
        marginBottom: 10,

        borderRadius: 5,

        width: "100%",

        backgroundColor: "#fff"
    },
    titleText: {
        fontFamily: Font.type.medium,
        fontSize: Font.size.m
    },
    actionText: {
        fontFamily: Font.type.medium,
        fontSize: Font.size.m,

        color: Color.red,

        marginTop: 7
    },
    input: {
       ...backgroundInput,

       marginTop: 25,
       marginBottom: 10
    },
    buttonDefault: {
        ...filledButton,

        backgroundColor: Color.grayLight
    },
    buttonActive: {
        backgroundColor: Color.red
    },
    buttonTextDefault: {
        ...buttonText,

        color: Color.gray
    },
    buttonTextActive: {
        color: "#fff"
    },
    inputError: {
        borderColor: Color.redError,
        color: Color.redError
    },
    error: {
        color: Color.redError,
        fontFamily: Font.type.light,

        marginBottom: 10
    },
    errorContaier: {
        fontFamily: Font.type.medium,

        backgroundColor: Color.red,
        borderRadius: 5,
        padding: 7,
        marginTop: 25
    },
    errorContaierText: {
        color: "#fff",
        textAlign: "center"
    }
});
