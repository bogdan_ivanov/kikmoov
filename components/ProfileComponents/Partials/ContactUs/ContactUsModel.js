import * as ClassValidator from "react-formawesome-core/class-validator";
import { ModelValidator } from "react-formawesome-core";

/* eslint-disable no-magic-numbers */
class ContactUsModel {
	@ClassValidator.MinLength(10, {
		groups: ["message"],
		message: "Message should have length 10 characters or more"
	})
	@ClassValidator.IsDefined({
		groups: ["message"],
		message: "Message is required"
	})
	message = undefined;
}

export const validator = new ModelValidator(ContactUsModel);
