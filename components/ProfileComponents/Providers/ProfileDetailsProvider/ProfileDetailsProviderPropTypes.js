import PropTypes from "prop-types";

import { NavigationPropTypes } from "../../../../navigation/NavigationPropTypes";

export const ProfileDetailsProviderPropTypes = {
	authorized: PropTypes.bool.isRequired,

	deleteUser: PropTypes.func.isRequired,
	getUserDetails: PropTypes.func.isRequired,
	updatePushStatus: PropTypes.func.isRequired,
	updateUserDetails: PropTypes.func.isRequired,

	...NavigationPropTypes
};
