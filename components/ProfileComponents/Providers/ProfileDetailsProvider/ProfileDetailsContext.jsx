import * as React from "react";
import PropTypes from "prop-types";
import hoistNonReactStatics from "hoist-non-react-statics";

import { initialState } from "@store/reducers/userReducer";

export const UserReducerStatePropTypes = {
	firstname: PropTypes.string.isRequired,
	pushStatus: PropTypes.bool.isRequired,
	lastname: PropTypes.string.isRequired,
	email: PropTypes.string.isRequired,
	companyName: PropTypes.string,
	phone: PropTypes.string
};

export const ProfileDetailsConextPropTypes = {
	profileDetails: PropTypes.shape(UserReducerStatePropTypes).isRequired,

	togglePushStatus: PropTypes.func.isRequired,
	onDeleteAccount: PropTypes.func.isRequired,
	onUpdateDetails: PropTypes.func.isRequired,
};

export const ProfileDetailsConext = React.createContext({
	profileDetails: initialState,

	togglePushStatus: () => undefined,
	onDeleteAccount: () => undefined,
	onUpdateDetails: () => undefined,
});

export function withProfileDetailsContext(WrappedComponent) {
	class Enhanced extends React.Component {
		render() {
			return (
				<ProfileDetailsConext.Consumer>
					{this.renderChildren}
				</ProfileDetailsConext.Consumer>
			);
		}

		renderChildren = (context) => {
			return <WrappedComponent {...this.props} profileDetailsContext={context} />;
		}
	}

	return hoistNonReactStatics(Enhanced, WrappedComponent);
}
