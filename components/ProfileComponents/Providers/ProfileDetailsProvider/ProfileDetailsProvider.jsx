import * as React from "react";
import { connect } from "react-redux";
import { withNavigation } from "react-navigation";

import { bindCancelToken } from "@store/utils/bindCancelToken";
import { getUserDetails, updatePushStatus, updateUserDetails, deleteUser } from "@store/actions";

import { UserContainer } from "@containers/UserContainer";

import { ProfileDetailsConext } from "./ProfileDetailsContext";
import { ProfileDetailsProviderPropTypes } from "./ProfileDetailsProviderPropTypes";

const mapStateToProps = (state) => ({
	authorized: state.securityReducer.authorized
});

const mapDispatchToProps = bindCancelToken({
	deleteUser,
	getUserDetails,
	updatePushStatus,
	updateUserDetails
});

@withNavigation
@connect(mapStateToProps, mapDispatchToProps)
export class ProfileDetailsPropvider extends React.Component {
	static propTypes = ProfileDetailsProviderPropTypes;

	state = {
		pushStatus: false
	};

	componentWillUnmount() {
		this.cancelRequests();
	}

	render() {
		return (
			<UserContainer onFetchSuccess={this.handleFetchSuccess}>
				{(profileDetails) => (
					<ProfileDetailsConext.Provider value={this.getChildContextValue(profileDetails)}>
						{this.props.children}
					</ProfileDetailsConext.Provider>
				)}
			</UserContainer>
		);
	}

	handleFetchSuccess = ({ pushStatus }) => {
		this.setState({ pushStatus });
	}

	getChildContextValue = (profileDetails) => {
		return {
			profileDetails: {
				...profileDetails,
				pushStatus: this.state.pushStatus
			},

			onUpdateDetails: bindCancelToken.link(
				this.handleUpdateDetails(profileDetails),
				this.props.updateUserDetails
			),
			onDeleteAccount: bindCancelToken.link(this.handleDeleteAccount, this.props.deleteUser),
			togglePushStatus: this.handleUpdatePushStatus,
		};
	}

	cancelRequests = () => {
		this.props.deleteUser.cancel();
		this.props.updateUserDetails.cancel();
	}

	handleUpdatePushStatus = () => {
		this.setState(({ pushStatus }) => ({
			pushStatus: !pushStatus
		}), () => this.props.updatePushStatus(this.state.pushStatus));
	}

	handleDeleteAccount = async () => {
		await this.props.deleteUser();

		if (!this.props.authorized) {
			this.props.navigation.navigate("Main");
		}
	}

	handleUpdateDetails = (prevDetails) => (newDetails) => {
		const diff = {};

		Object.keys(newDetails)
			.filter((key) => prevDetails[key] !== newDetails[key])
			.forEach((key) => diff[key] = newDetails[key]);

		return this.props.updateUserDetails(diff);
	}
}
