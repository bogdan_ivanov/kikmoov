import { StyleSheet } from "react-native";

import { shadowArea, Color, filledButton, buttonText, containerStatic, Font } from "@constants/UI";

export const commonStyles = StyleSheet.create({
    button: {
        ...shadowArea,
        ...filledButton,
        ...containerStatic,

        alignItems: "center",
        flexDirection: "row",
        justifyContent: "flex-start",

        marginBottom: 10,

        paddingTop: 15,
        paddingBottom: 15
    },
    buttonText: {
        ...buttonText,
        fontSize: Font.size.l,

        color: Color.red
    }
});
