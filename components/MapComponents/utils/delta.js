import interpolate from "interpolate-range";

const MAX_DELTA = 80;
const MIN_DELTA = 0.0005;

export const delta = interpolate({
	inputRange: [1, 0],
	outputRange: [MIN_DELTA, MAX_DELTA],
	clamp: true
});
