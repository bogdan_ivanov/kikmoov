import PropTypes from "prop-types";

export const MapMarkerPropTypes = {
	title: PropTypes.string.isRequired,
	coordinate: PropTypes.shape({
		latitude: PropTypes.string.isRequired,
		longitude: PropTypes.string.isRequired
	}).isRequired,
};
