import * as React from "react";
import { MapView } from "expo";
import { Text, View } from "react-native";

import { MapMarkerPropTypes } from "./MapMarkerPropTyps";
import { styles } from "./styles";

export const MapMarker = ({ coordinate, title }) => (
	<MapView.Marker
		coordinate={{
			latitude: Number(coordinate.latitude),
			longitude: Number(coordinate.longitude)
		}}
	>
		<View style={styles.root}>
			<View style={styles.container}>
				<Text style={styles.title}>{title}</Text>
			</View>
			<View style={styles.triangle} />
		</View>
	</MapView.Marker>
);

MapMarker.propTypes = MapMarkerPropTypes;
