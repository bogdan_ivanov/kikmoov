import { StyleSheet } from "react-native";

import { Color, Font, triangle } from "@constants/UI";

export const styles = StyleSheet.create({
	root: {
		position: "relative",

		paddingBottom: 10,

		overflow: "hidden"
	},
	container: {
		paddingLeft: 7,
		paddingRight: 7,
		paddingTop: 2.5,
		paddingBottom: 5,

		borderRadius: 5,

		backgroundColor: Color.red
	},
	title: {
		color: "#fff",

		fontSize: Font.size.s,
		fontFamily: Font.type.medium,

		textAlign: "center"
	},
	triangle: {
		...triangle({ type: "bottom", size: 10, height: 10, color: Color.red }),

		position: "absolute",
		bottom: 0,
		left: "50%",

		transform: [{ translateX: -10 }]
	}
});
