import * as React from "react";
import { MapView } from "expo";
import { View, Text, Modal, Image, TouchableOpacity } from "react-native";

import { WorkspacePreviewItem } from "@components/WorkspaceComponents";

import { NumberManager } from "@utils/NumberManager";
import { Images } from "@assets/Images";

import { MapTooltipPropTypes } from "./MapTooltipPropTypes";
import { styles } from "./styles";

export class MapTooltip extends React.Component {
	static propTypes = MapTooltipPropTypes;

	state = {
		modalOpened: false
	};

	supportedOrientations = [
		"portrait-upside-down",
		"landscape-right",
		"landscape-left",
		"landscape",
		"portrait"
	];

	componentWillUnmount() {
		this.setState({ modalOpened: false });
	}

	render() {
		return (
			<MapView.Marker
				onPress={this.handleOpenModal}
				coordinate={this.props.coordinate}
			>
				<WorkspacePreviewItem
					onNavigate={this.handleNavigate}
					workspace={this.props.workspace}
					location={this.props.location}
					related={this.props.related}
					extraData={this.state.modalOpened}
				>
					{this.renderTooltipItem}
				</WorkspacePreviewItem>
			</MapView.Marker>
		);
	}

	renderTooltipItem = (renderPreview) => (workspace, onChange, related) => (
		<React.Fragment>
			<View style={styles.triangleContainer}>
				<View style={styles.tooltipBody}>
					<Text style={styles.tooltipText}>
						£{NumberManager.abbreviate(workspace.price)}
					</Text>
				</View>
				<View style={styles.triangle} />
			</View>
			<Modal
				supportedOrientations={this.supportedOrientations}
				onRequestClose={this.handleCloseModal}
				visible={this.state.modalOpened}
				animationType="fade"
				transparent
			>
				<View style={styles.modalRoot}>
					<View style={styles.relativeContainer}>
						<View style={styles.modalContainer}>
							<View style={styles.paddingContainer}>
								{renderPreview(workspace, onChange, related)}
							</View>
						</View>
						<TouchableOpacity style={styles.closeButton} onPress={this.handleCloseModal}>
							<Image resizeMode="stretch" style={styles.closeButtonIcon} source={Images.mapClose} />
						</TouchableOpacity>
					</View>
				</View>
			</Modal>
		</React.Fragment>
	);

	handleNavigate = () => {
		this.handleCloseModal();
	}

	handleOpenModal = () => {
		this.setState({ modalOpened: true });
	}

	handleCloseModal = () => {
		this.setState({ modalOpened: false });
	}
}
