import PropTypes from "prop-types";

const workspacePropTypes = {
	images: PropTypes.array.isRequired,
	price: PropTypes.string.isRequired,
	type: PropTypes.string.isRequired,
	id: PropTypes.number.isRequired,

	coverImageUrl: PropTypes.string,
	deskType: PropTypes.string
};

export const MapTooltipPropTypes = {
	location: PropTypes.shape({
		name: PropTypes.string.isRequired,
		address: PropTypes.string.isRequired
	}).isRequired,
	workspace: PropTypes.shape(workspacePropTypes).isRequired,
	related: PropTypes.arrayOf(PropTypes.shape(workspacePropTypes)).isRequired,
	coordinate: PropTypes.shape({
		latitude: PropTypes.number.isRequired,
        longitude: PropTypes.number.isRequired,
        latitudeDelta: PropTypes.number.isRequired,
        longitudeDelta: PropTypes.number.isRequired
	}).isRequired
};
