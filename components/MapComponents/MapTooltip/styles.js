import { StyleSheet } from "react-native";

import { Color, Font, triangle, modalOverlay } from "@constants/UI";

export const styles = StyleSheet.create({
	tooltipBody: {
		backgroundColor: Color.grayDark,

		borderRadius: 3,

		paddingLeft: 7,
		paddingRight: 7,
		paddingTop: 4,
		paddingBottom: 4,

		minWidth: 40,

		marginBottom: 7
	},
	tooltipText: {
		fontSize: Font.size.m,
		fontFamily: Font.type.medium,

		width: "100%",

		color: "#fff",

		textAlign: "center"
	},
	triangleContainer: {
		position: "relative"
	},
	triangle: {
		...triangle({ type: "bottom", size: 7, height: 7, color: Color.grayDark }),

		position: "absolute",
		bottom: 0,
		left: "50%",

		transform: [{ translateX: -7 }]
	},
	modalRoot: {
		...modalOverlay,

		paddingLeft: 0,
		paddingRight: 0,

		height: "100%",
	},
	modalContainer: {
		borderRadius: 5,
		overflow: "hidden",

		backgroundColor: "#fff"
	},
	paddingContainer: {
		paddingLeft: 15,
		paddingRight: 15
	},
	closeButtonIcon: {
		width: 39,
		height: 39
	},
	closeButton: {
		position: "absolute",

		top: 0,
		right: 0
	},
	relativeContainer: {
		position: "relative",

		padding: 19.5
	}
});
