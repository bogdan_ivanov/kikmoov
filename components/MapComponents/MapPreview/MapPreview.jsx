import * as React from "react";
import { MapView } from "expo";
import { Image, View } from "react-native";
import { withNavigation } from "react-navigation";
import Touchable from "react-native-platform-touchable";

import { Images } from "@assets/Images";

import { MapPreviewPropTypes } from "./MapPreviewPropTypes";
import { delta } from "../utils/delta";

import { styles } from "./styles";

@withNavigation
export class MapPreview extends React.Component {
    static propTypes = MapPreviewPropTypes;

    render() {
        return (
            <View style={[this.props.style, { position: "relative" }]}>
                <MapView
                    initialRegion={{
                        latitude: Number(this.props.region.latitude),
                        longitude: Number(this.props.region.longitude),
                        latitudeDelta: delta(this.props.zoom),
                        longitudeDelta: delta(this.props.zoom)
                    }}
                    provider={MapView.PROVIDER_GOOGLE}
                    rotateEnabled={false}
                    scrollEnabled={false}
                    pitchEnabled={false}
                    zoomEnabled={false}
                    loadingEnabled

                    style={this.props.style}
                >
                    {this.props.children}
                </MapView>
                <Touchable
                    onPress={this.handleNavigate}
                    style={{ top: 0, left: 0, width: "100%", height: "100%", position: "absolute" }}
                >
                    <Image source={Images.expandIcon} style={styles.expand} />
                </Touchable>
            </View>
        );
    }

    handleNavigate = () => this.props.navigation.navigate("Map", {
        regions: [this.props.region],
        renderWith: "markers"
    });
}
