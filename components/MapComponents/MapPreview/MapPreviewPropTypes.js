import PropTypes from "prop-types";

import { NavigationPropTypes } from "../../../navigation/NavigationPropTypes";

export const MapPreviewPropTypes = {
	region: PropTypes.shape({
		latitude: PropTypes.string.isRequired,
		longitude: PropTypes.string.isRequired
	}).isRequired,
	zoom: PropTypes.number.isRequired,

	...NavigationPropTypes
};
