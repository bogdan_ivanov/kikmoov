import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
	expand: {
		width: 35,
		height: 35,

		position: "absolute",
		top: 5,
		right: 5
	}
});
