import { StyleSheet } from "react-native";
import { Constants } from "expo";

import { Color, Font } from "@constants/UI";

const commonContainer = {
	justifyContent: "center",
	alignItems: "center",

	paddingTop: Constants.statusBarHeight,
	paddingBottom: 17,
	paddingLeft: 17,
	paddingRight: 17
};

const text = {
	fontFamily: Font.type.medium,
	fontSize: Font.size.m,

	color: "#fff"
};

const icon = {
	width: 30,
	height: 30,

	tintColor: "#fff",

	marginBottom: 10
};

export const styles = {
	info: StyleSheet.create({
		container: {
			...commonContainer,

			backgroundColor: Color.blue
		},
		text: {
			...text,

			textAlign: "center"
		},
		icon: {
			...icon
		}
	}),
	success: StyleSheet.create({
		container: {
			...commonContainer,

			backgroundColor: Color.grayDark
		},
		text: {
			...text,

			textAlign: "center"
		},
		icon: {
			...icon
		}
	}),
	error: StyleSheet.create({
		container: {
			...commonContainer,

			backgroundColor: Color.red
		},
		text: {
			...text,

			textAlign: "center"
		},
		icon: {
			...icon
		}
	})
};
